#include "precompiled.hpp"

#include "grace/render/material/material.hpp"
#include "grace/render/shader/shader.hpp"
#include "grace/render/texture/texture2D.hpp"
#include "grace/render/texture/cube_texture.hpp"

//-----------------------------------------------------------------------------

namespace gc
{

//-----------------------------------------------------------------------------

struct PropertyBinder
{
	PropertyBinder( const gc::Shader& i_shader, std::string_view i_uniformName, gc::u16& i_textureUnit ) :
		m_shader( i_shader ),
		m_uniformName( i_uniformName.data() ),
		m_textureUnit( i_textureUnit )
	{
	}

	void operator() ( const gc::Texture2D* i_texture ) const
	{
		if( m_textureUnit >= m_shader.getUniformsCount() )
			return;

		i_texture->bind( m_textureUnit );
		m_shader.uniformValue( m_shader.getTextureUnifrormName( m_textureUnit ), m_textureUnit );
		m_textureUnit++;
	}

	void operator() ( const gc::CubeTexture* i_texture ) const
	{
		if( m_textureUnit >= m_shader.getUniformsCount() )
			return;

		i_texture->bind( m_textureUnit );
		m_shader.uniformValue( m_shader.getTextureUnifrormName( m_textureUnit ), m_textureUnit );
		m_textureUnit++;
	}

	template<typename T>
	void operator() ( const T& i_value ) const
	{
		m_shader.uniformValue( m_uniformName, i_value );
	}

private:
	const gc::Shader& m_shader;
	std::string m_uniformName;
	gc::u16& m_textureUnit;
};

//-----------------------------------------------------------------------------

Material::Material() :
	m_shader( nullptr )
{
}

void Material::setShader( const Shader* i_shader )
{
	m_shader = i_shader;
}

void Material::bind() const
{
	if( m_shader )
	{
		gc::u16 textureUnit = 0;
		for( const auto& [name, value] : m_materialData )
		{
			PropertyBinder binder( *m_shader, name, textureUnit );
			std::visit( binder, value );
		}
	}
}

void Material::updateValue( std::string_view i_name, const gc::Texture2D& i_val )
{
	for( auto& data : m_materialData )
	{
		if( data.m_name == i_name )
		{
			data.m_value = &i_val;
		}
	}
}

void Material::forEachMaterialData( const std::function<void( const MaterialData& )>& i_callback ) const
{
	for( const auto& data : m_materialData )
	{
		i_callback( data );
	}
}

void Material::forEachMaterialData( const std::function<void( MaterialData& )>& i_callback )
{
	for( auto& data : m_materialData )
	{
		i_callback( data );
	}
}

//-----------------------------------------------------------------------------

} // namespace gc

//-----------------------------------------------------------------------------