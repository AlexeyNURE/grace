#pragma once

#include <string_view>
#include <variant>
#include <vector>

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include "grace/core/utils/basic_types.hpp"

//-----------------------------------------------------------------------------

namespace gc
{

//-----------------------------------------------------------------------------

class Shader;
class Texture2D;
class CubeTexture;

//-----------------------------------------------------------------------------

struct MaterialData
{
	using Data = std::variant<float, gc::s32, glm::vec2, glm::vec3, glm::vec4, const gc::Texture2D*, const gc::CubeTexture*>;

	std::string m_name;
	Data m_value;
};

//-----------------------------------------------------------------------------

class Material
{
public:
	Material();
	~Material() = default;

	void setValue( std::string_view i_name, float i_val ) { m_materialData.emplace_back( MaterialData{ i_name.data(), i_val } ); }
	void setValue( std::string_view i_name, gc::s32 i_val ) { m_materialData.emplace_back( MaterialData{ i_name.data(), i_val } ); }
	void setValue( std::string_view i_name, const glm::vec3& i_val ) { m_materialData.emplace_back( MaterialData{ i_name.data(), i_val } ); }
	void setValue( std::string_view i_name, const glm::vec2& i_val ) { m_materialData.emplace_back( MaterialData{ i_name.data(), i_val } ); }
	void setValue( std::string_view i_name, const glm::vec4& i_val ) { m_materialData.emplace_back( MaterialData{ i_name.data(), i_val } ); }
	void setValue( std::string_view i_name, const gc::Texture2D& i_val ) { m_materialData.emplace_back( MaterialData{ i_name.data(), &i_val } ); }
	void setValue( std::string_view i_name, const gc::CubeTexture& i_val ) { m_materialData.emplace_back( MaterialData{ i_name.data(), &i_val } );  }

	void bind() const;

	void updateValue( std::string_view i_name, const gc::Texture2D& i_val );

	void setShader( const Shader* i_shader );
	const Shader* getShader() const;

	void forEachMaterialData( const std::function<void( const MaterialData& )>& i_callback ) const;
	void forEachMaterialData( const std::function<void( MaterialData& )>& i_callback );

	template<typename UnaryPredicate>
	std::vector<MaterialData>::const_iterator findIf( UnaryPredicate i_pred ) const;
private:

	const Shader* m_shader;
	std::vector<MaterialData> m_materialData;
};

//-----------------------------------------------------------------------------

inline const Shader* gc::Material::getShader() const
{
	return m_shader;
}

template<typename UnaryPredicate>
inline std::vector<MaterialData>::const_iterator Material::findIf( UnaryPredicate i_pred ) const
{
	auto it = std::find_if( m_materialData.begin(), m_materialData.end(), i_pred );

	return it;
}


//-----------------------------------------------------------------------------

} // namespace gc

//-----------------------------------------------------------------------------
