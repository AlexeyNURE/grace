#pragma once

#include "grace/render/renderer_API.hpp"
#include "grace/render/texture/texture2D.hpp"
#include "grace/render/shader/shader.hpp"
#include "grace/render/effects/FBO.hpp"
#include "grace/render/model/model.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

class Pass
{
public:
	Pass();
	~Pass() = default;

	void setFBO( FBO* i_fbo );
	void setShader( Shader* i_shader );
	void addTextures( std::vector<const Texture2D*> i_textures );
	void setParams( std::vector<float>& i_params );
	void setCameraFar( float i_far );
	void setCameraNear( float i_near );
	void render( const OpenGLRendererAPI& i_renderer );

private:
	static std::unique_ptr<Model> createRectModel();
	void uniformShaderParams();
	void uniformTextures();
	void uniformStep();
	void uniformDOF();

	FBO* m_pFBO;
	Shader* m_pShader;
	std::unique_ptr<Model> m_model;
	std::vector<const Texture2D*> m_textures;
	std::vector<s32> m_texureUniformValue;

	std::vector<float> m_parameters;

	bool m_hasDOF;
	float m_cameraFar;
	float m_cameraNear;
};

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////