#pragma once

#include "grace/render/texture/texture2D.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

// Frames buffer object
class FBO
{
public:
	FBO();
	~FBO();

	void init();

	void bind();
	void unbind();

	void regenerate();

	const Texture2D* getColorTex() const;
	const Texture2D* getDepthTex() const;

	u32 m_ID;
private:

	Texture2D m_colorTex;
	Texture2D m_depthTex;
};

inline const Texture2D* FBO::getColorTex() const
{
	return &m_colorTex;
}

inline const Texture2D* FBO::getDepthTex() const
{
	return &m_depthTex;
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////