#include "precompiled.hpp"

#include <glad/glad.h>

#include "grace/render/effects/FBO.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

FBO::FBO() :
	m_ID( 0 )
{
	m_colorTex.initFrame( GL_RGBA, GL_UNSIGNED_BYTE );
	m_depthTex.initFrame( GL_DEPTH_COMPONENT, GL_UNSIGNED_INT );

	glGenFramebuffers( 1, &m_ID );
}

FBO::~FBO()
{
	glDeleteFramebuffers( 1, &m_ID );
}

void FBO::init()
{
	bind();

	const u32 hColorTex = m_colorTex.getTextureHandle();
	const u32 hDepthTex = m_depthTex.getTextureHandle();

	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, hColorTex, 0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, hDepthTex, 0 );
}

void FBO::bind()
{
	glBindFramebuffer( GL_FRAMEBUFFER, m_ID );
}

void FBO::unbind()
{
	glBindFramebuffer( GL_FRAMEBUFFER, 0 );
}

void FBO::regenerate()
{
	glDeleteFramebuffers( 1, &m_ID );

	m_colorTex.regenerate( GL_RGBA, GL_UNSIGNED_BYTE );
	m_depthTex.regenerate( GL_DEPTH_COMPONENT, GL_UNSIGNED_INT );

	glGenFramebuffers( 1, &m_ID );

	init();
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////