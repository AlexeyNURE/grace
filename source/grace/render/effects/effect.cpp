#include "precompiled.hpp"

#include "grace/render/effects/effect.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

void Effect::addPasses( std::vector<std::unique_ptr<Pass>> _passes )
{
	m_passes = std::move( _passes );
}

void Effect::render( const OpenGLRendererAPI& renderer )
{
	for( auto& pass : m_passes )
	{
		pass->render( renderer );
	}
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////