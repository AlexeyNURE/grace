#include "precompiled.hpp"

#include "grace/core/utils/globals.hpp"

#include "grace/render/openGL/vertex.hpp"
#include "grace/render/effects/pass.hpp"
#include "grace/render/model/mesh.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

Pass::Pass() :
	m_pFBO( nullptr ),
	m_pShader( nullptr ),
	m_hasDOF( false ),
	m_cameraFar( 0.f ),
	m_cameraNear( 0.f )
{
	m_model = createRectModel();
}

void Pass::setFBO( FBO* i_fbo )
{
	m_pFBO = i_fbo;
}

void Pass::setShader( Shader* i_shader )
{
	m_pShader = i_shader;
	m_hasDOF = m_pShader->hasDOFParams();
}

void Pass::addTextures( std::vector<const Texture2D*> i_textures )
{
	m_textures = std::move( i_textures );
}

void Pass::setParams( std::vector<float>& i_params )
{
	m_parameters = std::move( i_params );
}

void Pass::setCameraFar( float i_far )
{
	m_cameraFar = i_far;
}

void Pass::setCameraNear( float i_near )
{
	m_cameraNear = i_near;
}

void Pass::render( const OpenGLRendererAPI& i_renderer )
{
	ASSERT( m_pFBO && m_pShader, "PostFX pass is not complitely initialized before it being used" );

	m_pFBO->bind();

	i_renderer.clear();

	m_pShader->bind();

	uniformShaderParams();

	m_model->bind();
	i_renderer.render( m_model.get() );

	m_model->unbind();
	m_pFBO->unbind();
}

std::unique_ptr<Model> Pass::createRectModel()
{
	u64 verticesCount = 4;
	u64 indicesCount = 6;

	std::unique_ptr<utils::Vertex[]> pVerticesData( new utils::Vertex[verticesCount] );
	std::unique_ptr<u32[]> pIndices( new u32[indicesCount] );

	pVerticesData[0].pos = glm::vec3( -1.0f, -1.0f, 0.0f );
	pVerticesData[1].pos = glm::vec3( -1.0f, 1.0f, 0.0f );
	pVerticesData[2].pos = glm::vec3( 1.0f, -1.0f, 0.0f );
	pVerticesData[3].pos = glm::vec3( 1.0f, 1.0f, 0.0f );
	pVerticesData[0].uv = glm::vec2( 0.0f, 0.0f );
	pVerticesData[1].uv = glm::vec2( 0.0f, 1.0f );
	pVerticesData[2].uv = glm::vec2( 1.0f, 0.0f );
	pVerticesData[3].uv = glm::vec2( 1.0f, 1.0f );

	pIndices[0] = 0;
	pIndices[1] = 1;
	pIndices[2] = 2;
	pIndices[3] = 1;
	pIndices[4] = 2;
	pIndices[5] = 3;

	std::unique_ptr<Mesh> mesh = std::make_unique<Mesh>( pVerticesData.get(), verticesCount, pIndices.get(), indicesCount );
	std::unique_ptr<Model> model = std::make_unique<Model>();
	model->addMesh( std::move( mesh ) );

	return model;
}

void Pass::uniformShaderParams()
{
	uniformTextures();
	uniformStep();

	if( m_hasDOF )
		uniformDOF();

	if( m_pShader->hasUniform( "u_limit" ) )
		m_pShader->uniformValue( "u_limit", m_parameters[0] );

	if( m_pShader->hasUniform( "u_passMult" ) )
		m_pShader->uniformValue( "u_passMult", m_parameters[0] );
}

void Pass::uniformTextures()
{
	gc::u16 textureUnit = 0;
	size_t texture2DCount = m_textures.size();
	for( size_t i = 0; i < texture2DCount; i++ )
	{
		m_textures[i]->bind( textureUnit );
		m_pShader->uniformValue( m_pShader->getTextureUnifrormName( textureUnit ), textureUnit );
		++textureUnit;
	}
}

void Pass::uniformStep()
{
	if( !m_pShader->hasUniform( "u_step" ) )
		return;

	float step = m_parameters[0];

	float x = 1.0f / Globals::s_screenWidth * step;
	float y = 1.0f / Globals::s_screenHeight * step;
	float z = sqrt( 2.0f ) / 2.0f * x;
	float w = sqrt( 2.0f ) / 2.0f * y;

	glm::vec4 stepVec = glm::vec4( x, y, z, w );

	m_pShader->uniformValue( "u_step", stepVec );
}

void Pass::uniformDOF()
{
	float clarity = m_parameters[1];
	float fade = m_parameters[2];

	m_pShader->uniformValue( "u_far", m_cameraFar );
	m_pShader->uniformValue( "u_near", m_cameraNear );
	m_pShader->uniformValue( "u_clarity", clarity );
	m_pShader->uniformValue( "u_fade", fade );
}

} // namespace gc

///////////////////////////////////////////////////////////////////////////////