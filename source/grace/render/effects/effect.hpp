#pragma once

#include "grace/render/renderer_API.hpp"
#include "grace/render/effects/pass.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

class Effect
{
public:
	Effect() = default;
	~Effect() = default;

	void addPasses( std::vector<std::unique_ptr<Pass>> _passes );

	void render( const OpenGLRendererAPI& renderer );

private:

	std::vector<std::unique_ptr<Pass>> m_passes;
};

//-----------------------------------------------------------------------------

}

///////////////////////////////////////////////////////////////////////////////