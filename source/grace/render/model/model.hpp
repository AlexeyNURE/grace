#pragma once

#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>
#include <memory>
#include <vector>

#include "grace/core/utils/math/aabb3.hpp"

#include "grace/render/openGL/vertexArray.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

class Mesh;
class Material;

//-----------------------------------------------------------------------------

class Model : boost::noncopyable
{
public:
	Model() = default;
	Model( const utils::Vertex* i_vertices, u64 i_verticesCount, const u32* i_indices, u64 i_indicesCount );
	~Model();

	void addMesh( std::unique_ptr<Mesh> i_mesh );

	void setBoundingBox( const utils::aabb3f& i_boundingBox );
	const utils::aabb3f& getBoundingBox() const;

	void forEachMesh( const std::function<void( const Mesh& )>& i_callback ) const;
	void forEachMesh( const std::function<void( Mesh& )>& i_callback );

	void forEachMaterial( const std::function<void( const Material& )>& i_callback ) const;
	void forEachMaterial( const std::function<void( Material& )>& i_callback );

	const Mesh* getMeshByName( std::string_view i_name ) const;
	Mesh* getMeshByName( std::string_view i_name );
	const Mesh* getMeshByIdx( gc::u32 i_idx ) const;
	Mesh* getMeshByIdx( gc::u32 i_idx );

	void bind();
	void unbind();

private:
	void calculateBoundingBox();

	std::vector<std::unique_ptr<Mesh>> m_meshes;

	utils::aabb3f m_boundingBox;
};

//-----------------------------------------------------------------------------

inline const utils::aabb3f& gc::Model::getBoundingBox() const
{
	return m_boundingBox;
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////