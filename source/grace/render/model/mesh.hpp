#pragma once

#include <boost/noncopyable.hpp>

#include "grace/core/utils/math/aabb3.hpp"

#include "grace/render/openGL/vertexArray.hpp"
#include "grace/render/material/material.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

class Mesh : boost::noncopyable
{
public:
	Mesh( const utils::Vertex* i_vertices, u64 i_verticesCount, const u32* i_indices, u64 i_indicesCount );
	~Mesh() = default;

	u64 getVerticesCount() const;
	u64 getIndicesCount() const;

	void setMaterial( const gc::Material& i_mat );
	const gc::Material& getMaterial() const;
	gc::Material& getMaterial();

	const utils::aabb3f& getBoundingBox() const;
	void setBoundingBox( const utils::aabb3f& i_aabb );

	void setName( std::string_view i_name );
	std::string_view getName() const;

	void bind() const;
	void unbind() const;

private:
	void calculateBoundingBox( const utils::Vertex* i_vertices, u64 i_verticesCount );

	utils::aabb3f m_boundingBox;

	VertexArray m_vertexArray;
	gc::Material m_material;

	std::string m_name;
};

//-----------------------------------------------------------------------------

inline u64 Mesh::getVerticesCount() const
{
	return m_vertexArray.getVerticesCount();
}

inline u64 Mesh::getIndicesCount() const
{
	return m_vertexArray.getIndicesCount();
}

inline void Mesh::setMaterial( const gc::Material& i_mat )
{
	m_material = i_mat;
}

inline const gc::Material& Mesh::getMaterial() const
{
	return m_material;
}

inline gc::Material& gc::Mesh::getMaterial()
{
	return m_material;
}

inline const utils::aabb3f& gc::Mesh::getBoundingBox() const
{
	return m_boundingBox;
}

inline std::string_view Mesh::getName() const
{
	return m_name;
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////