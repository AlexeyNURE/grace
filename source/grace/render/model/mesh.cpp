#include "precompiled.hpp"

#include "grace/render/model/mesh.hpp"
#include "grace/core/utils/utils.hpp"
#include "grace/render/openGL/vertex.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

Mesh::Mesh( const utils::Vertex* i_vertices, u64 i_verticesCount, const u32* i_indices, u64 i_indicesCount ) :
	m_vertexArray( i_vertices, i_verticesCount, i_indices, i_indicesCount )
{
	calculateBoundingBox( i_vertices, i_verticesCount );
}

void Mesh::bind() const
{
	m_material.bind();
	m_vertexArray.bind();
}

void Mesh::unbind() const
{
	m_vertexArray.unbind();
}

void Mesh::setBoundingBox( const utils::aabb3f& i_aabb )
{
	m_boundingBox = i_aabb;
}

void Mesh::setName( std::string_view i_name )
{
	m_name = i_name;
}

void Mesh::calculateBoundingBox( const utils::Vertex* i_vertices, u64 i_verticesCount )
{
	float minX = std::numeric_limits<float>::max();
	float maxX = std::numeric_limits<float>::lowest();
	float minY = std::numeric_limits<float>::max();
	float maxY = std::numeric_limits<float>::lowest();
	float minZ = std::numeric_limits<float>::max();
	float maxZ = std::numeric_limits<float>::lowest();

	for( size_t i = 0; i < i_verticesCount; i++ )
	{
		const glm::vec3& currentPos = i_vertices[i].pos;

		if( currentPos.x < minX )
			minX = currentPos.x;
		if( currentPos.x > maxX )
			maxX = currentPos.x;
		if( currentPos.y < minY )
			minY = currentPos.y;
		if( currentPos.y > maxY )
			maxY = currentPos.y;
		if( currentPos.z < minZ )
			minZ = currentPos.z;
		if( currentPos.z > maxZ )
			maxZ = currentPos.z;
	}

	setBoundingBox( utils::aabb3f( minX, minY, minZ, maxX, maxY, maxZ ) );
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////