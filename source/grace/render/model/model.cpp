#include "precompiled.hpp"

#include "grace/render/model/model.hpp"
#include "grace/render/model/mesh.hpp"
#include "grace/core/utils/utils.hpp"
#include "grace/render/openGL/vertex.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

Model::Model( const utils::Vertex* i_vertices, u64 i_verticesCount, const u32* i_indices, u64 i_indicesCount )
{
	std::unique_ptr<Mesh> mesh = std::make_unique<Mesh>( i_vertices, i_verticesCount, i_indices, i_indicesCount );
	addMesh( std::move( mesh ) );

	calculateBoundingBox();
}

Model::~Model() = default;

void Model::addMesh( std::unique_ptr<Mesh> i_mesh )
{
	m_meshes.emplace_back( std::move( i_mesh ) );
	calculateBoundingBox();
}

void Model::forEachMesh( const std::function<void( const Mesh& )>& i_callback ) const
{
	for( const auto& mesh : m_meshes )
	{
		i_callback( *mesh );
	}
}

void Model::forEachMesh( const std::function<void( Mesh& )>& i_callback )
{
	for( auto& mesh : m_meshes )
	{
		i_callback( *mesh );
	}
}

void Model::forEachMaterial( const std::function<void( const Material& )>& i_callback ) const
{
	for( const auto& mesh : m_meshes )
	{
		i_callback( mesh->getMaterial() );
	}
}

void Model::forEachMaterial( const std::function<void( Material& )>& i_callback )
{
	for( auto& mesh : m_meshes )
	{
		i_callback( mesh->getMaterial() );
	}
}

const Mesh* Model::getMeshByName( std::string_view i_name ) const
{
	for( const auto& mesh : m_meshes )
	{
		if( mesh->getName() == i_name )
		{
			return mesh.get();
		}
	}

	return nullptr;
}

Mesh* Model::getMeshByName( std::string_view i_name )
{
	for( const auto& mesh : m_meshes )
	{
		if( mesh->getName() == i_name )
		{
			return mesh.get();
		}
	}

	return nullptr;
}

const Mesh* Model::getMeshByIdx( gc::u32 i_idx ) const
{
	return m_meshes[i_idx].get();
}

Mesh* Model::getMeshByIdx( gc::u32 i_idx )
{
	return m_meshes[i_idx].get();
}

void Model::setBoundingBox( const utils::aabb3f& i_boundingBox )
{
	m_boundingBox = i_boundingBox;
}

void Model::bind()
{
	for( const auto& mesh : m_meshes )
	{
		mesh->bind();
	}
}

void Model::unbind()
{
	for( const auto& mesh : m_meshes )
	{
		mesh->unbind();
	}
}

void Model::calculateBoundingBox()
{
	float minX = std::numeric_limits<float>::max();
	float maxX = std::numeric_limits<float>::lowest();
	float minY = std::numeric_limits<float>::max();
	float maxY = std::numeric_limits<float>::lowest();
	float minZ = std::numeric_limits<float>::max();
	float maxZ = std::numeric_limits<float>::lowest();

	for( const auto& mesh : m_meshes )
	{
		const utils::aabb3f& aabbMesh = mesh->getBoundingBox();

		if( aabbMesh.getMin().x < minX )
			minX = aabbMesh.getMin().x;
		if( aabbMesh.getMax().x > maxX )
			maxX = aabbMesh.getMax().x;
		if( aabbMesh.getMin().y < minY )
			minY = aabbMesh.getMin().y;
		if( aabbMesh.getMax().y > maxY )
			maxY = aabbMesh.getMax().y;
		if( aabbMesh.getMin().z < minZ )
			minZ = aabbMesh.getMin().z;
		if( aabbMesh.getMax().z > maxZ )
			maxZ = aabbMesh.getMax().z;
	}

	setBoundingBox( utils::aabb3f( minX, minY, minZ, maxX, maxY, maxZ ) );
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////