#pragma once

#include <glm/vec4.hpp>
#include "grace/core/utils/basic_types.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

class Model;
class Mesh;
class Shader;

//-----------------------------------------------------------------------------

class OpenGLRendererAPI
{
public:
	OpenGLRendererAPI() = default;
	~OpenGLRendererAPI() = default;

	void init() const;

	void renderIndices( u32 i_indicesCount ) const;
	void renderVertices( u32 i_verticesCount ) const;
	void render( const gc::Model* i_model ) const;
	void render( const gc::Mesh* i_mesh ) const;

	void setClearColor( const glm::vec4& i_color ) const;
	void clearColor() const;

	void setViewPort( s32 i_x, s32 i_y, s32 i_width, s32 i_height ) const;

	void clear( u32 i_mask ) const;
	void clear() const;

	void setPolygonMode( u32 i_face, u32 i_mode ) const;

};

//-----------------------------------------------------------------------------

};

///////////////////////////////////////////////////////////////////////////////