#include "precompiled.hpp"

#include "grace/render/renderer_API.hpp"
#include "grace/render/model/model.hpp"
#include "grace/render/model/mesh.hpp"
#include "grace/render/shader/shader.hpp"

#include <glad/glad.h>

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

void OpenGLRendererAPI::init() const
{
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

void OpenGLRendererAPI::renderIndices( u32 i_indicesCount ) const
{
	glDrawElements( GL_TRIANGLES, i_indicesCount, GL_UNSIGNED_INT, nullptr );
}

void OpenGLRendererAPI::renderVertices( u32 i_verticesCount ) const
{
	glDrawArrays( GL_TRIANGLES, 0, i_verticesCount );
}

void OpenGLRendererAPI::render( const gc::Model* i_model ) const
{
	i_model->forEachMesh( [this]( const Mesh& i_mesh )
	{
		i_mesh.bind();

		glDrawElements( GL_TRIANGLES, i_mesh.getIndicesCount(), GL_UNSIGNED_INT, nullptr );
	} );
}

void OpenGLRendererAPI::render( const gc::Mesh* i_mesh ) const
{
	gc::u64 indicesCount = i_mesh->getIndicesCount();
	if( indicesCount != 0 )
	{
		renderIndices( static_cast<gc::u32>( indicesCount ) );
	}
	else
	{
		renderVertices( static_cast<gc::u32>( i_mesh->getVerticesCount() ) );
	}
}

void OpenGLRendererAPI::setClearColor( const glm::vec4& i_color ) const
{
	glClearColor( i_color.r, i_color.g, i_color.b, i_color.w );
}

void OpenGLRendererAPI::clearColor() const
{
	glClearColor( 0.f, 0.f, 0.f, 1.f );
}

void OpenGLRendererAPI::setViewPort( s32 i_x, s32 i_y, s32 i_width, s32 i_height ) const
{
	glViewport( i_x, i_y, i_width, i_height );
}

void OpenGLRendererAPI::clear( u32 mask ) const
{
	glClear( mask );
}

void OpenGLRendererAPI::clear() const
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void OpenGLRendererAPI::setPolygonMode( u32 i_face, u32 i_mode ) const
{
	glPolygonMode( i_face, i_mode );
}

//-----------------------------------------------------------------------------

}

///////////////////////////////////////////////////////////////////////////////