#include "precompiled.hpp"

#include "grace/render/light/light.hpp"

//-----------------------------------------------------------------------------

namespace gc
{

//-----------------------------------------------------------------------------

Light::Light( LightType i_lightType, const glm::vec3& i_posDir, const glm::vec4& i_color ) :
	m_lightType( i_lightType ),
	m_positionDir( i_posDir ),
	m_color( i_color )
{
}

void Light::setPositionDir( const glm::vec3& i_posDir )
{
	m_positionDir = i_posDir;
}

void Light::setColor( const glm::vec4& i_color )
{
	m_color = i_color;
}


//-----------------------------------------------------------------------------

} // gc

//-----------------------------------------------------------------------------