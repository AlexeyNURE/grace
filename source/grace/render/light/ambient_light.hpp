#pragma once

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

struct AmbientLight
{
	glm::vec4 color;
	float weight;
};

//-----------------------------------------------------------------------------

} // gc

///////////////////////////////////////////////////////////////////////////////