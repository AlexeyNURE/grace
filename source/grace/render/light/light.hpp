#pragma once

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include "grace/core/utils/enum_utils.hpp"

//-----------------------------------------------------------------------------

namespace gc
{

//-----------------------------------------------------------------------------

REGISTER_ENUM_CLASS(
	LightType,
	directional,
	point
);

//-----------------------------------------------------------------------------

class Light
{
public:
	Light( LightType i_lightType, const glm::vec3& i_posDir, const glm::vec4& i_color );
	~Light() = default;

	LightType getLightType() const;

	const glm::vec3& getPositionDir() const;
	glm::vec3& getPositionDir();

	const glm::vec4& getColor() const;
	glm::vec4& getColor();

	void setPositionDir( const glm::vec3& i_posDir );
	void setColor( const glm::vec4& i_color );

private:
	LightType m_lightType;

	glm::vec3 m_positionDir;
	glm::vec4 m_color;
};

//-----------------------------------------------------------------------------

inline LightType Light::getLightType() const
{
	return m_lightType;
}

inline const glm::vec3& Light::getPositionDir() const
{
	return m_positionDir;
}

inline glm::vec3& Light::getPositionDir()
{
	return m_positionDir;
}

inline const glm::vec4& Light::getColor() const
{
	return m_color;
}

inline glm::vec4& Light::getColor()
{
	return m_color;
}

//-----------------------------------------------------------------------------

} // gc

//-----------------------------------------------------------------------------