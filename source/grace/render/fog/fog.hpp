#pragma once

#include <glm/vec4.hpp>

//-----------------------------------------------------------------------------

namespace gc
{

struct Fog
{
	glm::vec4 color;
	float start;
	float range;
};

} // namespace gc

//-----------------------------------------------------------------------------