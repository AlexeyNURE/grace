#pragma once

#include "grace/render/texture/texture.hpp"
#include "grace/core/utils/basic_types.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

class Texture2D : public Texture
{
public:
	Texture2D();
	virtual ~Texture2D();

	void init( TextureParams params, unsigned char* bufferTga );

	void setTiling( s32 tiling );
	void setFormat( s32 format );

	void bind( u16 _unit ) const override;
	u32 getTextureHandle() const override;
	
	void initFrame( s32 _format, u32 _type );
	void regenerate( s32 _format, u32 _type );

private:
	u32 m_hTexture;

	TextureParams m_params;
};

//-----------------------------------------------------------------------------

inline u32 Texture2D::getTextureHandle() const
{
	return m_hTexture;
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////