#include "precompiled.hpp"

#include <glad/glad.h>

#include "grace/render/texture/texture2D.hpp"
#include "grace/render/texture/load_tga.hpp"
#include "grace/core/utils/globals.hpp"
#include "grace/core/utils/logging/log.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

Texture2D::Texture2D()
{
	glGenTextures( 1, &m_hTexture );
	glBindTexture( GL_TEXTURE_2D, m_hTexture );
}

Texture2D::~Texture2D()
{
	glDeleteBuffers( 1, &m_hTexture );
}

void Texture2D::init( TextureParams i_params, unsigned char* i_buffer )
{
	m_params = i_params;

	glTexImage2D( GL_TEXTURE_2D, 0, m_params.format, m_params.width, m_params.height, 0, m_params.format,
			GL_UNSIGNED_BYTE, i_buffer );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	glGenerateMipmap( GL_TEXTURE_2D );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_params.tiling );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_params.tiling );
}

void Texture2D::initFrame( s32 i_format, u32 i_type )
{
	glGenTextures( 1, &m_hTexture );
	glBindTexture( GL_TEXTURE_2D, m_hTexture );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

	glTexImage2D( GL_TEXTURE_2D, 0, i_format, Globals::s_screenWidth, Globals::s_screenHeight, 0, static_cast<u32>( i_format ), i_type, nullptr );
	glBindTexture( GL_TEXTURE_2D, 0 );
}

void gc::Texture2D::regenerate( s32 i_format, u32 i_type )
{
	glDeleteBuffers( 1, &m_hTexture );
	initFrame( i_format, i_type );
}

void Texture2D::setTiling( s32 i_tiling )
{
	m_params.tiling = i_tiling;
}

void Texture2D::setFormat( s32 format )
{
	m_params.format = format;
}

void Texture2D::bind( u16 _unit ) const
{
	glActiveTexture( GL_TEXTURE0 + _unit );
	glBindTexture( GL_TEXTURE_2D, m_hTexture );
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////
