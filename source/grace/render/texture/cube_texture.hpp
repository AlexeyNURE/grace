#pragma once

#include "grace/render/texture/texture.hpp"
#include "grace/render/openGL/vertex.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

struct CubeTextureParams : TextureParams
{
	u16 faceWidth;
	static constexpr u16 k_facesCount = 6;
};

class CubeTexture : Texture
{
public:
	CubeTexture();
	virtual ~CubeTexture();

	void init( CubeTextureParams params, unsigned char* bufferTga, unsigned char* bufferFace );
	void bind( u16 _unit ) const override;
	u32 getTextureHandle() const override;

private:
	CubeTextureParams m_params;

	u32 m_hTexture;
};

//-----------------------------------------------------------------------------

inline u32 CubeTexture::getTextureHandle() const
{
	return m_hTexture;
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////