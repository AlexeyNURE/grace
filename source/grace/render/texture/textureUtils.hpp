#pragma once

#include <utility>
#include "grace/core/utils/basic_types.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc::utils
{

//-----------------------------------------------------------------------------

void initOffsets( std::pair<u32, u32>* _offsets, u16 _width );
void extractFace(
	const unsigned char* _bufferTga,
	unsigned char* _bufferFace,
	u64 _faceWidth,
	u16 _width,
	u32 _hOffset,
	u32 _vOffser,
	s32 _bpp
);

//-----------------------------------------------------------------------------

} // namespace gc::utils

///////////////////////////////////////////////////////////////////////////////