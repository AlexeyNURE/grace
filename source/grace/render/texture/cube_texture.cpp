#include "precompiled.hpp"

#include <glad/glad.h>

#include "grace/render/texture/cube_texture.hpp"
#include "grace/render/texture/load_tga.hpp"
#include "grace/render/texture/textureUtils.hpp"

namespace gc
{

//-----------------------------------------------------------------------------

CubeTexture::CubeTexture()
{
	glGenTextures( 1, &m_hTexture );
	glBindTexture( GL_TEXTURE_CUBE_MAP, m_hTexture );
}

CubeTexture::~CubeTexture()
{
	glDeleteBuffers( 1, &m_hTexture );
}

void CubeTexture::init( CubeTextureParams i_params, unsigned char* bufferTga, unsigned char* bufferFace )
{
	m_params = i_params;
	std::pair< u32, u32 > offsets[CubeTextureParams::k_facesCount];

	utils::initOffsets( offsets, m_params.faceWidth );

	for( u16 i = 0; i < CubeTextureParams::k_facesCount; i++ )
	{
		utils::extractFace(
			bufferTga,
			bufferFace,
			m_params.faceWidth,
			m_params.width,
			offsets[i].first,
			offsets[i].second,
			m_params.bpp
		);
		glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, m_params.format,
			m_params.faceWidth, m_params.faceWidth, 0, m_params.format, GL_UNSIGNED_BYTE, bufferFace );
	}

	glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	glGenerateMipmap( GL_TEXTURE_CUBE_MAP );

	glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, m_params.tiling );
	glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, m_params.tiling );
}

void CubeTexture::bind( u16 i_unit ) const
{
	glActiveTexture( GL_TEXTURE0 + i_unit );
	glBindTexture( GL_TEXTURE_CUBE_MAP, m_hTexture );
}

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////