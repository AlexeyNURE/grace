#include "precompiled.hpp"

#include "textureUtils.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc::utils
{

//-----------------------------------------------------------------------------

void gc::utils::initOffsets( std::pair<u32, u32>* _offsets, u16 _width )
{
	// X
	_offsets[0].first = _width * 2;
	_offsets[0].second = _width;
	// -X
	_offsets[1].first = 0;
	_offsets[1].second = _width;
	// Y
	_offsets[2].first = _width;
	_offsets[2].second = 0;
	// -Y
	_offsets[3].first = _width;
	_offsets[3].second = _width * 2;
	// Z
	_offsets[4].first = _width;
	_offsets[4].second = _width;
	// -Z
	_offsets[5].first = _width * 3;
	_offsets[5].second = _width;
}

void gc::utils::extractFace( const unsigned char* i_bufferTga, unsigned char* o_bufferFace, u64 i_faceWidth, u16 i_width, u32 i_hOffset, u32 i_vOffset, s32 i_bpp )
{
	const u64 faceWidthBytes = i_faceWidth * i_bpp;
	const u64 hOffsetBytes = i_hOffset * i_bpp;

	for( u64 i = 0; i < i_faceWidth; ++i )
	{
		const u64 vOffsetBytes = i_width * i_bpp * ( i_vOffset + i );

		const unsigned char* pSrc = i_bufferTga + hOffsetBytes + vOffsetBytes;
		unsigned char* pDst = o_bufferFace + faceWidthBytes * i;

		memcpy_s( pDst, faceWidthBytes, pSrc, faceWidthBytes );
	}
}

//-----------------------------------------------------------------------------

} // namespace gc::utils

///////////////////////////////////////////////////////////////////////////////