#ifndef __TEXTURE_HPP__
#define __TEXTURE_HPP__

#include <boost/noncopyable.hpp>
#include <string>

#include "grace/core/utils/basic_types.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

struct TextureParams
{
	s32 width;
	s32 height;
	s32 bpp;

	s32 tiling;
	s32 format;
};

//-----------------------------------------------------------------------------

class Texture : boost::noncopyable
{
public:

	virtual ~Texture() = default;
	virtual void bind( u16 _unit ) const = 0;

	virtual u32 getTextureHandle() const = 0;
};

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////

#endif