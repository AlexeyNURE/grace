#include "precompiled.hpp"

#include <glad/glad.h>
#include <fstream>

#include "grace/render/shader/shader.hpp"
#include "grace/core/utils/utils.hpp"
#include "grace/render/openGL/vertex.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//------------------------------------------------------------------------------

Shader::~Shader()
{
	glDeleteProgram( m_program );
	glDeleteShader( m_vertexShader );
	glDeleteShader( m_fragmentShader );
}

int Shader::init( std::string_view i_fileVertexShader, std::string_view i_fileFragmentShader, ShaderKind i_kind )
{
	loadShader( m_vertexShader, i_fileVertexShader );
	loadShader( m_fragmentShader, i_fileFragmentShader );
	loadProgram();

	initTexUniforms( i_kind );

	m_kind = i_kind;

	return 0;
}

bool Shader::checkShaderCompliled( u32 i_shader )
{
	s32 status;
	glGetShaderiv( i_shader, GL_COMPILE_STATUS, &status );

	if( !status )
	{
		s32 messageLength;
		glGetShaderiv( i_shader, GL_INFO_LOG_LENGTH, &messageLength );

		if( messageLength > 1 )
		{
			char* message = new char[messageLength];

			glGetShaderInfoLog( i_shader, messageLength, nullptr, message );
			LOG_ERROR( "Shader compile error message: {}", message );

			delete[] message;
		}

		return false;
	}

	return true;
}

bool Shader::checkProgramLinkage()
{
	s32 status;
	glGetProgramiv( m_program, GL_LINK_STATUS, &status );

	if( !status )
	{
		s32 messageLength;
		glGetProgramiv( m_program, GL_INFO_LOG_LENGTH, &messageLength );

		if( messageLength > 1 )
		{
			char* message = new char[messageLength];

			glGetProgramInfoLog( m_program, messageLength, nullptr, message );
			LOG_INFO( "Shading program link error message: {}", message );

			delete[] message;
		}

		return false;
	}

	return true;
}

gc::s32 Shader::getUniformLocation( std::string_view i_name ) const
{
	auto it = m_uniformLocations.find( i_name.data() );
	if( it != m_uniformLocations.end() )
	{
		return it->second;
	}

	gc::s32 location = glGetUniformLocation( m_program, i_name.data() );
	m_uniformLocations[i_name.data()] = location;

	return location;
}

std::string Shader::getFileContent( std::string_view i_fileName )
{
	std::string fileContent;

	std::ifstream file( i_fileName.data(), std::ios::in );
	ASSERT( file.is_open(), "Can't open shader file {}", i_fileName.data() );
	if( file.is_open() )
	{
		std::stringstream buffer;
		buffer << file.rdbuf();
		fileContent = buffer.str();
	}

	return fileContent;
}

void Shader::loadShader( u32 &o_shader, std::string_view i_fileName )
{
	std::string fileContent = getFileContent( i_fileName );
	if( !fileContent.empty() )
	{
		const char* fileContentStr = fileContent.data();
		if( i_fileName.substr( i_fileName.length() - 3 ) == ".vs" )
			o_shader = glCreateShader( GL_VERTEX_SHADER );
		else
			o_shader = glCreateShader( GL_FRAGMENT_SHADER );
		glShaderSource( o_shader, 1, &fileContentStr, nullptr );
		glCompileShader( o_shader );
	}

	if( !checkShaderCompliled( o_shader ) )
	{
		glDeleteShader( o_shader );
		o_shader = -1;

		ASSERT( false, "Couldn't load shader {}", i_fileName );
	}
}

void Shader::loadProgram()
{
	m_program = glCreateProgram();

	glAttachShader( m_program, m_vertexShader );
	glAttachShader( m_program, m_fragmentShader );
	glLinkProgram( m_program );

	const bool linkedSuccessful = checkProgramLinkage();

	glDetachShader( m_program, m_vertexShader );
	glDetachShader( m_program, m_fragmentShader );

	glDeleteShader( m_vertexShader );
	glDeleteShader( m_fragmentShader );

	if( !linkedSuccessful )
	{
		glDeleteProgram( m_program );
		m_program = -1;

		ASSERT( false, "Couldn't link program" );
	}
}

void Shader::initTexUniforms( ShaderKind i_kind )
{
	switch( i_kind )
	{
	case ShaderKind::simple:
		break;
	case ShaderKind::triangle:
	case ShaderKind::material:
		initTriangleShader();
		break;
	case ShaderKind::terrain:
		initTerrainShader();
		break;
	case ShaderKind::skyBox:
		initSkyBoxShader();
		break;
	case ShaderKind::reflection:
		initReflectionShader();
	case ShaderKind::normalMaterial:
		initNormalShader();
		break;
	case ShaderKind::UVDisplacement:
		initUVDisplacementShader();
		break;
	case ShaderKind::normal:
		initNormalShader();
		break;
	case ShaderKind::water:
		initWaterShader();
		break;
	case ShaderKind::blackWhite:
		initBWShader();
		break;
	case ShaderKind::blur:
		initBlurShader();
		break;
	case ShaderKind::preBloom:
		initPreBloomShader();
		break;
	case ShaderKind::postBloom:
		initPostBloomShader();
		break;
	case ShaderKind::DOFBlur:
		initDOFBlurShader();
		break;
	case ShaderKind::DOF:
		initDOFShader();
		break;
	default:
		ASSERT( false, "Invalid shader kind" );
		break;
	}
}

void Shader::initTriangleShader()
{
	m_textureUniforms.push_back( "u_texture" );
}

void Shader::initTerrainShader()
{
	m_textureUniforms.push_back( "u_blend_map" );
	m_textureUniforms.push_back( "u_grass" );
	m_textureUniforms.push_back( "u_dirt" );
	m_textureUniforms.push_back( "u_rock" );
}

void Shader::initSkyBoxShader()
{
	m_textureUniforms.push_back( "u_s_texture" );
}

void Shader::initReflectionShader()
{
	m_textureUniforms.push_back( "u_texture" );
}

void Shader::initUVDisplacementShader()
{
	m_textureUniforms.push_back( "u_displacement_map" );
	m_textureUniforms.push_back( "u_fire_mask" );
	m_textureUniforms.push_back( "u_fire" );
}

void Shader::initNormalShader()
{
	m_textureUniforms.push_back( "u_texture" );
	m_textureUniforms.push_back( "u_normalMapTex" );
}

void Shader::initWaterShader()
{
	m_textureUniforms.push_back( "u_rock" );
	m_textureUniforms.push_back( "u_normalMapTex" );
	m_textureUniforms.push_back( "u_displacement_map" );
	m_textureUniforms.push_back( "u_cubeTexture" );
}

void Shader::initBWShader()
{
	m_textureUniforms.push_back( "u_texture" );
}

void Shader::initBlurShader()
{
	m_textureUniforms.push_back( "u_colorTex" );
}

void Shader::initPreBloomShader()
{
	m_textureUniforms.push_back( "u_colorTex" );
}

void Shader::initPostBloomShader()
{
	m_textureUniforms.push_back( "u_colorBuffer" );
	m_textureUniforms.push_back( "u_blurredCBuffer" );
}

void Shader::initDOFBlurShader()
{
	m_textureUniforms.push_back( "u_texOriginal" );
	m_textureUniforms.push_back( "u_texBlurred" );
	m_textureUniforms.push_back( "u_texDepth" );
}

void Shader::initDOFShader()
{
	m_textureUniforms.push_back( "u_texOriginal" );
	m_textureUniforms.push_back( "u_texBlurred" );
	m_textureUniforms.push_back( "u_texDepth" );
}

bool gc::Shader::hasUniform( std::string_view i_name ) const
{
	return getUniformLocation( i_name.data() ) != -1;
}

gc::u64 Shader::getUniformsCount() const
{
	return m_textureUniforms.size();
}

bool Shader::hasFogParams() const
{
	return
		hasUniform( "u_fogColor" ) &&
		hasUniform( "u_fogRange" ) &&
		hasUniform( "u_fogStart" );
}

bool Shader::hasLightParams() const
{
	return
		hasUniform( "u_ambientColor" )	&&
		hasUniform( "u_ambientWeight" ) &&
		hasUniform( "u_dirColors" ) &&
		hasUniform( "u_pointColors" ) &&
		hasUniform( "u_lightDirections" ) &&
		hasUniform( "u_lightPositions" );
}

bool Shader::hasWaterParams() const
{
	return
		hasUniform( "u_waterColor" ) &&
		hasUniform( "u_depthAdjust" ) &&
		hasUniform( "u_depthDispl" ) &&
		hasUniform( "u_dMax" );
}

bool Shader::hasDOFParams() const
{
	return
		hasUniform( "u_clarity" ) &&
		hasUniform( "u_fade" ) &&
		hasUniform( "u_far" ) &&
		hasUniform( "u_near" ) &&
		hasUniform( "u_step" );
}

void Shader::bind() const
{
	glUseProgram( m_program );
}

void Shader::uniformValue( std::string_view i_name, s32 i_val ) const
{
	glUniform1i( getUniformLocation( i_name ), i_val );
}

void Shader::uniformValue( std::string_view i_name, float i_val ) const
{
	glUniform1f( getUniformLocation( i_name ), i_val );
}

void Shader::uniformValue( std::string_view i_name, const glm::vec2& i_val ) const
{
	glUniform2fv( getUniformLocation( i_name ), 1, reinterpret_cast<const float*>( &i_val ) );
}

void Shader::uniformValue( std::string_view i_name, const glm::vec3& i_val ) const
{
	glUniform3fv( getUniformLocation( i_name ), 1, reinterpret_cast<const float*>( &i_val ) );
}

void Shader::uniformValue( std::string_view i_name, const glm::vec4& i_val ) const
{
	glUniform4fv( getUniformLocation( i_name ), 1, reinterpret_cast<const float*>( &i_val ) );
}

void Shader::uniformValue( std::string_view i_name, const glm::mat4& i_val ) const
{
	glUniformMatrix4fv( getUniformLocation( i_name ), 1, false, reinterpret_cast<const float*>( &i_val ) );
}

void Shader::uniformValue( std::string_view i_name, const std::vector<s32>& i_val ) const
{
	glUniform1iv( getUniformLocation( i_name ), static_cast<s32>( i_val.size() ), i_val.data() );
}

void Shader::uniformValue( std::string_view i_name, const std::vector<glm::vec3>& i_val ) const
{
	glUniform3fv( getUniformLocation( i_name ), static_cast<s32>( i_val.size() ), reinterpret_cast< const float* >( i_val.data() ) );
}

void Shader::uniformValue( std::string_view i_name, const std::vector<glm::vec4>& i_val ) const
{
	glUniform4fv( getUniformLocation( i_name ), static_cast<s32>( i_val.size() ), reinterpret_cast< const float* >( i_val.data() ) );
}

void Shader::uniformValue( std::string_view i_name, u32 i_count, const s32* i_val ) const
{
	glUniform1iv( getUniformLocation( i_name ), i_count, i_val );
}

void Shader::uniformValue( std::string_view i_name, u32 i_count, const glm::vec3* i_val ) const
{
	glUniform3fv( getUniformLocation( i_name ), i_count, reinterpret_cast<const float*>( i_val ) );
}

void Shader::uniformValue( std::string_view i_name, u32 i_count, const glm::vec4* i_val ) const
{
	glUniform4fv( getUniformLocation( i_name ), i_count, reinterpret_cast<const float*>( i_val ) );
}

//------------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////