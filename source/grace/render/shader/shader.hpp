#pragma once

#include <unordered_map>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <boost/noncopyable.hpp>

#include "grace/core/utils/basic_types.hpp"

#include "grace/render/shader/shader_kind.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//------------------------------------------------------------------------------

class Shader : boost::noncopyable
{
public:
	Shader() = default;
	~Shader();

	int init( std::string_view i_fileVertexShader, std::string_view i_fileFragmentShader, ShaderKind i_kind );

	static std::string getFileContent( std::string_view i_fileName );
	void loadShader( u32& i_shader, std::string_view i_fileName );
	void loadProgram();

	u32 getProgram() const;
	u32 getVertexShader() const;
	u32 getFragmentShader() const;

	std::string_view getTextureUnifrormName( size_t i_idx ) const;

	ShaderKind getShaderKind() const;

	bool hasFogParams() const;
	bool hasLightParams() const;
	bool hasWaterParams() const;
	bool hasDOFParams() const;

	void bind() const;

	void uniformValue( std::string_view i_name, s32 i_val ) const;
	void uniformValue( std::string_view i_name, float i_val ) const;
	void uniformValue( std::string_view i_name, const glm::vec2& i_val ) const;
	void uniformValue( std::string_view i_name, const glm::vec3& i_val ) const;
	void uniformValue( std::string_view i_name, const glm::vec4& i_val ) const;
	void uniformValue( std::string_view i_name, const glm::mat4& i_val ) const;

	void uniformValue( std::string_view i_name, const std::vector<s32>& i_val ) const;
	void uniformValue( std::string_view i_name, const std::vector<glm::vec3>& i_val ) const;
	void uniformValue( std::string_view i_name, const std::vector<glm::vec4>& i_val ) const;
	void uniformValue( std::string_view i_name, const std::vector<glm::vec3*>& i_val ) const;
	void uniformValue( std::string_view i_name, const std::vector<glm::vec4*>& i_val ) const;
	void uniformValue( std::string_view i_name, u32 i_count, const s32* i_val ) const;
	void uniformValue( std::string_view i_name, u32 i_count, const glm::vec3* i_val ) const;
	void uniformValue( std::string_view i_name, u32 i_count, const glm::vec4* i_val ) const;

	bool hasUniform( std::string_view i_name ) const;

	gc::u64 getUniformsCount() const;

private:

	bool checkShaderCompliled( u32 i_shader );
	bool checkProgramLinkage();

	gc::s32 getUniformLocation( std::string_view i_name ) const;
	
	void initTexUniforms( ShaderKind i_kind );
	void initTriangleShader();
	void initTerrainShader();
	void initSkyBoxShader();
	void initReflectionShader();
	void initUVDisplacementShader();
	void initNormalShader();
	void initWaterShader();
	void initBWShader();
	void initBlurShader();
	void initPreBloomShader();
	void initPostBloomShader();
	void initDOFBlurShader();
	void initDOFShader();

	u32 m_program;
	u32 m_vertexShader;
	u32 m_fragmentShader;

	std::vector<std::string> m_textureUniforms;
	ShaderKind m_kind;

	mutable std::unordered_map<std::string, gc::s32> m_uniformLocations;
};

//------------------------------------------------------------------------------

inline u32 Shader::getProgram() const
{
	return m_program;
}

inline u32 Shader::getVertexShader() const
{
	return m_vertexShader;
}

inline u32 Shader::getFragmentShader() const
{
	return m_fragmentShader;
}

inline std::string_view Shader::getTextureUnifrormName( size_t i_idx ) const
{
	return m_textureUniforms[i_idx];
}

inline ShaderKind Shader::getShaderKind() const
{
	return m_kind;
}

//------------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////