#pragma once

///////////////////////////////////////////////////////////////////////////////

#include "grace/core/utils/enum_utils.hpp"
#include "grace/core/utils/basic_types.hpp"

namespace gc
{

//------------------------------------------------------------------------------

REGISTER_ENUM_CLASS(
	ShaderKind,
	material,
	normalMaterial,
	simple,
	triangle,
	terrain,
	skyBox,
	reflection,
	UVDisplacement,
	normal,
	water,
	blackWhite,
	blur,
	DOF,
	DOFBlur,
	postBloom,
	preBloom,
	effectVS
)

//------------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////