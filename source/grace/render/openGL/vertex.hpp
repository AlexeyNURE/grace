#pragma once

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//-----------------------------------------------------------------------------

struct Vertex
{
	glm::vec3 pos{ 0.f };
	glm::vec2 uv{ 0.f };
	glm::vec3 normal{ 0.f };
	glm::vec3 bitangent{ 0.f };
	glm::vec3 tangent{ 0.f };
};

} // namespace utils

//-----------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////