#include "precompiled.hpp"

#include <glad/glad.h>
#include "grace/render/openGL/buffer.hpp"
#include "grace/render/openGL/vertex.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

VertexBuffer::VertexBuffer( u64 i_size ) :
	m_count( i_size )
{
	glGenBuffers( 1, &m_hRendererId );
}

VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers( 1, &m_hRendererId );
}

void VertexBuffer::bind() const
{
	glBindBuffer( GL_ARRAY_BUFFER, m_hRendererId );
}

void VertexBuffer::unbind() const
{
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

void VertexBuffer::bufferData( const utils::Vertex* i_vertices, u64 i_size ) const
{
	glBufferData( GL_ARRAY_BUFFER, sizeof( utils::Vertex ) * i_size, i_vertices, GL_STATIC_DRAW );
}

//-----------------------------------------------------------------------------

IndexBuffer::IndexBuffer( u64 i_count ) :
	m_count( i_count )
{
	glGenBuffers( 1, &m_hRendererId );
}

IndexBuffer::~IndexBuffer()
{
	glDeleteBuffers( 1, &m_hRendererId );
}

void IndexBuffer::bind() const
{
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_hRendererId );
}

void IndexBuffer::unbind() const
{
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void IndexBuffer::bufferData( const u32* i_indices, u64 i_size ) const
{
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, i_size * sizeof( u32 ), i_indices, GL_STATIC_DRAW );
}

//-----------------------------------------------------------------------------

}; // namespace gc

///////////////////////////////////////////////////////////////////////////////
