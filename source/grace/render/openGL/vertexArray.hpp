#pragma once

#include "grace/core/utils/basic_types.hpp"
#include "grace/render/openGL/buffer.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{
struct Vertex;
};

//-----------------------------------------------------------------------------

class VertexArray
{
public:
	VertexArray( const utils::Vertex* _vertices, u64 verticesCount, const u32* _indices, u64 _indicesCount );
	~VertexArray();

	void bind() const;
	void unbind() const;

	u64 getIndicesCount() const;
	u64 getVerticesCount() const;

private:
	void applyLayout() const;

	u32 m_hRendererId;

	VertexBuffer m_vertexBuffer;
	IndexBuffer m_indexBuffer;
};

//-----------------------------------------------------------------------------

inline u64 VertexArray::getIndicesCount() const
{
	return m_indexBuffer.getCount();
}

inline u64 VertexArray::getVerticesCount() const
{
	return m_vertexBuffer.getCount();
}

//-----------------------------------------------------------------------------

}; // namespace gc

///////////////////////////////////////////////////////////////////////////////