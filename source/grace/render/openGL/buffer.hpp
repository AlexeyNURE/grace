#pragma once

#include "grace/core/utils/basic_types.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{
struct Vertex;
};

//-----------------------------------------------------------------------------

class Buffer
{
public:
	virtual ~Buffer() = default;

	virtual void bind() const = 0;
	virtual void unbind() const = 0;
};

//-----------------------------------------------------------------------------

class VertexBuffer : public Buffer
{
public:
	VertexBuffer( u64 i_size );
	virtual ~VertexBuffer();

	void bind() const override;
	void unbind() const override;

	void bufferData( const utils::Vertex* i_vertices, u64 i_size ) const;

	u64 getCount() const;

private:
	u32 m_hRendererId;
	u64 m_count;
};

//-----------------------------------------------------------------------------

class IndexBuffer : public Buffer
{
public:
	IndexBuffer( u64 i_count );
	virtual ~IndexBuffer();

	void bind() const override;
	void unbind() const override;

	void bufferData( const u32* i_indices, u64 i_size ) const;

	u64 getCount() const;

private:
	u32 m_hRendererId;
	u64 m_count;
};

//-----------------------------------------------------------------------------

inline u64 VertexBuffer::getCount() const
{
	return m_count;
}

//-----------------------------------------------------------------------------

inline u64 IndexBuffer::getCount() const
{
	return m_count;
}

//-----------------------------------------------------------------------------

}; // namespace gc

///////////////////////////////////////////////////////////////////////////////