#include "precompiled.hpp"

#include <glad/glad.h>
#include "grace/render/openGL/vertexArray.hpp"
#include "grace/render/openGL/vertex.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

VertexArray::VertexArray( const utils::Vertex* _vertices, u64 _verticesCount, const u32* _indices, u64 _indicesCount ) :
	m_vertexBuffer( _verticesCount ),
	m_indexBuffer( _indicesCount )
{
	glGenVertexArrays( 1, &m_hRendererId );
	bind();

	m_vertexBuffer.bind();
	m_vertexBuffer.bufferData( _vertices, _verticesCount );
	m_indexBuffer.bind();
	m_indexBuffer.bufferData( _indices, _indicesCount );

	applyLayout();
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays( 1, &m_hRendererId );
}

void VertexArray::bind() const
{
	glBindVertexArray( m_hRendererId );
}

void VertexArray::unbind() const
{
	glBindVertexArray( 0 );
}

void VertexArray::applyLayout() const
{
	constexpr u32 k_vertexAttribsCount = 5;
	std::array<std::pair<u32/*index*/, u32/*offset*/>, k_vertexAttribsCount> vertexData =
	{
		std::pair{0,3}, // pos
		std::pair{1,2}, // uv
		std::pair{2,3}, // norm
		std::pair{3,3}, // bitangent
		std::pair{4,3}  // tangent
	};

	const float* offset = nullptr;
	for( auto [index, size] : vertexData )
	{
		glEnableVertexAttribArray( index );
		glVertexAttribPointer( index, size, GL_FLOAT, GL_FALSE, sizeof( utils::Vertex ), offset );

		offset += size;
	}
}

//-----------------------------------------------------------------------------

}; // namespace gc

///////////////////////////////////////////////////////////////////////////////