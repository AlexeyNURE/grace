#pragma once

#include "grace/core/utils/basic_types.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//-----------------------------------------------------------------------------

struct Globals
{
	static u32 s_screenWidth;
	static u32 s_screenHeight;
	static float s_totalTime;
};

//-----------------------------------------------------------------------------

} // namespace gc