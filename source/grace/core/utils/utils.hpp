#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <cassert>
#include "grace/core/utils/logging/log.hpp"
#include "grace/core/utils/basic_types.hpp"

//------------------------------------------------------------------------------

#ifdef _DEBUG
#define ENABLE_ASSERTS
#endif

namespace gc
{

namespace utils
{

template<typename ... Args>
void doAssert( bool i_isCore, std::string_view i_message, std::string_view i_file, u32 i_line, Args&& ... i_args )
{
	std::string message = fmt::format( i_message.data(), std::forward<Args>( i_args )... );

	if( i_isCore )
		LOG_CORE_ERROR( "Core Assertion Failed: {0} (FILE: {1} LINE: {2})", i_message, i_file, i_line );
	else
		LOG_ERROR( "Assertion Failed: {0} (FILE: {1} LINE: {2})", i_message, i_file, i_line );

	__debugbreak();
}

} // namespace utils

} // namespace gc

#ifdef ENABLE_ASSERTS
	#define ASSERT_NO_MESSAGE(expression, ...) { if(!(expression)) { gc::utils::doAssert( false, "No message provided!", __FILE__, __LINE__, __VA_ARGS__ ); } }
	#define ASSERT_MESSAGE(expression, message, ...) { if(!(expression)) { gc::utils::doAssert( false, message, __FILE__, __LINE__, __VA_ARGS__ ); } }

	#define ASSERT_INTERNAL( x, expression, message, FUNC, ... ) FUNC

	#define ASSERT(...)	ASSERT_INTERNAL(,##__VA_ARGS__,			\
						ASSERT_MESSAGE(__VA_ARGS__),			\
						ASSERT_NO_MESSAGE(__VA_ARGS__) )		\

	#define CORE_ASSERT(expression, message, ...) { if(!(expression)) { gc::utils::doAssert( true, message, __FILE__, __LINE__, __VA_ARGS__ ); } }
#else
	#define ASSERT(x, ...)
	#define CORE_ASSERT(x, ...)
#endif

//------------------------------------------------------------------------------

#define ASSERT_THROWS( TESTED_CODE, EXPECTED_EXCEPTION )	\
	try														\
	{														\
		{ TESTED_CODE; }									\
		ASSERT( ! "Exception must have been thrown" );		\
	}														\
	catch ( const EXPECTED_EXCEPTION & )					\
	{														\
	}														\
	catch ( const std::exception & )						\
	{														\
		ASSERT( ! "Wrong Exception has been thrown" );		\
	}

//-----------------------------------------------------------------------------

#define STR2_(x) #x
#define STR1_(x) STR2_(x)
#define TODO(str)														\
	__pragma(message (__FILE__"("STR1_(__LINE__)"): TODO: " str))		\

//------------------------------------------------------------------------------

#endif // __UTILS_HPP__