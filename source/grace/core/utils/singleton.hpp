#pragma once

#include <boost/noncopyable.hpp>

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

//------------------------------------------------------------------------------

template<typename T>
class Singleton : boost::noncopyable
{
public:
	static T& getInstance();

protected:
	Singleton() = default;
	~Singleton() = default;
};

template<typename T>
inline T& Singleton<T>::getInstance()
{
	static T s_instance;
	return s_instance;
}

//------------------------------------------------------------------------------

} // namespace gc

///////////////////////////////////////////////////////////////////////////////