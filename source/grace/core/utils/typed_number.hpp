#pragma once
#include <type_traits>

//-----------------------------------------------------------------------------

namespace utils
{

//-----------------------------------------------------------------------------

template<typename Traits>
class TypedNumber
{
public:
	using base_t = typename Traits::base_t;

public:
	TypedNumber();
	explicit TypedNumber( const typename base_t& i_id );
	TypedNumber( const TypedNumber& i_id );
	TypedNumber& operator =( const TypedNumber& i_other );
	TypedNumber( TypedNumber&& i_id );
	TypedNumber& operator =( TypedNumber&& i_other );

	const typename base_t& value() const;

	bool operator ==( const TypedNumber& i_other ) const;
	bool operator !=( const TypedNumber& i_other ) const;

	bool operator <( const TypedNumber& i_other ) const;
	bool operator >( const TypedNumber& i_other ) const;
	bool operator <=( const TypedNumber& i_other ) const;
	bool operator >=( const TypedNumber& i_other ) const;

	TypedNumber& operator +=( const TypedNumber& i_other );
	TypedNumber& operator -=( const TypedNumber& i_other );
	TypedNumber& operator *=( const TypedNumber& i_other );
	TypedNumber& operator /=( const TypedNumber& i_other );
	TypedNumber& operator ++();
	TypedNumber operator ++( int );
	TypedNumber& operator --();
	TypedNumber operator --( int );

	void swap( TypedNumber& i_other );

	static const TypedNumber<Traits> k_default;

	static const TypedNumber& zero();

private:
	typename base_t m_value = Traits::getDefault();

	template<typename T>
	friend struct std::hash;
};

//-----------------------------------------------------------------------------

template<typename Traits>
const TypedNumber<Traits> TypedNumber<Traits>::k_default( Traits::getDefault() );

//-----------------------------------------------------------------------------

template<typename Traits>
TypedNumber<Traits> operator +( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs );
template<typename Traits>
TypedNumber<Traits> operator -( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs );
template<typename Traits>
TypedNumber<Traits> operator *( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs );
template<typename Traits>
TypedNumber<Traits> operator /( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs );

//-----------------------------------------------------------------------------

} // namespace utils

#include "typed_number.inl"

//-----------------------------------------------------------------------------

#define DECLARE_TYPED_NUMBER( Name, Type, DefaultValue )						\
struct TraitsFor##Name															\
{																				\
	using base_t = Type;														\
	static_assert(																\
			std::is_arithmetic_v<base_t>,										\
			"Base type should be arithmetic for TypedNumber" );					\
	static const Type& getDefault()												\
	{																			\
		static const Type k_defaultValue = DefaultValue;						\
		return k_defaultValue;													\
	}																			\
																				\
	template<typename Traits>													\
	friend class utils::TypedNumber;											\
};																				\
using Name = utils::TypedNumber<TraitsFor##Name>;								\

//-----------------------------------------------------------------------------