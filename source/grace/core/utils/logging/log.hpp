#ifndef __LOG_HPP__
#define __LOG_HPP__

#include <spdlog/fmt/ostr.h>
#include <spdlog/spdlog.h>

//////////////////////////////////////////////////////////////////////////

namespace gc
{

//------------------------------------------------------------------------

class Log
{
public:
	static void Init();

	inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
	inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
private:
	static std::shared_ptr<spdlog::logger> s_CoreLogger;
	static std::shared_ptr<spdlog::logger> s_ClientLogger;
};

//------------------------------------------------------------------------

} // namespace gc

//------------------------------------------------------------------------

// Core log macros
#define LOG_CORE_TRACE(...)    ::gc::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define LOG_CORE_INFO(...)     ::gc::Log::GetCoreLogger()->info(__VA_ARGS__)
#define LOG_CORE_WARN(...)     ::gc::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define LOG_CORE_ERROR(...)    ::gc::Log::GetCoreLogger()->error(__VA_ARGS__)
#define LOG_CORE_CRITICAL(...) ::gc::Log::GetCoreLogger()->critical(__VA_ARGS__)

// Client log macros
#define LOG_TRACE(...)         ::gc::Log::GetClientLogger()->trace(__VA_ARGS__)
#define LOG_INFO(...)          ::gc::Log::GetClientLogger()->info(__VA_ARGS__)
#define LOG_WARN(...)          ::gc::Log::GetClientLogger()->warn(__VA_ARGS__)
#define LOG_ERROR(...)         ::gc::Log::GetClientLogger()->error(__VA_ARGS__)
#define LOG_CRITICAL(...)      ::gc::Log::GetClientLogger()->critical(__VA_ARGS__)

//------------------------------------------------------------------------

#endif // __LOG_HPP__

//////////////////////////////////////////////////////////////////////////
