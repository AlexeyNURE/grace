#include "typed_number.hpp"

namespace utils
{

//-----------------------------------------------------------------------------

template<typename Traits>
inline TypedNumber<Traits>::TypedNumber() :
	m_value( Traits::getDefault() )
{
}

template<typename Traits>
inline TypedNumber<Traits>::TypedNumber( const base_t& i_id ) :
	m_value( i_id )
{
	static_assert( std::is_arithmetic_v<base_t>, "Base type should be arithmetic for TypedNumber" );
}

template<typename Traits>
inline TypedNumber<Traits>::TypedNumber( TypedNumber&& i_other ) :
	m_value( i_other.m_value )
{
	i_other.m_value = Traits::getDefault();
}

template<typename Traits>
inline TypedNumber<Traits>::TypedNumber( const TypedNumber<Traits>& i_id ) :
	m_value( i_id.m_value )
{
}

template<typename Traits>
inline const typename TypedNumber<Traits>::base_t& TypedNumber<Traits>::value() const
{
	return m_value;
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator=( const TypedNumber& i_other )
{
	if( *this == i_other )
		return *this;

	m_value = i_other.m_value;

	return *this;
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator=( TypedNumber&& i_other )
{
	if( *this == i_other )
		return *this;

	std::swap( m_value, i_other.m_value );

	return *this;
}

template<typename Traits>
inline bool TypedNumber<Traits>::operator ==( const TypedNumber& i_other ) const
{
	return m_value == i_other.m_value;
}

template<typename Traits>
inline bool TypedNumber<Traits>::operator !=( const TypedNumber& i_other ) const
{
	return !(*this == i_other);
}

template<typename Traits>
inline bool TypedNumber<Traits>::operator <( const TypedNumber& i_other ) const
{
	return m_value < i_other.m_value;
}

template<typename Traits>
inline bool TypedNumber<Traits>::operator >( const TypedNumber& i_other ) const
{
	return i_other < *this;
}

template<typename Traits>
inline bool TypedNumber<Traits>::operator <=( const TypedNumber& i_other ) const
{
	return !( *this > i_other );
}

template<typename Traits>
inline bool TypedNumber<Traits>::operator >=( const TypedNumber& i_other ) const
{
	return !( *this < i_other );
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator +=( const TypedNumber& i_other )
{
	m_value += i_other.m_value;
	return *this;
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator -=( const TypedNumber& i_other )
{
	m_value -= i_other.m_value;
	return *this;
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator *=( const TypedNumber& i_other )
{
	m_value *= i_other.m_value;
	return *this;
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator /=( const TypedNumber& i_other )
{
	m_value /= i_other.m_value;
	return *this;
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator ++()
{
	m_value++;
	return *this;
}

template<typename Traits>
inline TypedNumber<Traits> TypedNumber<Traits>::operator ++( int )
{
	TypedNumber<Traits> res( *this );
	operator++();
	return res;
}

template<typename Traits>
inline TypedNumber<Traits>& TypedNumber<Traits>::operator --()
{
	m_value--;
	return *this;
}

template<typename Traits>
inline TypedNumber<Traits> TypedNumber<Traits>::operator --( int )
{
	TypedNumber<Traits> res( *this );
	operator--();
	return res;
}

template<typename Traits>
inline TypedNumber<Traits> operator +( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs )
{
	TypedNumber<Traits> res( i_lhs.value() + i_rhs.value() );
	return res;
}

template<typename Traits>
inline TypedNumber<Traits> operator -( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs )
{
	TypedNumber<Traits> res( i_lhs.value() - i_rhs.value() );
	return res;
}

template<typename Traits>
inline TypedNumber<Traits> operator *( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs )
{
	TypedNumber<Traits> res( i_lhs.value() * i_rhs.value() );
	return res;
}

template<typename Traits>
inline TypedNumber<Traits> operator /( const TypedNumber<Traits>& i_lhs, const TypedNumber<Traits>& i_rhs )
{
	TypedNumber<Traits> res( i_lhs.value() / i_rhs.value() );
	return res;
}

template<typename Traits>
inline void TypedNumber<Traits>::swap( TypedNumber& i_other )
{
	TypedNumber<Traits> temp = std::move( *this );
	*this = std::move( i_other );
	i_other = std::move( temp );
}

template<typename Traits>
inline const TypedNumber<Traits>& TypedNumber<Traits>::zero()
{
	static const TypedNumber<Traits> k_zero = TypedNumber<Traits>{ 0 };

	return k_zero;
}

} // namespace utils

//-----------------------------------------------------------------------------

namespace std
{

template <typename Traits>
struct hash<utils::TypedNumber<Traits>>
{
	std::size_t operator()( const utils::TypedNumber<Traits>& i_id ) const
	{
		return std::hash<Traits::base_t>()( i_id.value() );
	}
};

} // namespace std

//-----------------------------------------------------------------------------