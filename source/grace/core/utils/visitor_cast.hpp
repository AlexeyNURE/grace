#pragma once

#include <boost/optional/optional.hpp>

//------------------------------------------------------------------------------

template<typename _Type>
struct ConstTraits
{
	using ResultType = const _Type&;
};

//------------------------------------------------------------------------------

template<typename _Type>
struct NonConstTraits
{
	using ResultType = _Type&;
};

//------------------------------------------------------------------------------

template<
		typename _TargetType,
		typename _SourceRootType,
		typename _DefaultVisitor,
		template<typename> class _Traits = ConstTraits
>
class Cast
	:	private _DefaultVisitor
{
public:

	//------------------------------------------------------------------------------

	using TargetType = typename _Traits<_TargetType>::ResultType;

	using SourceRootType = typename _Traits<_SourceRootType>::ResultType;

	//------------------------------------------------------------------------------

	boost::optional<TargetType> cast( SourceRootType o_sourceObject )
	{
		m_castResult.reset();
		o_sourceObject.accept( *this );
		return m_castResult;
	}

	//------------------------------------------------------------------------------

private:

	//------------------------------------------------------------------------------

	virtual void visit( TargetType i_targetObject )
	{
		m_castResult = i_targetObject;
	}

	//------------------------------------------------------------------------------

	boost::optional<TargetType> m_castResult;

	//------------------------------------------------------------------------------

};

//------------------------------------------------------------------------------

#define DEFINE_CAST( _construct )											\
template<typename _TargetType>												\
class _construct##ConstCast													\
:	public Cast<_TargetType, _construct, _construct##DefaultConstVisitor>	\
{																			\
};																			\
																			\
template<typename _TargetType>												\
class _construct##Cast														\
:	public Cast<_TargetType, _construct, _construct##DefaultVisitor, NonConstTraits> \
{																			\
};																			\

//------------------------------------------------------------------------------