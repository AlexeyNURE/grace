#pragma once

#include "grace/core/utils/utils.hpp"
#include "grace/core/utils/basic_types.hpp"

//-----------------------------------------------------------------------------

namespace gc
{

//-----------------------------------------------------------------------------

template<size_t ElementsCount>
constexpr std::array<std::string_view, ElementsCount>
getEnumNames( std::string_view i_enumNames ) noexcept
{
	std::array<std::string_view, ElementsCount> result{ "" };

	constexpr std::string_view delim = ", ";

	std::size_t start = 0;
	for( std::size_t i = 0; i < ElementsCount; i++ )
	{
		const std::size_t end = i_enumNames.find( delim, start );
		result[i] = i_enumNames.substr( start, end - start );
		start = end + delim.size();
	}

	return result;
}

#define REGISTER_ENUM_CLASS( EnumName, ... )											\
enum class EnumName																		\
{																						\
	__VA_ARGS__,																		\
	COUNT,																				\
	INVALID = COUNT,																	\
	FIRST = 0,																			\
	LAST = COUNT - 1																	\
};																						\
																						\
constexpr std::string_view toString( EnumName type ) noexcept							\
{																						\
	constexpr std::string_view k_namesStr{ #__VA_ARGS__ };								\
	constexpr std::size_t k_count{ static_cast<std::size_t>( EnumName::COUNT ) };		\
																						\
	constexpr auto k_enumNames{ gc::getEnumNames<k_count>( k_namesStr ) };				\
																						\
	return k_enumNames[static_cast<std::size_t>( type )];								\
}																						\
																						\
inline void forEachEnumCase( const std::function<void( EnumName type )>& i_action ) noexcept	\
{																						\
	constexpr std::size_t k_first{ static_cast<std::size_t>( EnumName::FIRST ) };		\
	constexpr std::size_t k_count{ static_cast<std::size_t>( EnumName::COUNT ) };		\
																						\
	for( std::size_t i = k_first; i < k_count; i++ )									\
	{																					\
		i_action( static_cast<EnumName>( i ) );											\
	}																					\
}																						\

template<typename T>
constexpr std::enable_if_t<std::is_enum_v<T>, T> fromString( std::string_view _str ) noexcept
{
	constexpr std::size_t k_count{ static_cast<std::size_t>( T::COUNT ) };
	constexpr std::size_t k_first{ static_cast<std::size_t>( T::FIRST ) };
	for( std::size_t i = k_first; i < k_count; ++i )
	{
		const T current{ static_cast<T>( i ) };
		if( toString( current ) == _str )
		{
			return current;
		}
	}

	return T::INVALID;
}

#define GET_ENUM_NAME( EnumName ) #EnumName

//-----------------------------------------------------------------------------

} //namespace gc

//-----------------------------------------------------------------------------