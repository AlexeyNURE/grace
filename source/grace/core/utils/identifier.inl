#include "identifier.hpp"

namespace utils
{

//-----------------------------------------------------------------------------

template<typename Traits>
inline constexpr Identifier<Traits>::Identifier() :
	m_value( Traits::getInvalid() )
{
}

template<typename Traits>
inline constexpr Identifier<Traits>::Identifier( const base_t i_id ) :
	m_value( i_id )
{
}

template<typename Traits>
inline Identifier<Traits>::Identifier( Identifier&& i_other )
{
	m_value = std::move( i_other.m_value );
	i_other.m_value = Traits::getInvalid();
}

template<typename Traits>
inline Identifier<Traits>::Identifier( const Identifier<Traits>& i_id ) :
	m_value( i_id.m_value )
{
}

template<typename Traits>
inline typename Identifier<Traits>::base_t Identifier<Traits>::value() const
{
	return m_value;
}

template<typename Traits>
inline Identifier<Traits>& Identifier<Traits>::operator=( const Identifier& i_other )
{
	if( *this == i_other )
		return *this;

	m_value = i_other.m_value;

	return *this;
}

template<typename Traits>
inline Identifier<Traits>& Identifier<Traits>::operator=( Identifier&& i_other )
{
	if( *this == i_other )
		return *this;

	m_value = std::move( i_other.m_value );
	i_other.m_value = Traits::getInvalid();

	return *this;
}

template<typename Traits>
inline bool Identifier<Traits>::operator ==( const Identifier& i_other ) const
{
	return m_value == i_other.m_value;
}

template<typename Traits>
inline bool Identifier<Traits>::operator !=( const Identifier& i_other ) const
{
	return !(*this == i_other);
}

template<typename Traits>
inline void Identifier<Traits>::swap( Identifier& i_other )
{
	Identifier<Traits> temp = std::move( *this );
	*this = std::move( i_other );
	i_other = std::move( temp );
}

} // namespace utils

//-----------------------------------------------------------------------------

namespace std
{

template <typename Traits>
struct hash<utils::Identifier<Traits>>
{
	std::size_t operator()( const utils::Identifier<Traits>& i_id ) const
	{
		return std::hash<Traits::base_t>()( i_id.value() );
	}
};

} // namespace std

//-----------------------------------------------------------------------------