#pragma once

#include <glm/vec3.hpp>

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
struct aabb3
{
	using value_t = T;
	using this_t = aabb3<T>;

	template<typename U>
	using vec3 = glm::vec<3, U, glm::defaultp>;

	aabb3();

	aabb3( const vec3<T>& min, const vec3<T>& max );
	aabb3( const vec3<T>& point );
	aabb3( T minX, T minY, T minZ, T maxX, T maxY, T maxZ );

	vec3<T> getCenter() const;
	vec3<T> getMin() const;
	vec3<T> getMax() const;

	vec3<T> getExtent() const;

	T getVolume() const;
	T getArea() const;

	void getCorenrs( vec3<T> corners[8] ) const;

	bool operator==( const aabb3<T>& other ) const;
	bool operator!=( const aabb3<T>& other ) const;

	vec3<T> minPoint;
	vec3<T> maxPoint;
};

//------------------------------------------------------------------------------

using aabb3f = aabb3<float>;
using aabb3d = aabb3<double>;
using aabb3i = aabb3<int>;

} // namespace utils

} // namespace gc

#include "aabb3.inl"

///////////////////////////////////////////////////////////////////////////////