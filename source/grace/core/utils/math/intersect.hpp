#pragma once

#include "grace/core/utils/math/aabb3.hpp"
#include "grace/core/utils/math/ray3.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
bool intersectsWithRay( const ray3<T>& ray, T& outIntersectionNearDistance, T& outIntersectionFarDistance, const aabb3<T>& aabb );
template<typename T>
bool intersectsWithRay( const ray3<T>& ray, glm::vec<3, T, glm::defaultp>& outIntersectionNear, glm::vec<3, T, glm::defaultp>& outIntersectionFar, const aabb3<T>& aabb );

//------------------------------------------------------------------------------

} // namespace utils

} // namespace gc

#include "intersect.inl"

///////////////////////////////////////////////////////////////////////////////