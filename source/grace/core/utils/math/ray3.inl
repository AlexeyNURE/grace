#include "grace/core/utils/math/ray3.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
inline ray3<T>::ray3( const vec3<T>& i_origin, const vec3<T>& i_direction ) :
	origin( i_origin ),
	direction( i_direction )
{
}

//------------------------------------------------------------------------------

} // namespace utils

} // namespace gc

///////////////////////////////////////////////////////////////////////////////