#include "grace/core/utils/math/intersect.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
bool intersectsWithRay( const ray3<T>& ray, glm::vec<3, T, glm::defaultp>& outIntersectionNear, glm::vec<3, T, glm::defaultp>& outIntersectionFar, const aabb3<T>& aabb )
{
	T outIntersectionNearDistance;
	T outIntersectionFarDistance;
	bool result = intersectsWithRay( ray, outIntersectionNearDistance, outIntersectionFarDistance, aabb );

	if( result )
	{
		const glm::vec<3, T, glm::defaultp>& rayOrigin = ray.origin;
		const glm::vec<3, T, glm::defaultp>& rayDirection = ray.directiondir;

		outIntersectionNear = rayOrigin + rayDirection * outIntersectionNearDistance;
		outIntersectionFar = rayOrigin + rayDirection * outIntersectionFarDistance;
	}

	return result;
}

template<typename T>
bool intersectsWithRay( const ray3<T>& ray, T& outIntersectionNearDistance, T& outIntersectionFarDistance, const aabb3<T>& aabb )
{
	const glm::vec<3, T, glm::defaultp>& minBounds = aabb.minPoint;
	const glm::vec<3, T, glm::defaultp>& maxBounds = aabb.maxPoint;

	const glm::vec<3, T, glm::defaultp>& rayOrigin = ray.origin;
	const glm::vec<3, T, glm::defaultp>& rayDirection = ray.direction;

	T tnear = -std::numeric_limits<T>::max();
	T tfar = std::numeric_limits<T>::max();

	for( uint32_t i = 0; i < 3; ++i )
	{
		T rayOriginI = rayOrigin[i];
		T rayDirectionI = rayDirection[i];
		T minBoundsI = minBounds[i];
		T maxBoundsI = maxBounds[i];
		if( rayDirectionI == 0 ) // the ray is parallel to the planes so: 
		{
			if( rayOriginI < minBoundsI || rayOriginI > maxBoundsI ) //origin not between planes
			{
				return false;
			}
		}
		else
		{
			T t1 = ( minBoundsI - rayOriginI ) / rayDirectionI; // time at which ray intersects minimum i plane
			T t2 = ( maxBoundsI - rayOriginI ) / rayDirectionI; // time at which ray intersects maximum i plane
			if( t1 > t2 )
			{
				std::swap( t1, t2 ); // make t1 the lower value
			}

			if( t1 > tnear )
			{
				tnear = t1; // update tnear
			}

			if( t2 < tfar )
			{
				tfar = t2; // update tfar
			}

			if( tnear > tfar ) //box is missed so return FALSE 
			{
				return false;
			}

			if( tfar < 0 ) //box is behind ray so return FALSE
			{
				return false;
			}
		}
	}

	outIntersectionNearDistance = tnear;
	outIntersectionFarDistance = tfar;

	return true;
}

//------------------------------------------------------------------------------

} // namespace utils

} // namespace gc