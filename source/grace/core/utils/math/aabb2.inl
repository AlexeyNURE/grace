#include "grace/core/utils/math/aabb2.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
inline aabb2<T>::aabb2()
{
}

template<typename T>
inline aabb2<T>::aabb2( const vec2<T>& min, const vec2<T>& max ) :
	minPoint( min ),
	maxPoint( max )
{
}

template<typename T>
inline aabb2<T>::aabb2( const vec2<T>& point ) :
	minPoint( point ),
	minPoint( point )
{
}

template<typename T>
inline aabb2<T>::aabb2( T minX, T minY, T maxX, T maxY ) :
	minPoint( minX, minY ),
	maxPoint( maxX, maxY )
{
}

template<typename T>
inline aabb2<T>::vec2<T> aabb2<T>::getCenter() const
{
	return ( minPoint + maxPoint ) * T( 0.5 );
}

template<typename T>
inline aabb2<T>::vec2<T> aabb2<T>::getMin() const
{
	return minPoint;
}

template<typename T>
inline aabb2<T>::vec2<T> aabb2<T>::getMax() const
{
	return maxPoint;
}

template<typename T>
inline aabb2<T>::vec2<T> aabb2<T>::getExtent() const
{
	return maxPoint - minPoint;
}

template<typename T>
inline T aabb2<T>::getArea() const
{
	const vec2<T> e = getExtent();
	return e.x * e.y;
}

template<typename T>
inline bool aabb2<T>::operator==( const aabb2<T>& other ) const
{
	return ( minPoint == other.minPoint && other.maxPoint == maxPoint );
}

template<typename T>
inline bool aabb2<T>::operator!=( const aabb2<T>& other ) const
{
	return !( *this == other );
}

//------------------------------------------------------------------------------

} // namespace utils

} // namespace gc

///////////////////////////////////////////////////////////////////////////////