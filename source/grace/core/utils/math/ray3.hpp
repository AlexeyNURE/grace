#pragma once

#include <glm/vec2.hpp>

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
struct ray3
{
	using value_t = T;
	using this_t = ray3<T>;
	template<typename U>
	using vec3 = glm::vec<3, U, glm::defaultp>;

	ray3( const vec3<T>& origin, const vec3<T>& direction );
	
	vec3<T> origin;
	vec3<T> direction;
};

//------------------------------------------------------------------------------

using ray3f = ray3<float>;
using ray3d = ray3<double>;
using ray3i = ray3<int>;

} // namespace utils

} // namespace gc

#include "ray3.inl"

///////////////////////////////////////////////////////////////////////////////