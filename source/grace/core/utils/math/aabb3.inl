#include "grace/core/utils/math/aabb3.hpp"

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
inline utils::aabb3<T>::aabb3() :
	minPoint( std::numeric_limits<T>::max() ),
	maxPoint( std::numeric_limits<T>::lowest() )
{
}

template<typename T>
inline aabb3<T>::aabb3( const vec3<T>& min, const vec3<T>& max ) :
	minPoint( min ),
	maxPoint( max )
{
}

template<typename T>
inline aabb3<T>::aabb3( const vec3<T>& point ) :
	minPoint( point ),
	maxPoint( point )
{
}

template<typename T>
inline aabb3<T>::aabb3( T minX, T minY, T minZ, T maxX, T maxY, T maxZ ) :
	minPoint( minX, minY, minZ ),
	maxPoint( maxX, maxY, maxZ )
{
}

template<typename T>
inline aabb3<T>::vec3<T> aabb3<T>::getCenter() const
{
	return ( minPoint + maxPoint ) * 0.5f;
}

template<typename T>
inline aabb3<T>::vec3<T> aabb3<T>::getMin() const
{
	return minPoint;
}

template<typename T>
inline aabb3<T>::vec3<T> aabb3<T>::getMax() const
{
	return maxPoint;
}

template<typename T>
inline aabb3<T>::vec3<T> aabb3<T>::getExtent() const
{
	return maxPoint - minPoint;
}

template<typename T>
inline T aabb3<T>::getVolume() const
{
	const vec3<T> e = getExtent();
	return e.x * e.y * e.z;
}

template<typename T>
inline T aabb3<T>::getArea() const
{
	const vec3<T> e = getExtent();
	return 2 * ( e.x * e.y + e.x * e.z + e.y * e.z );
}

template<typename T>
inline void aabb3<T>::getCorenrs( vec3<T> corners[8] ) const
{
	const vec3<T> middle = getCenter();
	const vec3<T> diag = middle - maxPoint;

	/*
	Edges are stored in this way:
			/3--------/7
		   /  |      / |
		  /   |     /  |
		  1---------5  |
		  |   2- - -| -6
		  |  /      |  /
		  |/        | /
		  0---------4/
	*/

	corners[0] = vec3<T>( middle.x + diag.x, middle.y + diag.y, middle.z + diag.z );
	corners[1] = vec3<T>( middle.x + diag.x, middle.y - diag.y, middle.z + diag.z );
	corners[2] = vec3<T>( middle.x + diag.x, middle.y + diag.y, middle.z - diag.z );
	corners[3] = vec3<T>( middle.x + diag.x, middle.y - diag.y, middle.z - diag.z );
	corners[4] = vec3<T>( middle.x - diag.x, middle.y + diag.y, middle.z + diag.z );
	corners[5] = vec3<T>( middle.x - diag.x, middle.y - diag.y, middle.z + diag.z );
	corners[6] = vec3<T>( middle.x - diag.x, middle.y + diag.y, middle.z - diag.z );
	corners[7] = vec3<T>( middle.x - diag.x, middle.y - diag.y, middle.z - diag.z );
}

template<typename T>
inline bool aabb3<T>::operator==( const aabb3<T>& other ) const
{
	return ( minPoint == other.minPoint && other.maxPoint == maxPoint );
}

template<typename T>
inline bool aabb3<T>::operator!=( const aabb3<T>& other ) const
{
	return !( *this == other );
}

//------------------------------------------------------------------------------

} // namespace utils

} // namespace gc