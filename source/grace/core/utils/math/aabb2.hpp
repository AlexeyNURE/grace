#pragma once

#include <glm/vec2.hpp>

///////////////////////////////////////////////////////////////////////////////

namespace gc
{

namespace utils
{

//------------------------------------------------------------------------------

template<typename T>
struct aabb2
{
	using value_t = T;
	using this_t = aabb2<T>;
	template<typename U>
	using vec2 = glm::vec<2, U, glm::defaultp>;

	aabb2();

	aabb2( const vec2<T>& min, const vec2<T>& max );
	aabb2( const vec2<T>& point );
	aabb2( T minX, T minY, T maxX, T maxY );

	vec2<T> getCenter() const;
	vec2<T> getMin() const;
	vec2<T> getMax() const;

	vec2<T> getExtent() const;

	T getArea() const;

	bool operator==( const aabb2<T>& other ) const;
	bool operator!=( const aabb2<T>& other ) const;

	vec2<T> minPoint;
	vec2<T> maxPoint;
};

//------------------------------------------------------------------------------

using aabb2f = aabb2<float>;
using aabb2d = aabb2<double>;
using aabb2i = aabb2<int>;

} // namespace utils

} // namespace gc

#include "aabb2.inl"

///////////////////////////////////////////////////////////////////////////////