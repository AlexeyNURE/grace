#pragma once
#include <type_traits>

//-----------------------------------------------------------------------------

namespace utils
{

//-----------------------------------------------------------------------------

template<typename Traits>
class Identifier
{
public:
	using base_t = typename Traits::base_t;

public:
	constexpr Identifier();
	constexpr explicit Identifier( typename base_t i_id );
	Identifier( const Identifier& i_id );
	Identifier& operator =( const Identifier& i_other );
	Identifier( Identifier&& i_id );
	Identifier& operator =( Identifier&& i_other );

	typename base_t value() const;

	bool operator ==( const Identifier& i_other ) const;
	bool operator !=( const Identifier& i_other ) const;

	void swap( Identifier& i_other );

	static const Identifier<Traits> k_invalid;

private:
	typename base_t m_value = Traits::getInvalid();

	template<typename T>
	friend struct std::hash;
};

//-----------------------------------------------------------------------------

template<typename Traits>
inline constexpr Identifier<Traits> Identifier<Traits>::k_invalid = Traits::getInvalid();

//-----------------------------------------------------------------------------

} // namespace utils

#include "identifier.inl"

//-----------------------------------------------------------------------------

#define DECLARE_IDENTIFIER( Name, Type, InvalidValue )							\
struct TraitsFor##Name															\
{																				\
	using base_t = Type;														\
	static const Type& getInvalid()												\
	{																			\
		static const Type k_invalidValue = InvalidValue;						\
		return k_invalidValue;													\
	}																			\
																				\
	template<typename Traits>													\
	friend class utils::Identifier;												\
};																				\
using Name = utils::Identifier<TraitsFor##Name>;								\

//-----------------------------------------------------------------------------