#include "application/src/camera/Camera.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <grace/core/utils/utils.hpp>
#include <grace/core/utils/Globals.hpp>

#include "application/src/input/input_manager.hpp"
#include "application/src/scene/scene.hpp"

//-----------------------------------------------------------------------------

void Camera::update( float i_deltaTime )
{
	InputManager& inputManager = InputManager::getInstance();

	glm::vec3 camMoveOffset( 0.0f, 0.0f, 0.0f );
	camMoveOffset.y += inputManager.isPressed( 'W' ) * m_moveSpeed * i_deltaTime;
	camMoveOffset.y -= inputManager.isPressed( 'S' ) * m_moveSpeed * i_deltaTime;

	camMoveOffset.x += inputManager.isPressed( 'D' ) * m_moveSpeed * i_deltaTime;
	camMoveOffset.x -= inputManager.isPressed( 'A' ) * m_moveSpeed * i_deltaTime;

	camMoveOffset.z += inputManager.isPressed( 'F' ) * m_moveSpeed * i_deltaTime;
	camMoveOffset.z -= inputManager.isPressed( 'V' ) * m_moveSpeed * i_deltaTime;

	glm::vec3 camRotationOffet( 0.0f, 0.0f, 0.0f );
	camRotationOffet.y -= inputManager.isPressed( 'L' ) * m_rotationSpeed * i_deltaTime;
	camRotationOffet.y += inputManager.isPressed( 'J' ) * m_rotationSpeed * i_deltaTime;
	camRotationOffet.x += inputManager.isPressed( 'I' ) * m_rotationSpeed * i_deltaTime;
	camRotationOffet.x -= inputManager.isPressed( 'K' ) * m_rotationSpeed * i_deltaTime;

	if( camMoveOffset.x != 0 || camMoveOffset.y != 0 || camMoveOffset.z != 0 )
		move( camMoveOffset );

	if( camRotationOffet.x != 0 || camRotationOffet.y != 0 )
		rotate( camRotationOffet );
}

void Camera::init( float i_near, float i_far, float i_fov, float i_moveSpeed, float i_rotSpeed )
{
	m_near = i_near;
	m_far = i_far;
	m_fov = i_fov;
	m_moveSpeed = i_moveSpeed;
	m_rotationSpeed = i_rotSpeed;

	m_upVector = glm::vec3( 0.0f, 1.0f, 0.0f );
	m_target = glm::vec3( 0.0f, 0.0f, -1.0f );

	calculateProjectionMatrix();
}

void Camera::setFar( float i_far )
{
	m_far = i_far;
	calculateProjectionMatrix();
}

void Camera::setNear( float i_near )
{
	m_near = i_near;
	calculateProjectionMatrix();
}

void Camera::setFov( float i_fov )
{
	m_fov = i_fov;
	calculateProjectionMatrix();
}

void Camera::setMoveSpeed( float i_moveSpeed )
{
	m_moveSpeed = i_moveSpeed;
	calculateProjectionMatrix();
}

void Camera::setRotationSpeed( float i_rotationSpeed )
{
	m_rotationSpeed = i_rotationSpeed;
	calculateProjectionMatrix();
}

void Camera::setPosition( const glm::vec3& i_position )
{
	m_position = i_position;
	m_rotationTarget = m_position + m_target;
	m_needToCalculateMatrix = true;
}

void Camera::setRotation( const glm::quat& i_target )
{
	m_rotation = i_target;
	rotate( glm::vec3() );

	m_needToCalculateMatrix = true;
}

const glm::mat4x4& Camera::calculateViewMatrix()
{
	if( m_needToCalculateMatrix )
		calculateViewMatrixInternal();

	return m_viewMatrix;
}

const glm::mat4x4& Camera::getProjectionMatrix() const
{
	return m_projectionMatrix;
}

void Camera::calculateViewMatrixInternal()
{
	glm::vec3 xAxis;
	glm::vec3 yAxis;
	glm::vec3 zAxis;

	zAxis = glm::normalize( ( m_position - m_rotationTarget ) );
	xAxis = glm::normalize( ( glm::cross( m_upVector, zAxis ) ) );
	yAxis = glm::normalize( ( glm::cross( zAxis, xAxis ) ) );

	float dotX = glm::dot( -m_position, xAxis );
	float dotY = glm::dot( -m_position, yAxis );
	float dotZ = glm::dot( -m_position, zAxis );

	m_viewMatrix[0][0] = xAxis.x;
	m_viewMatrix[0][1] = yAxis.x;
	m_viewMatrix[0][2] = zAxis.x;
	m_viewMatrix[0][3] = 0;

	m_viewMatrix[1][0] = xAxis.y;
	m_viewMatrix[1][1] = yAxis.y;
	m_viewMatrix[1][2] = zAxis.y;
	m_viewMatrix[1][3] = 0;

	m_viewMatrix[2][0] = xAxis.z;
	m_viewMatrix[2][1] = yAxis.z;
	m_viewMatrix[2][2] = zAxis.z;
	m_viewMatrix[2][3] = 0;

	m_viewMatrix[3][0] = dotX;
	m_viewMatrix[3][1] = dotY;
	m_viewMatrix[3][2] = dotZ;
	m_viewMatrix[3][3] = 1;

	m_needToCalculateMatrix = false;
}

void Camera::calculateProjectionMatrix()
{
	float width = static_cast<float>( gc::Globals::s_screenWidth );
	float height = static_cast<float>( gc::Globals::s_screenHeight );
	float aspect = width / height;

	m_projectionMatrix = glm::perspective( m_fov, aspect, m_near, m_far );
}

void Camera::move( const glm::vec3& i_vec )
{
	glm::vec3 fwdBack;
	glm::vec3 leftRight;
	glm::vec3 upDown;
	const glm::mat4x4& viewMatrix = calculateViewMatrix();

	fwdBack = glm::vec3(
		-viewMatrix[0][2] * i_vec.y,
		-viewMatrix[1][2] * i_vec.y,
		-viewMatrix[2][2] * i_vec.y
	);

	leftRight = glm::vec3(
		viewMatrix[0][0] * i_vec.x,
		viewMatrix[1][0] * i_vec.x,
		viewMatrix[2][0] * i_vec.x
	);

	upDown = glm::vec3(
		viewMatrix[0][1] * i_vec.z,
		viewMatrix[1][1] * i_vec.z,
		viewMatrix[2][1] * i_vec.z
	);

	glm::vec3 res = fwdBack + leftRight + upDown;

	m_position += res;
	m_rotationTarget += res;

	m_needToCalculateMatrix = true;
}

void Camera::rotate( const glm::vec3& i_vec )
{
	m_rotation += glm::quat( i_vec );

	m_rotation.x = glm::clamp( m_rotation.x + i_vec.x, -ms_rotationXLimit, ms_rotationXLimit );

	glm::mat4x4 rotX = glm::rotate( m_rotation.x, Scene::getRightVector() );
	glm::mat4x4 rotY = glm::rotate( m_rotation.y, Scene::getFrontVector() );

	glm::vec4 rotated = rotY * ( rotX * glm::vec4( m_target, 1.f ) );
	m_rotationTarget = m_position + glm::vec3( rotated.x, rotated.y, rotated.z );

	m_needToCalculateMatrix = true;
}

//-----------------------------------------------------------------------------