#pragma once

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>

//------------------------------------------------------------------------------

class Camera
{
public:
	Camera() = default;
	~Camera() = default;

	void init(
		float i_near,
		float i_far,
		float i_fov,
		float i_moveSpeed,
		float i_rotSpeed
	);

	void setPosition( const glm::vec3& i_position );
	void setRotation( const glm::quat& i_target );

	const glm::mat4x4& calculateViewMatrix();
	const glm::mat4x4& getViewMatrix() const;
	const glm::mat4x4& getProjectionMatrix() const;

	const glm::vec3& getPosition() const;
	const glm::quat& getRotation() const;
	float getFar() const;
	float getNear() const;
	float getFov() const;
	float getMoveSpeed() const;
	float getRotationSpeed() const;

	void setFar( float i_far );
	void setNear( float i_near );
	void setFov( float i_fov );
	void setMoveSpeed( float i_moveSpeed );
	void setRotationSpeed( float i_rotSpeed );

	void move( const glm::vec3& i_vec );
	void rotate( const glm::vec3& i_vec );

	void update( float i_deltaTime );

	void calculateProjectionMatrix();

private:
	void calculateViewMatrixInternal();

	glm::mat4x4 m_viewMatrix;
	glm::mat4x4 m_projectionMatrix;

	glm::vec3 m_position;
	glm::quat m_rotation;
	glm::vec3 m_rotationTarget;
	glm::vec3 m_target;
	glm::vec3 m_upVector;

	bool m_needToCalculateMatrix{ true };

	float m_near{ 0.f };
	float m_far{ 0.f };
	float m_fov{ 0.f };
	float m_moveSpeed{ 0.f };
	float m_rotationSpeed{ 0.f };

	static constexpr float ms_rotationXLimit = 0.99f;
};

//------------------------------------------------------------------------------

inline const glm::vec3& Camera::getPosition() const
{
	return m_position;
}

inline const glm::quat& Camera::getRotation() const
{
	return m_rotation;
}

inline float Camera::getFar() const
{
	return m_far;
}

inline float Camera::getNear() const
{
	return m_near;
}

inline float Camera::getFov() const
{
	return m_fov;
}

inline float Camera::getMoveSpeed() const
{
	return m_moveSpeed;
}

inline float Camera::getRotationSpeed() const
{
	return m_rotationSpeed;
}

inline const glm::mat4x4& Camera::getViewMatrix() const
{
	return m_viewMatrix;
}

///////////////////////////////////////////////////////////////////////////////