#include "precompiled.hpp"

#include <json/json.h>
#include <fstream>

#include <glad/glad.h>

#include <grace/core/utils/logging/log.hpp>
#include <grace/core/utils/utils.hpp>

#include <grace/render/shader/shader_kind.hpp>

#include "application/src/resource/textureFactory.hpp"
#include "application/src/resource/model_loader.hpp"
#include "application/src/resource/resource_manager.hpp"

//-----------------------------------------------------------------------------

namespace utils
{

static gc::s32 tilingFromString( std::string_view i_str )
{
	gc::s32 tiling = -1;
	if( i_str == "CLAMP_TO_EDGE" )
		tiling = GL_CLAMP_TO_EDGE;
	else if( i_str == "REPEAT" )
		tiling = GL_REPEAT;

	return tiling;
}

} // namespace utils

//-----------------------------------------------------------------------------

void ResourceManager::init()
{
	Json::Value root;

	std::ifstream RMFile( ms_RMFileName.data(), std::ifstream::in );
	if( !RMFile.is_open() )
	{
		ASSERT( false, "Couldn't open file {}", ms_RMFileName.data() );
		return;
	}

	RMFile >> root;

	Json::Value textures = root["2DTextures"];
	load2DTextures( textures );

	Json::Value cubeTextures = root["cubeTextures"];
	loadCubeTextures( cubeTextures );

	Json::Value shaders = root["shaders"];
	loadShaders( shaders );

	RMFile.close();
}

void ResourceManager::load2DTextures( const Json::Value& i_textures )
{
	size_t Textures2DCount = i_textures.size();
	m_p2DTextures.reserve( Textures2DCount );
	load2DTexture( i_textures, Textures2DCount );
}

void ResourceManager::loadCubeTextures( const Json::Value& i_textures )
{
	size_t cubeTexturesCount = i_textures.size();
	m_pCubeTextures.reserve( cubeTexturesCount );
	loadCubeTexture( i_textures, cubeTexturesCount );
}

void ResourceManager::load2DTexture( const Json::Value& i_val, size_t i_count )
{
	for( size_t i = 0; i < i_count; i++ )
	{
		Json::Value currentTexture = i_val[static_cast<int>( i )];
		const char* name = currentTexture["name"].asCString();
		int ID = currentTexture["ID"].asInt();
		std::string fileName = currentTexture["file"].asString();
		std::string tilingStr = currentTexture["tiling"].asString();

		m_p2DTextures[name] = TextureFactory::load2DTexture( fileName, utils::tilingFromString( tilingStr ) );
	}
}

void ResourceManager::loadCubeTexture( const Json::Value& i_val, size_t i_count )
{
	for( size_t i = 0; i < i_count; i++ )
	{
		Json::Value currentTexture = i_val[static_cast<int>( i )];
		const char* name = currentTexture["name"].asCString();
		int ID = currentTexture["ID"].asInt();
		std::string fileName = currentTexture["file"].asString();
		std::string tilingStr = currentTexture["tiling"].asString();

		m_pCubeTextures[name] = TextureFactory::loadCubeTexture( fileName.c_str(), utils::tilingFromString( tilingStr ) );
	}
}

void ResourceManager::loadShaders( const Json::Value& i_shaders )
{
	size_t shadersCount = i_shaders.size();
	m_pShaders.reserve( shadersCount );
	for( size_t i = 0; i < shadersCount; i++ )
	{
		Json::Value currentShader = i_shaders[static_cast<int>( i )];
		int ID = currentShader["ID"].asInt();
		std::string VS = currentShader["VS"].asString();
		std::string FS = currentShader["FS"].asString();
		std::string kind = currentShader["kind"].asString();

		m_pShaders[kind] = std::make_unique<gc::Shader>();
		m_pShaders[kind]->init( VS.c_str(), FS.c_str(), gc::fromString<gc::ShaderKind>( kind ) );
	}
}

const std::string& ResourceManager::getModelByName( std::string_view i_name )
{
	auto& it = m_pModels.find( i_name.data() );
	ASSERT( it != m_pModels.end(), "Couldn't find model {}", i_name );

	return it->second;
}

gc::Texture2D* ResourceManager::get2DTextureByName( std::string_view i_name )
{
	auto& it = m_p2DTextures.find( i_name.data() );
	ASSERT( it != m_p2DTextures.end(), "Couldn't find 2D texture {}", i_name );

	return it->second.get();
}

gc::CubeTexture* ResourceManager::getCubeTextureByName( std::string_view i_name )
{
	auto& it = m_pCubeTextures.find( i_name.data() );
	ASSERT( it != m_pCubeTextures.end(), "Couldn't find cube texture {}", i_name );

	return it->second.get();
}

gc::Shader* ResourceManager::getShaderByName( std::string_view i_name )
{
	auto& it = m_pShaders.find( i_name.data() );
	ASSERT( it != m_pShaders.cend(), "Couldn't find shader {}", i_name );

	return it->second.get();
}

const gc::Texture2D* ResourceManager::loadTexture2D( std::string_view i_name, gc::u32 i_tiling )
{
	auto& it = m_p2DTextures.find( i_name.data() );
	if( it != m_p2DTextures.end() )
	{
		return it->second.get();
	}

	auto texture = TextureFactory::load2DTexture( i_name, i_tiling );
	const gc::Texture2D* texPtr = texture.get();
	m_p2DTextures[i_name.data()] = std::move( texture );

	return texPtr;
}

//-----------------------------------------------------------------------------