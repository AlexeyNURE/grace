#pragma once

#include <boost/noncopyable.hpp>
#include <memory>
#include <grace/core/utils/basic_types.hpp>
#include <grace/render/model/model.hpp>

#include <assimp/postprocess.h>
#include <assimp/material.h>

///////////////////////////////////////////////////////////////////////////////

struct aiMesh;
struct aiNode;
struct aiScene;
struct aiMaterial;

//-----------------------------------------------------------------------------

class ModelLoader : boost::noncopyable
{
public:
	ModelLoader() = default;
	~ModelLoader() = default;

	static std::unique_ptr<gc::Model> loadModel( std::string_view _modelFileName );
	static std::unique_ptr<gc::Model> loadRectModel();

private:
	static void processNode( gc::Model* model, aiNode* node, const aiScene* scene );
	static std::unique_ptr<gc::Mesh> processMesh( aiMesh* mesh, const aiScene* scene );
	static void loadMaterialTextures( gc::Material& o_material, aiMaterial* mat, aiTextureType type );

	static std::unique_ptr<gc::Model> readNfgModel( std::string_view i_modelFileName );

	static std::string s_modelDirectory;
};

//-----------------------------------------------------------------------------