#include <fstream>
#include <json/json.h>

#include <glad/glad.h>

#include "application/src/resource/effect_manager.hpp"
#include "application/src/input/input_manager.hpp"
#include "application/src/camera/camera.hpp"

//-----------------------------------------------------------------------------

void EffectManager::load( const Camera& i_camera )
{
	m_camera = &i_camera;

	std::ifstream EMFile( ms_EMFileName.data(), std::ifstream::in );
	if( !EMFile.is_open() )
		return;

	Json::Value root;
	EMFile >> root;

	EMFile.close();

	Json::Value fbo = root["fboCount"];
	loadFBO( fbo );

	Json::Value shaders = root["shaders"];
	loadShaders( shaders );

	Json::Value effects = root["effects"];
	loadEffects( effects );
}

void EffectManager::bindFbo()
{
	if( m_pCurrentEffect )
	{
		bindScreenFbo();
		bindFrameBufferFbo();
	}
}

void EffectManager::bindScreenFbo()
{
	gc::s32 qt_buffer;
	glGetIntegerv( GL_FRAMEBUFFER_BINDING, &qt_buffer );
	m_fbos[s_screenFboIdx]->m_ID = qt_buffer;
}

void EffectManager::bindFrameBufferFbo()
{
	m_fbos[1]->bind();
}

void EffectManager::loadEffects( const Json::Value& i_effects )
{
	const size_t effectsCount = i_effects.size();
	m_effects.reserve( effectsCount );
	for( gc::u32 i = 0; i < effectsCount; i++ )
	{
		Json::Value currentEffect = i_effects[i];
		Json::Value passes = currentEffect["passes"];

		std::vector<std::unique_ptr<gc::Pass>> effectPasses = loadEffectPasses( passes );

		std::unique_ptr<gc::Effect> effect = std::make_unique<gc::Effect>();

		effect->addPasses( std::move( effectPasses ) );
		m_effects.emplace_back( std::move( effect ) );
	}
}

std::vector<std::unique_ptr<gc::Pass>> EffectManager::loadEffectPasses( const Json::Value& i_passes )
{
	const size_t passesCount = i_passes.size();
	std::vector<std::unique_ptr<gc::Pass>> passes;
	passes.reserve( passesCount );
	for( int i = 0; i < passesCount; i++ )
	{
		Json::Value currentPass = i_passes[i];

		const int ID = currentPass["ID"].asInt();
		const char* shaderName = currentPass["shader"].asCString();
		Json::Value textures = currentPass["textures"];
		const int fboID = currentPass["fbo"].asInt();
		Json::Value parameters = currentPass["parameters"];

		std::vector<const gc::Texture2D*> passTexs = loadPassTextures( textures );
		std::vector<float> passParams = loadPassParams( parameters );

		std::unique_ptr<gc::Pass> pass = std::make_unique<gc::Pass>();

		pass->addTextures( std::move( passTexs ) );
		pass->setShader( getShaderByName( shaderName ) );
		pass->setFBO( m_fbos[fboID].get() );
		pass->setParams( std::move( passParams ) );
		pass->setCameraFar( m_camera->getFar() );
		pass->setCameraNear( m_camera->getNear() );

		passes.emplace_back( std::move( pass ) );
	}

	return passes;
}

std::vector<const gc::Texture2D*> EffectManager::loadPassTextures( const Json::Value& i_textures )
{
	const size_t supportedTexCount = i_textures.size();
	size_t validTexCount = supportedTexCount;
	for( int i = 0; i < supportedTexCount; i++ )
	{
		const char* str = i_textures[i].asCString();
		if( str[0] == '0' )
			--validTexCount;
	}

	std::vector<const gc::Texture2D*> textures;
	textures.reserve( validTexCount );

	for( int i = 0; i < validTexCount; i++ )
	{
		const char* str = i_textures[i].asCString();
		const int fboIndex = str[0] - 48;

		if( str[1] == 'c' )
			textures.emplace_back( m_fbos[fboIndex]->getColorTex() );
		else
			textures.emplace_back( m_fbos[fboIndex]->getDepthTex() );
	}

	return textures;
}

std::vector<float> EffectManager::loadPassParams( const Json::Value& i_data )
{
	const size_t dataCount = i_data.size();
	std::vector<float> parameters;
	parameters.reserve( dataCount );

	for( int i = 0; i < dataCount; i++ )
		parameters.emplace_back( i_data[i].asFloat() );

	return parameters;
}

void EffectManager::loadShaders( const Json::Value& i_shaders )
{
	const int shadersCount = i_shaders.size();
	m_shaders.reserve( shadersCount );

	for( int i = 0; i < shadersCount; ++i )
	{
		const Json::Value currentShader = i_shaders[i];

		std::unique_ptr<gc::Shader> shader = std::make_unique<gc::Shader>();

		const int shaderID = currentShader["ID"].asInt();
		const char* VS = currentShader["VS"].asCString();
		const char* FS = currentShader["FS"].asCString();
		std::string kind = currentShader["kind"].asString();

		shader->init( VS, FS, gc::fromString<gc::ShaderKind>( kind ) );

		m_shaders.emplace_back( std::move( shader ) );
	}
}

void EffectManager::loadFBO( const Json::Value& i_fbo )
{
	// fbo[0] is for screen, others are for framebuffers
	size_t fboCount = i_fbo.asUInt();
	const size_t actualCount = fboCount + 1;
	m_fbos.reserve( actualCount );

	m_fbos.emplace_back( std::make_unique<gc::FBO>() ); // screen

	for( int i = 1; i < actualCount; i++ )
	{
		std::unique_ptr<gc::FBO> fbo = std::make_unique<gc::FBO>();
		fbo->init();
		m_fbos.emplace_back( std::move( fbo ) );
	}
}

gc::Shader* EffectManager::getShaderByName( std::string_view i_name ) const
{
	for( auto& shader : m_shaders )
	{
		if( gc::fromString<gc::ShaderKind>( i_name ) == shader->getShaderKind() )
			return shader.get();
	}

	return nullptr;
}

gc::Shader* EffectManager::getShaderByID( gc::s32 i_id ) const
{
	ASSERT( m_shaders.size() > i_id, "Invalid id" );
	return m_shaders[i_id].get();
}

void EffectManager::render( const gc::OpenGLRendererAPI& i_renderer )
{
	if( m_pCurrentEffect )
		m_pCurrentEffect->render( i_renderer );
}

void EffectManager::regenerateFBO()
{
	for( auto& fbo : m_fbos )
	{
		fbo->regenerate();
	}
}

void EffectManager::update( float i_deltaTime )
{
	const InputManager& IM = InputManager::getInstance();
	const size_t effectsCount = m_effects.size();

	if( IM.isPressed( '0' ) )
		m_pCurrentEffect = nullptr;
	else if( IM.isPressed( '1' ) && effectsCount >= 1 )
		m_pCurrentEffect = m_effects[0].get();
	else if( IM.isPressed( '2' ) && effectsCount >= 2 )
		m_pCurrentEffect = m_effects[1].get();
	else if( IM.isPressed( '3' ) && effectsCount >= 3 )
		m_pCurrentEffect = m_effects[2].get();
	else if( IM.isPressed( '4' ) && effectsCount >= 4 )
		m_pCurrentEffect = m_effects[3].get();
	else if( IM.isPressed( '5' ) && effectsCount >= 5 )
		m_pCurrentEffect = m_effects[4].get();
}

//-----------------------------------------------------------------------------