#pragma once

#include <json/json.h>

#include <grace/render/effects/FBO.hpp>
#include <grace/render/effects/effect.hpp>
#include <grace/render/effects/pass.hpp>
#include <grace/render/shader/shader.hpp>

#include <grace/core/utils/singleton.hpp>

//-----------------------------------------------------------------------------

class Camera;

//-----------------------------------------------------------------------------

class EffectManager : public gc::Singleton<EffectManager>
{
public:
	void load( const Camera& i_camera );
	void bindFbo();

	void update( float _deltaTime );

	void render( const gc::OpenGLRendererAPI& i_renderer );

	void regenerateFBO();

	gc::Shader* getShaderByName( std::string_view i_name ) const;
	gc::Shader* getShaderByID( gc::s32 i_id ) const;

private:
	void loadEffects( const Json::Value& i_effects );
	std::vector<std::unique_ptr<gc::Pass>> loadEffectPasses( const Json::Value& i_passes );
	std::vector<const gc::Texture2D*> loadPassTextures( const Json::Value& i_textures );
	std::vector<float> loadPassParams( const Json::Value& i_params );
	void loadFBO( const Json::Value& i_fbo );
	void loadShaders( const Json::Value& i_shaders );

	void bindScreenFbo();
	void bindFrameBufferFbo();

private:
	std::vector<std::unique_ptr<gc::FBO>> m_fbos;
	std::vector<std::unique_ptr<gc::Shader>> m_shaders;
	std::vector<std::unique_ptr<gc::Effect>> m_effects;

	const Camera* m_camera{ nullptr };
	gc::Effect* m_pCurrentEffect{ nullptr };

	static constexpr gc::u8 s_screenFboIdx{ 0 };

	static constexpr std::string_view ms_EMFileName{ "..\\..\\data\\EM.json" };
};

//-----------------------------------------------------------------------------