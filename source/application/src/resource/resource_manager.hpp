#pragma once

#include <json/json.h>

#include <grace/render/model/model.hpp>
#include <grace/render/texture/texture2D.hpp>
#include <grace/render/texture/cube_texture.hpp>
#include <grace/render/shader/shader.hpp>

#include <grace/core/utils/singleton.hpp>

//-----------------------------------------------------------------------------

class ResourceManager : public gc::Singleton<ResourceManager>
{
	using modelsView = std::unordered_map<std::string, std::string>;
	using textures2DView = std::unordered_map<std::string, std::unique_ptr<gc::Texture2D>>;
	using cubeTexturesView = std::unordered_map<std::string, std::unique_ptr<gc::CubeTexture>>;
	using shadersView = std::unordered_map<std::string, std::unique_ptr<gc::Shader>>;

public:
	void init();

	const std::string& getModelByName( std::string_view i_name );
	gc::Texture2D* get2DTextureByName( std::string_view i_name );
	gc::CubeTexture* getCubeTextureByName( std::string_view i_name );
	gc::Shader* getShaderByName( std::string_view i_name );

	const modelsView& getModels() const;
	const textures2DView& getTextures2D() const;
	const cubeTexturesView& getCubeTextures() const;
	const shadersView& getShaders() const;

	gc::u64 getModelsCount() const;
	gc::u64 get2DTextureCount() const;
	gc::u64 getCubeTexturesCount() const;
	gc::u64 getShadersCount() const;

	const gc::Texture2D* loadTexture2D( std::string_view i_name, gc::u32 i_tiling );

private:
	void load2DTextures( const Json::Value& i_textures );
	void loadCubeTextures( const Json::Value& i_textures );
	void load2DTexture( const Json::Value& i_val, size_t i_count );
	void loadCubeTexture( const Json::Value& i_val, size_t i_count );
	void loadShaders( const Json::Value& i_shaders );

	modelsView m_pModels;
	textures2DView m_p2DTextures;
	cubeTexturesView m_pCubeTextures;
	shadersView m_pShaders;

	static constexpr std::string_view ms_RMFileName{ "..\\..\\data\\RM.json" };
};

//-----------------------------------------------------------------------------

inline gc::u64 ResourceManager::getModelsCount() const
{
	return m_pModels.size();
}

inline gc::u64 ResourceManager::get2DTextureCount() const
{
	return m_p2DTextures.size();
}

inline gc::u64 ResourceManager::getCubeTexturesCount() const
{
	return m_pCubeTextures.size();
}

inline gc::u64 ResourceManager::getShadersCount() const
{
	return m_pShaders.size();
}

inline const ResourceManager::modelsView& ResourceManager::getModels() const
{
	return m_pModels;
}

inline const ResourceManager::textures2DView& ResourceManager::getTextures2D() const
{
	return m_p2DTextures;
}

inline const ResourceManager::cubeTexturesView& ResourceManager::getCubeTextures() const
{
	return m_pCubeTextures;
}

inline const ResourceManager::shadersView& ResourceManager::getShaders() const
{
	return m_pShaders;
}

//inline Model* ResourceManager::getModelByID( s32 i_ID )
//{
//	return m_pModels[_ID].get();
//}
//
//inline Texture2D* ResourceManager::get2DTextureByID( s32 i_ID )
//{
//	return m_p2DTextures[_ID].get();
//}
//
//inline CubeTexture* ResourceManager::getCubeTextureByID( s32 i_ID )
//{
//	return m_pCubeTextures[_ID].get();
//}
//
//inline Shader* ResourceManager::getShaderByID( s32 i_ID )
//{
//	return m_pShaders[_ID].get();
//}

//-----------------------------------------------------------------------------