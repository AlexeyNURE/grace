#pragma once

#include <boost/noncopyable.hpp>
#include <memory>
#include "grace/core/utils/basic_types.hpp"
#include "grace/render/texture/texture2D.hpp"
#include "grace/render/texture/cube_texture.hpp"

//-----------------------------------------------------------------------------

class TextureFactory : boost::noncopyable
{
public:
	TextureFactory() = default;
	~TextureFactory() = default;

	static std::unique_ptr<gc::Texture2D> load2DTexture( std::string_view _textureFileName, gc::s32 tiling );
	static std::unique_ptr<gc::CubeTexture> loadCubeTexture( std::string_view _textureFileName, gc::s32 _tiling );
};

//-----------------------------------------------------------------------------