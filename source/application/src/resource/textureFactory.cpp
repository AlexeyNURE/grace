#include "precompiled.hpp"

#include <glad/glad.h>
#include <SOIL2/SOIL2.h>

#include "application/src/resource/textureFactory.hpp"
#include <grace/render/texture/load_tga.hpp>

//-----------------------------------------------------------------------------

std::unique_ptr<gc::Texture2D> TextureFactory::load2DTexture( std::string_view i_textureFileName, gc::s32 i_tiling )
{
	LOG_INFO( "Loading 2D texture : {}", i_textureFileName );

	gc::TextureParams params;
	params.tiling = i_tiling;
	unsigned char* image = SOIL_load_image( i_textureFileName.data(), &params.width, &params.height, &params.bpp, SOIL_LOAD_AUTO );

	ASSERT( image, "Coundn't load texture '{}'", i_textureFileName.data() );

	params.format = GL_RGB;
	if( params.bpp == 4 )
		params.format = GL_RGBA;

	auto texture = std::make_unique<gc::Texture2D>();

	texture->init( params, image );

	SOIL_free_image_data( image );

	return texture;
}

//-----------------------------------------------------------------------------

std::unique_ptr<gc::CubeTexture> TextureFactory::loadCubeTexture( std::string_view i_textureFileName, gc::s32 _tiling )
{
	LOG_INFO( "Loading Cube texture : {}", i_textureFileName );

	gc::CubeTextureParams params;
	params.tiling = _tiling;
	params.format = GL_RGB;
	int channels;

	unsigned char* image = SOIL_load_image( i_textureFileName.data(), &params.width, &params.height, &params.bpp, SOIL_LOAD_RGB );

	ASSERT( image, "Coundn't load texture '{}'", i_textureFileName.data() );

	std::unique_ptr<gc::CubeTexture> texture = std::make_unique<gc::CubeTexture>();

	params.faceWidth = params.width / 4;
	std::unique_ptr<unsigned char[]> bufferFace( new unsigned char[params.faceWidth * params.faceWidth * params.bpp] ); // faces are square

	texture->init( params, image, bufferFace.get() );

	SOIL_free_image_data( image );

	return texture;
}

//-----------------------------------------------------------------------------