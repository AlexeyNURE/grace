#include <memory>
#include <boost/filesystem/path.hpp>

#include "application/src/resource/model_loader.hpp"
#include "application/src/resource/resource_manager.hpp"

#include <grace/core/utils/math/aabb3.hpp>
#include <grace/core/utils/logging/log.hpp>
#include <grace/core/utils/utils.hpp>

#include <grace/render/openGL/vertex.hpp>
#include <grace/render/model/mesh.hpp>

#include <grace/render/texture/texture2D.hpp>

#include <SOIL2/SOIL2.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glad/glad.h>

//-----------------------------------------------------------------------------

std::string ModelLoader::s_modelDirectory = "";

std::unique_ptr<gc::Model> ModelLoader::loadModel( std::string_view i_modelFileName )
{
	LOG_INFO( "Loading model : {}", i_modelFileName );

	if( i_modelFileName.find( ".nfg" ) != std::string::npos )
	{
		return readNfgModel( i_modelFileName );
	}

	boost::filesystem::path p( i_modelFileName.data() );
	s_modelDirectory = p.parent_path().string() + "/";

	// Read file via ASSIMP
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile( i_modelFileName.data(), aiProcess_Triangulate | aiProcess_FlipUVs );

	// Check for errors
	if( !scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode ) // if is Not Zero
	{
		LOG_ERROR( "ERROR::ASSIMP:: {}", importer.GetErrorString() );
	}

	std::unique_ptr<gc::Model> model = std::make_unique<gc::Model>();

	processNode( model.get(), scene->mRootNode, scene );

	return model;
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void ModelLoader::processNode( gc::Model* model, aiNode* node, const aiScene* scene )
{
	// Process each mesh located at the current node
	for( gc::u32 i = 0; i < node->mNumMeshes; i++ )
	{
		// The node object only contains indices to index the actual objects in the scene.
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

		model->addMesh( processMesh( mesh, scene ) );
	}

	// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for( gc::u32 i = 0; i < node->mNumChildren; i++ )
	{
		processNode( model, node->mChildren[i], scene );
	}
}

std::unique_ptr<gc::Mesh> ModelLoader::processMesh( aiMesh* mesh, const aiScene* scene )
{
	// Data to fill
	std::vector<gc::utils::Vertex> pVerticesData;
	std::vector<gc::u32> pIndices;

	// Walk through each of the mesh's vertices
	for( gc::u32 i = 0; i < mesh->mNumVertices; i++ )
	{
		gc::utils::Vertex vertex;

		// Positions
		vertex.pos.x = mesh->mVertices[i].x;
		vertex.pos.y = mesh->mVertices[i].y;
		vertex.pos.z = mesh->mVertices[i].z;

		// Normals
		if( mesh->mNormals )
		{
			vertex.normal.x = mesh->mNormals[i].x;
			vertex.normal.y = mesh->mNormals[i].y;
			vertex.normal.z = mesh->mNormals[i].z;
		}

		// Texture Coordinates
		if( mesh->mTextureCoords[0] ) // Does the mesh contain texture coordinates?
		{
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vertex.uv.x = mesh->mTextureCoords[0][i].x;
			vertex.uv.y = mesh->mTextureCoords[0][i].y;
		}

		if( mesh->mTangents )
		{
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vertex.tangent.x = mesh->mTangents[i].x;
			vertex.tangent.y = mesh->mTangents[i].y;
			vertex.tangent.z = mesh->mTangents[i].z;
		}

		if( mesh->mBitangents )
		{
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vertex.bitangent.x = mesh->mBitangents[i].x;
			vertex.bitangent.y = mesh->mBitangents[i].y;
			vertex.bitangent.z = mesh->mBitangents[i].z;
		}

		pVerticesData.emplace_back( vertex );
	}

	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for( gc::u32 i = 0; i < mesh->mNumFaces; i++ )
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices vector
		for( gc::u32 j = 0; j < face.mNumIndices; j++ )
		{
			pIndices.emplace_back( face.mIndices[j] );
		}
	}

	gc::Material mat;
	if( mesh->mMaterialIndex >= 0 )
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		aiString str = material->GetName();
		aiColor3D color;
		float shiness;
		float transparent;

		if( material->Get( AI_MATKEY_COLOR_AMBIENT, color ) == aiReturn_SUCCESS )
			mat.setValue( "u_matAmbient", glm::vec4( color.r, color.g, color.b, 1.0 ) );
		if( material->Get( AI_MATKEY_COLOR_DIFFUSE, color ) == aiReturn_SUCCESS )
			mat.setValue( "u_matDiffuse", glm::vec4( color.r, color.g, color.b, 1.0 ) );
		if( material->Get( AI_MATKEY_COLOR_SPECULAR, color ) == aiReturn_SUCCESS )
			mat.setValue( "u_matSpecular", glm::vec4( color.r, color.g, color.b, 1.0 ) );
		if( material->Get( AI_MATKEY_SHININESS, shiness ) == aiReturn_SUCCESS )
		{
			if( shiness == 0 )
			{
				shiness = 50.f;
			}
			mat.setValue( "u_matShiness", shiness );
		}
		if( material->Get( AI_MATKEY_OPACITY, transparent ) == aiReturn_SUCCESS )
			mat.setValue( "u_matAlpha", transparent );

		// 1. diffuse maps
		loadMaterialTextures( mat, material, aiTextureType_DIFFUSE );
		// 2. specular maps
		loadMaterialTextures( mat, material, aiTextureType_SPECULAR );
		// 3. normal maps
		loadMaterialTextures( mat, material, aiTextureType_HEIGHT );
		// 4. height maps
		loadMaterialTextures( mat, material, aiTextureType_AMBIENT );
	}

	auto resMesh = std::make_unique<gc::Mesh>( pVerticesData.data(), pVerticesData.size(), pIndices.data(), pIndices.size() );
	resMesh->setName( mesh->mName.C_Str() );
	resMesh->setMaterial( mat );

	// Return a mesh object created from the extracted mesh data
	return resMesh;
}

void ModelLoader::loadMaterialTextures( gc::Material& o_material, aiMaterial* mat, aiTextureType type )
{
	for( gc::u32 i = 0; i < mat->GetTextureCount( type ); i++ )
	{
		aiString str;
		mat->GetTexture( type, i, &str );

		std::string path = s_modelDirectory + str.C_Str();

		const auto* texture = ResourceManager::getInstance().loadTexture2D( path, GL_CLAMP_TO_EDGE );

		std::string uniformName;
		if( type == aiTextureType_DIFFUSE )
			uniformName = "u_texture";
		else if( type == aiTextureType_HEIGHT )
			uniformName = "u_normalMapTex";
		else
			return;

		o_material.setValue( uniformName, *texture );
	}
}

std::unique_ptr<gc::Model> ModelLoader::loadRectModel()
{
	gc::u64 verticesCount = 4;
	gc::u64 indicesCount = 6;

	std::unique_ptr<gc::utils::Vertex[]> pVerticesData( new gc::utils::Vertex[verticesCount] );
	std::unique_ptr<gc::u32[]> pIndices( new gc::u32[indicesCount] );

	pVerticesData[0].pos = glm::vec3( -1.0f, -1.0f, 0.0f );
	pVerticesData[1].pos = glm::vec3( -1.0f, 1.0f, 0.0f );
	pVerticesData[2].pos = glm::vec3( 1.0f, -1.0f, 0.0f );
	pVerticesData[3].pos = glm::vec3( 1.0f, 1.0f, 0.0f );
	pVerticesData[0].uv = glm::vec2( 0.0f, 0.0f );
	pVerticesData[1].uv = glm::vec2( 0.0f, 1.0f );
	pVerticesData[2].uv = glm::vec2( 1.0f, 0.0f );
	pVerticesData[3].uv = glm::vec2( 1.0f, 1.0f );

	pIndices[0] = 0;
	pIndices[1] = 1;
	pIndices[2] = 2;
	pIndices[3] = 1;
	pIndices[4] = 2;
	pIndices[5] = 3;

	std::unique_ptr<gc::Mesh> mesh = std::make_unique<gc::Mesh>( pVerticesData.get(), verticesCount, pIndices.get(), indicesCount );
	std::unique_ptr<gc::Model> model = std::make_unique<gc::Model>();
	model->addMesh( std::move( mesh ) );

	return model;
}

std::unique_ptr<gc::Model> ModelLoader::readNfgModel( std::string_view i_modelFileName )
{
	FILE* pFile = fopen( i_modelFileName.data(), "r" );
	if( pFile == nullptr )
	{
		ASSERT( false, "couldn't open file %s", i_modelFileName.data() );
		return nullptr;
	}

	gc::u64 verticesCount = 0;

	fscanf_s( pFile, "%*s %d", &verticesCount );
	std::vector<gc::utils::Vertex> pVerticesData( verticesCount, gc::utils::Vertex{} );

	for( size_t i = 0; i < verticesCount; i++ )
	{
		auto& vertex = pVerticesData[i];
		fscanf_s(
			pFile,
			" %*d. pos:[%f, %f, %f]; norm:[%f, %f, %f]; binorm:[%f, %f, %f]; tgt:[%f, %f, %f]; uv:[%f, %f];",
			&vertex.pos.x,
			&vertex.pos.y,
			&vertex.pos.z,
			&vertex.normal.x,
			&vertex.normal.y,
			&vertex.normal.z,
			&vertex.bitangent.x,
			&vertex.bitangent.y,
			&vertex.bitangent.z,
			&vertex.tangent.x,
			&vertex.tangent.y,
			&vertex.tangent.z,
			&vertex.uv.x,
			&vertex.uv.y
		);
	}

	gc::u64 indicesCount = 0;

	fscanf_s( pFile, "%*s %d", &indicesCount );
	std::vector<gc::u32> pIndices( indicesCount );

	for( size_t i = 0; i < indicesCount; i += 3 )
	{
		fscanf_s(
			pFile,
			" %*d. %hd, %hd, %hd",
			&pIndices[i],
			&pIndices[i + 1],
			&pIndices[i + 2]
		);
	}

	fclose( pFile );

	std::unique_ptr<gc::Model> model = std::make_unique<gc::Model>( pVerticesData.data(), verticesCount, pIndices.data(), indicesCount );

	return model;
}

//-----------------------------------------------------------------------------