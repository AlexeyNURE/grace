#pragma once

#include <unordered_map>
#include <grace/core/utils/basic_types.hpp>
#include <grace/core/utils/singleton.hpp>

//------------------------------------------------------------------------

class InputManager : public gc::Singleton<InputManager>
{
public:
	void processKey( gc::s32 i_key, bool i_isPressed );
	bool isPressed( gc::s32 i_key ) const;

	enum Arrows{ Left = 37, Up, Right, Down };

private:
	std::unordered_map<gc::s32, bool> m_keys;
};

//------------------------------------------------------------------------