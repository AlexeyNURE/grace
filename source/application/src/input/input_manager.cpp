#include "application/src/input/input_manager.hpp"

//------------------------------------------------------------------------

bool InputManager::isPressed( gc::s32 i_key ) const
{
	auto it = m_keys.find( i_key );
	return it != m_keys.cend() ? it->second : false;
}

void InputManager::processKey( gc::s32 i_key, bool i_isPressed )
{
	m_keys[i_key] = i_isPressed;
}

//------------------------------------------------------------------------