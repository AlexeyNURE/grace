#pragma once

#include <grace/core/utils/enum_utils.hpp>

//------------------------------------------------------------------------------

REGISTER_ENUM_CLASS(
	ComponentType,
	transform,
	motion,
	model,
	render,
	light
)

//------------------------------------------------------------------------------