#include "application/src/components/transform_component.hpp"

#include <json/json.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <grace/render/model/model.hpp>
#include <grace/core/utils/utils.hpp>

#include "application/src/project/project_loader.hpp"
#include "application/src/scene/scene.hpp"

#include "application/src/entities/entity.hpp"

//-----------------------------------------------------------------------------

TransformComponent::TransformComponent( Camera& i_camera ) :
	m_transformChanged( true ),
	m_camera( i_camera )
{
}

ComponentType TransformComponent::getComponentType()
{
	return ComponentType::transform;
}

void TransformComponent::update( float i_dt )
{
}

void TransformComponent::accept( ComponentConstVisitor& i_visitor ) const
{
	i_visitor.visit( *this );
}

void TransformComponent::accept( ComponentVisitor& i_visitor )
{
	i_visitor.visit( *this );
}

void TransformComponent::initComponent( const Json::Value& i_value )
{
	Component::initComponent( i_value );

	ASSERT( i_value, "Json value is invalid" );

	Json::Value position = i_value["position"];
	Json::Value rotation = i_value["rotation"];
	Json::Value scale = i_value["scale"];

	m_position = ProjectLoader::JsonToVector3( position );
	m_rotation = ProjectLoader::JsonToQuat( rotation );
	m_scale = ProjectLoader::JsonToVector3( scale );

	m_prevPosition = m_position;
}

std::unique_ptr<Component> TransformComponent::clone()
{
	TODO( "temp" )
	return nullptr;
}

void TransformComponent::setPosition( const glm::vec3& i_position )
{
	m_position = i_position;
	m_prevPosition = m_position;
	m_transformChanged = true;
}

void TransformComponent::setScale( const glm::vec3& i_scale )
{
	m_scale = i_scale;
	m_transformChanged = true;
}

void TransformComponent::setRotation( const glm::quat& i_rotation )
{
	m_rotation = i_rotation;
	m_transformChanged = true;
}

void TransformComponent::addOffset( const glm::vec3& i_offset )
{
	m_position = m_prevPosition + i_offset;
	m_transformChanged = true;
}

void TransformComponent::calculateModelMatrixInternal()
{
	glm::mat4x4 scale = glm::scale( m_scale );

	glm::mat4x4 translate = glm::translate( m_position );

	glm::mat4x4 rotateX = glm::rotate( m_rotation.x, Scene::getRightVector() );
	glm::mat4x4 rotateY = glm::rotate( m_rotation.y, Scene::getFrontVector() );
	glm::mat4x4 rotateZ = glm::rotate( m_rotation.z, Scene::getUpVector() );

	glm::mat4x4 rotation = rotateZ * rotateY * rotateX;

	m_modelMatrix = translate * rotation * scale;

	m_transformChanged = false;
}

glm::mat4x4& TransformComponent::calculateModelMatrix()
{
	if( m_transformChanged )
		calculateModelMatrixInternal();

	return m_modelMatrix;
}

glm::mat4x4 TransformComponent::calculateMVPMatrix()
{
	const glm::mat4x4& viewMatrix = m_camera.calculateViewMatrix();
	const glm::mat4x4& projectionMatrix = m_camera.getProjectionMatrix();

	glm::mat4x4 mvp = projectionMatrix * viewMatrix * calculateModelMatrix();
	return mvp;
}

glm::mat4x4 TransformComponent::getMVPMatrix() const
{
	const glm::mat4x4& viewMatrix = m_camera.getViewMatrix();
	const glm::mat4x4& projectionMatrix = m_camera.getProjectionMatrix();

	glm::mat4x4 mvp = projectionMatrix * viewMatrix * getModelMatrix();
	return mvp;
}

//-----------------------------------------------------------------------------