#include "application/src/components/motion_component.hpp"

#include <json/json.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "application/src/components/transform_component.hpp"

#include "application/src/scene/scene.hpp"

#include "application/src/entities/entity.hpp"

//-----------------------------------------------------------------------------

MotionComponent::MotionComponent( TransformComponent& i_transformCmp ) :
	m_transformCmp( i_transformCmp ),
	m_angle( 0.f ),
	m_speed( 0.f ),
	m_radius( 0.f )
{
}

void MotionComponent::update( float i_dt )
{
	m_angle += m_speed * i_dt;

	glm::vec3 diff;
	diff.x = m_radius * glm::cos( m_angle );
	diff.y = 0.0f;
	diff.z = m_radius * glm::sin( m_angle );

	m_transformCmp.addOffset( diff );
}

void MotionComponent::accept( ComponentConstVisitor& i_visitor ) const
{
	i_visitor.visit( *this );
}

void MotionComponent::accept( ComponentVisitor& i_visitor )
{
	i_visitor.visit( *this );
}

void MotionComponent::initComponent( const Json::Value& i_value )
{
	Component::initComponent( i_value );

	m_speed = i_value["speed"].asFloat();
	m_radius = i_value["radius"].asFloat();
	m_angle = i_value["angle"].asFloat();
}

std::unique_ptr<Component> MotionComponent::clone()
{
	TODO( "temp" )
	return nullptr;
}

void MotionComponent::setSpeed( float i_speed )
{
	m_speed = i_speed;
}

void MotionComponent::setRadius( float i_radius )
{
	m_radius = i_radius;
}

void MotionComponent::setAngle( float i_angle )
{
	m_angle = i_angle;
}

//-----------------------------------------------------------------------------