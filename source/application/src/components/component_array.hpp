#pragma once

#include <vector>
#include <type_traits>

#include <grace/core/utils/enum_utils.hpp>
#include <grace/render/renderer_API.hpp>

//------------------------------------------------------------------------------

class Entity;
class Component;

//------------------------------------------------------------------------------

class IComponentArray
{
public:
	virtual ~IComponentArray() = default;

	virtual void update( float i_dt ) = 0;

	virtual bool isEmpty() const = 0;
	virtual std::size_t getSize() const = 0;

	virtual void shrinkToFit() = 0;

	virtual Component* at( std::size_t i_idx ) = 0;
	virtual const Component* at( std::size_t i_idx ) const = 0;
};

//------------------------------------------------------------------------------

template<typename _ComponentType>
class ComponentArray : public IComponentArray
{
public:
	~ComponentArray() = default;

	void update( float i_dt ) override;

	bool isEmpty() const override;
	std::size_t getSize() const override;

	void shrinkToFit() override;

	Component* at( std::size_t ) override;
	const Component* at( std::size_t ) const override;

	template<typename T>
	void pushBack( std::unique_ptr<T> i_cmp );

	template<typename T>
	void erase( T* i_cmp );

private:
	std::vector<std::unique_ptr<_ComponentType>> m_components;
};

//-----------------------------------------------------------------------------

template<typename _ComponentType>
template<typename T>
inline void ComponentArray<_ComponentType>::pushBack( std::unique_ptr<T> i_cmp )
{
	static_assert( std::is_same_v<_ComponentType, T>, "Component types should be equal" );
	m_components.emplace_back( std::move( i_cmp ) );
}

template<typename _ComponentType>
template<typename T>
inline void ComponentArray<_ComponentType>::erase( T* i_cmp )
{
	static_assert( std::is_same_v<_ComponentType, T>, "Component types should be equal" );

	auto it = m_components.begin();
	while( it != m_components.end() )
	{
		if( it->get() == i_cmp )
		{
			it = m_components.erase( it );
		}
		else
			++it;
	}
}

//-----------------------------------------------------------------------------
