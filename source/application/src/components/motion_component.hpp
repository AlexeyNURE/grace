#pragma once

#include <string_view>

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>

#include "application/src/entities/entity_id.hpp"
#include "application/src/components/component.hpp"

//-----------------------------------------------------------------------------

class TransformComponent;

//-----------------------------------------------------------------------------

class MotionComponent : public Component
{
public:
	MotionComponent( TransformComponent& i_transformCmp );

	void update( float i_dt ) override;

	void accept( ComponentConstVisitor& i_visitor ) const override;
	void accept( ComponentVisitor& i_visitor ) override;

	static ComponentType getComponentType() { return ComponentType::motion; }

	void initComponent( const Json::Value& i_value );

	std::unique_ptr<Component> clone() override;

	void setSpeed( float i_speed );
	void setRadius( float i_radius );
	void setAngle( float i_angle );

	float getSpeed() const;
	float getRadius() const;
	float getAngle() const;

private:
	float m_speed;
	float m_radius;
	float m_angle;

	TransformComponent& m_transformCmp;
};

//------------------------------------------------------------------------------

inline float MotionComponent::getSpeed() const
{
	return m_speed;
}

inline float MotionComponent::getRadius() const
{
	return m_radius;
}

inline float MotionComponent::getAngle() const
{
	return m_angle;
}

//------------------------------------------------------------------------------