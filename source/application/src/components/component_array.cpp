#include "application/src/components/component_array.hpp"

#include "application/src/components/motion_component.hpp"
#include "application/src/components/transform_component.hpp"
#include "application/src/components/light_component.hpp"
#include "application/src/components/model_component.hpp"
#include "application/src/components/render_component.hpp"

//------------------------------------------------------------------------------

template<typename _ComponentType>
void ComponentArray<_ComponentType>::update( float i_dt )
{
	for( auto& cmp : m_components )
	{
		if( cmp->hasComponentTag( ComponentTags::update ) )
		{
			cmp->update( i_dt );
		}
	}
}

template<typename _ComponentType>
bool ComponentArray<_ComponentType>::isEmpty() const
{
	return m_components.empty();
}

template<typename _ComponentType>
std::size_t ComponentArray<_ComponentType>::getSize() const
{
	return m_components.size();
}

template<typename _ComponentType>
void ComponentArray<_ComponentType>::shrinkToFit()
{
	m_components.shrink_to_fit();
}

template<typename _ComponentType>
Component* ComponentArray<_ComponentType>::at( std::size_t i_idx )
{
	return m_components.at( i_idx ).get();
}

template<typename _ComponentType>
const Component* ComponentArray<_ComponentType>::at( std::size_t i_idx ) const
{
	return m_components.at( i_idx ).get();
}

//------------------------------------------------------------------------------

// explicit template instantiation
template class ComponentArray<TransformComponent>;
template class ComponentArray<MotionComponent>;
template class ComponentArray<LightComponent>;
template class ComponentArray<ModelComponent>;
template class ComponentArray<RenderComponent>;

//------------------------------------------------------------------------------