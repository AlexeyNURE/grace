#pragma once

#include <string_view>
#include <grace/core/utils/math/aabb3.hpp>

#include "application/src/entities/entity_id.hpp"
#include "application/src/components/component.hpp"
#include "application/src/scene/scene.hpp"

//-----------------------------------------------------------------------------

namespace gc
{
class Model;
class Shader;
}

//-----------------------------------------------------------------------------

class ModelComponent : public Component
{
public:
	ModelComponent( Scene& i_scene );
	virtual ~ModelComponent() = default;

	void update( float i_dt ) override;
	// it is called from RenderComponent
	void render( const gc::OpenGLRendererAPI& i_renderer ) const;

	void accept( ComponentConstVisitor& i_visitor ) const override;
	void accept( ComponentVisitor& i_visitor ) override;

	static ComponentType getComponentType() { return ComponentType::model; };

	void initComponent( const Json::Value& i_value ) override;

	std::unique_ptr<Component> clone() override;

	void setShader( const gc::Shader* i_shader );
	const gc::Shader* getShader() const;

	const gc::Model* getModel() const;
	gc::Model* getModel();

	std::string_view getModelName() const;
	gc::u32 getModelId() const;
	
	void setModel( std::string_view i_modelName );
	void setModelName( std::string_view i_modelName );

	void setRebderAabb( bool i_yes );
	boost::optional<gc::utils::aabb3f> getAabbWorld() const;

	boost::signals2::signal<void()> sigModelChanged;

private:
	Scene& m_scene;

	std::string m_modelFileName;
	gc::u32 m_modelId;

	std::unique_ptr<gc::Model> m_model;
	const gc::Shader* m_shader;

	bool m_renderAabb;
};

//------------------------------------------------------------------------------

inline std::string_view ModelComponent::getModelName() const
{
	return m_modelFileName;
}

inline gc::u32 ModelComponent::getModelId() const
{
	return m_modelId;
}

inline const gc::Model* ModelComponent::getModel() const
{
	return m_model.get();
}

inline gc::Model* ModelComponent::getModel()
{
	return m_model.get();
}

inline const gc::Shader* ModelComponent::getShader() const
{
	return m_shader;
}

//------------------------------------------------------------------------------