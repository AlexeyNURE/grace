#pragma once

#include <string_view>

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>

#include "application/src/entities/entity_id.hpp"
#include "application/src/components/component.hpp"
#include "application/src/camera/camera.hpp"

//-----------------------------------------------------------------------------

class Scene;

//-----------------------------------------------------------------------------

class TransformComponent : public Component
{
public:
	TransformComponent( Camera& i_camera );

	void update( float i_dt ) override;

	void accept( ComponentConstVisitor& i_visitor ) const override;
	void accept( ComponentVisitor& i_visitor ) override;

	static ComponentType getComponentType();

	void initComponent( const Json::Value& i_value );

	std::unique_ptr<Component> clone() override;

	void setPosition( const glm::vec3& i_position );
	void setScale( const glm::vec3& i_scale );
	void setRotation( const glm::quat& i_rotation );

	void addOffset( const glm::vec3& i_offset );

	const glm::vec3& getPosition() const;
	const glm::vec3& getScale() const;
	const glm::quat& getRotation() const;

	const glm::mat4x4& getModelMatrix() const;
	glm::mat4x4& calculateModelMatrix();
	glm::mat4x4 getMVPMatrix() const;
	glm::mat4x4 calculateMVPMatrix();

private:
	void calculateModelMatrixInternal();

private:
	Camera& m_camera;

	glm::vec3 m_position;
	glm::vec3 m_prevPosition;

	glm::quat m_rotation;
	glm::vec3 m_scale;
	
	glm::mat4x4 m_modelMatrix;

	bool m_transformChanged;
};

//------------------------------------------------------------------------------

inline const glm::vec3& TransformComponent::getPosition() const
{
	return m_position;
}

inline const glm::vec3& TransformComponent::getScale() const
{
	return m_scale;
}

inline const glm::quat& TransformComponent::getRotation() const
{
	return m_rotation;
}

inline const glm::mat4x4& TransformComponent::getModelMatrix() const
{
	return m_modelMatrix;
}

//------------------------------------------------------------------------------