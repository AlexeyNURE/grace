#pragma once

#include "grace/core/utils/visitor_cast.hpp"

//-----------------------------------------------------------------------------

class Component;
class TransformComponent;
class MotionComponent;
class LightComponent;
class ModelComponent;
class RenderComponent;

//-----------------------------------------------------------------------------

struct ComponentConstVisitor
{
	virtual ~ComponentConstVisitor() = default;

	virtual void visit( const Component& ) = 0;
	virtual void visit( const TransformComponent& ) = 0;
	virtual void visit( const MotionComponent& ) = 0;
	virtual void visit( const LightComponent& ) = 0;
	virtual void visit( const ModelComponent& ) = 0;
	virtual void visit( const RenderComponent& ) = 0;
};

//-----------------------------------------------------------------------------

struct ComponentVisitor
{
	virtual ~ComponentVisitor() = default;

	virtual void visit( Component& ) = 0;
	virtual void visit( TransformComponent& ) = 0;
	virtual void visit( MotionComponent& ) = 0;
	virtual void visit( LightComponent& ) = 0;
	virtual void visit( ModelComponent& ) = 0;
	virtual void visit( RenderComponent& ) = 0;
};

//-----------------------------------------------------------------------------

struct ComponentDefaultConstVisitor : public ComponentConstVisitor
{
	void visit( const Component& ) override {}
	void visit( const TransformComponent& ) override {}
	void visit( const MotionComponent& ) override {}
	void visit( const LightComponent& ) override {}
	void visit( const ModelComponent& ) override {}
	void visit( const RenderComponent& ) override {}
};

//------------------------------------------------------------------------------

struct ComponentDefaultVisitor : public ComponentVisitor
{
	void visit( Component& ) override {}
	void visit( TransformComponent& ) override {}
	void visit( MotionComponent& ) override {}
	void visit( LightComponent& ) override {}
	void visit( ModelComponent& ) override {}
	void visit( RenderComponent& ) override {}
};

//------------------------------------------------------------------------------