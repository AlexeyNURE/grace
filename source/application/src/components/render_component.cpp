#include <json/json.h>

#include <grace/render/texture/texture2D.hpp>
#include <grace/render/texture/cube_texture.hpp>

#include <grace/core/utils/utils.hpp>
#include <grace/core/utils/globals.hpp>

#include <grace/render/model/model.hpp>

#include "application/src/scene/scene.hpp"

#include "application/src/components/render_component.hpp"
#include "application/src/components/transform_component.hpp"
#include "application/src/components/light_component.hpp"

#include "application/src/resource/resource_manager.hpp"

#include "application/src/components/model_component.hpp"
#include "application/src/entities/entity.hpp"

//-----------------------------------------------------------------------------

RenderComponent::RenderComponent( Scene& o_scene, ModelComponent* i_modelCmp, TransformComponent* i_transformCmp ) :
	m_scene( o_scene ),
	m_shader( nullptr ),
	m_hasFog( false ),
	m_hasLight( false ),
	m_hasWater( false ),
	m_modelComponent( i_modelCmp ),
	m_transformComponent( i_transformCmp )
{
}

RenderComponent::~RenderComponent()
{
	m_scene.unregisterRenderable( *this );
}

void RenderComponent::update( float i_dt )
{
	// do nothing
}

void RenderComponent::render( const gc::OpenGLRendererAPI& i_renderer ) const
{
	if( !m_shader )
	{
		return;
	}

	m_shader->bind();
	uniformShaderParams();

	m_modelComponent->render( i_renderer );
}

void RenderComponent::accept( ComponentConstVisitor& i_visitor ) const
{
	i_visitor.visit( *this );
}

void RenderComponent::accept( ComponentVisitor& i_visitor )
{
	i_visitor.visit( *this );
}

void RenderComponent::initComponent( const Json::Value& i_value )
{
	Component::initComponent( i_value );

	Json::Value textures2D = i_value["2Dtex"];
	Json::Value cubeTextures = i_value["cubeTex"];
	m_shaderName = i_value["shader"].asCString();

	ResourceManager& resourceMgr = ResourceManager::getInstance();

	int textures2DCount = textures2D.size();
	for( int j = 0; j < textures2DCount; j++ )
	{
		const char* tex2DName = textures2D[j].asCString();
		setTexture2D( tex2DName, *resourceMgr.get2DTextureByName( tex2DName ) );
	}

	int cubeTexturesCount = cubeTextures.size();
	for( int j = 0; j < cubeTexturesCount; j++ )
	{
		const char* cubeTexName = cubeTextures[j].asCString();
		setCubeTexture( cubeTexName, *resourceMgr.getCubeTextureByName( cubeTexName ) );
	}

	setShader( resourceMgr.getShaderByName( m_shaderName ) );
}

void RenderComponent::setShader( const gc::Shader* i_shader )
{
	m_modelComponent->setShader( i_shader );
	m_shader = i_shader;
	m_shaderName = toString( i_shader->getShaderKind() );
	m_hasFog = m_shader->hasFogParams();
	m_hasLight = m_shader->hasLightParams();
	m_hasWater = m_shader->hasWaterParams();
}

void RenderComponent::setUniformValue( std::string_view i_name, float i_val )
{
	m_modelComponent->getModel()->forEachMaterial( [i_name, &i_val]( gc::Material& material )
	{
		material.setValue( i_name, i_val );
	} );
}

void RenderComponent::setUniformValue( std::string_view i_name, gc::s32 i_val )
{
	m_modelComponent->getModel()->forEachMaterial( [i_name, &i_val]( gc::Material& material )
	{
		material.setValue( i_name, i_val );
	} );
}

void RenderComponent::setUniformValue( std::string_view i_name, const glm::vec3& i_val )
{
	m_modelComponent->getModel()->forEachMaterial( [i_name, &i_val]( gc::Material& material )
	{
		material.setValue( i_name, i_val );
	} );
}

void RenderComponent::setUniformValue( std::string_view i_name, const glm::vec4& i_val )
{
	m_modelComponent->getModel()->forEachMaterial( [i_name, &i_val]( gc::Material& material )
	{
		material.setValue( i_name, i_val );
	} );
}

void RenderComponent::setTexture2D( std::string_view i_name, const gc::Texture2D& i_tex2D )
{
	m_modelComponent->getModel()->forEachMaterial( [i_name, &i_tex2D]( gc::Material& material )
	{
		material.setValue( i_name, i_tex2D );
	} );
}

void RenderComponent::setCubeTexture( std::string_view i_name, const gc::CubeTexture& i_cubeTex )
{
	m_modelComponent->getModel()->forEachMaterial( [i_name, &i_cubeTex]( gc::Material& material )
	{
		material.setValue( i_name, i_cubeTex );
	} );
}

std::unique_ptr<Component> RenderComponent::clone()
{
	TODO( "temp" )
	return nullptr;
}

void RenderComponent::uniformShaderParams() const
{
	if( m_shader->hasUniform( "u_tilingFactor" ) && m_params.tilingFactor )
		m_shader->uniformValue( "u_tilingFactor", m_params.tilingFactor.get() );

	uniformFog();

	uniformAmbient();

	if( m_shader->hasUniform( "u_time" ) )
		m_shader->uniformValue( "u_time", gc::Globals::s_totalTime );

	uniformLight();

	const glm::vec3& camPos = m_scene.getCamera().getPosition();
	if( m_shader->hasUniform( "u_camPos" ) )
		m_shader->uniformValue( "u_camPos", camPos );

	if( m_shader->hasUniform( "u_matM" ) )
	{
		m_shader->uniformValue( "u_matM", m_transformComponent->calculateModelMatrix() );
	}

	if( m_shader->hasUniform( "u_dMax" ) && m_params.dMax )
		m_shader->uniformValue( "u_dMax", m_params.dMax.get() );

	uniformWater();

	if( m_shader->hasUniform( "u_matMVP" ) )
	{
		glm::mat4x4 mvp = m_transformComponent->calculateMVPMatrix();
		m_shader->uniformValue( "u_matMVP", mvp );
	}
}

void RenderComponent::uniformAmbient() const
{
	if( m_shader->hasUniform( "u_ambientColor" ) )
		m_shader->uniformValue( "u_ambientColor", m_scene.getAmbientLight().color );

	if( m_shader->hasUniform( "u_ambientWeight" ) )
		m_shader->uniformValue( "u_ambientWeight", m_scene.getAmbientLight().weight );
}

void RenderComponent::uniformFog() const
{
	if( !m_hasFog )
		return;

	const gc::Fog& fog = m_scene.getFog();

	m_shader->uniformValue( "u_fogStart", fog.start );
	m_shader->uniformValue( "u_fogRange", fog.range );

	const glm::vec4& color = fog.color;
	m_shader->uniformValue( "u_fogColor", color );
}

void RenderComponent::uniformLight() const
{
	if( !m_hasLight )
		return;

	if( m_shader->hasUniform( "u_specularPower" ) && m_params.specular_power )
		m_shader->uniformValue( "u_specularPower", m_params.specular_power.get() );

	const auto& lightDirs = m_scene.getLightDirs();
	const auto& lightPoints = m_scene.getLightPoints();
	const auto& lightDirColors = m_scene.getLightDirColors();
	const auto& lightPointColors = m_scene.getLightPointColors();

	m_shader->uniformValue( "u_lightDirections", lightDirs );
	m_shader->uniformValue( "u_lightPositions", lightPoints );
	m_shader->uniformValue( "u_dirColors", lightDirColors );
	m_shader->uniformValue( "u_pointColors", lightPointColors );
}

void RenderComponent::uniformWater() const
{
	if( !m_hasWater )
		return;

	if( m_params.depth_displ )
		m_shader->uniformValue( "u_depthDispl", m_params.depth_displ.get() );
	if( m_params.depth_adjust )
		m_shader->uniformValue( "u_depthAdjust", m_params.depth_adjust.get() );
	if( m_params.color )
		m_shader->uniformValue( "u_waterColor", m_params.color.get() );
}

//-----------------------------------------------------------------------------