#pragma once

#include <string_view>
#include <glm/glm.hpp>

#include <grace/render/material/material.hpp>

#include "application/src/components/component.hpp"

//-----------------------------------------------------------------------------

namespace gc
{
class Shader;
class Texture2D;
class CubeTexture;
}

class ModelComponent;
class TransformComponent;
class Scene;

//-----------------------------------------------------------------------------

struct RenderParams
{
	boost::optional<float> tilingFactor;
	boost::optional<float> dMax;
	boost::optional<float> depth_displ;
	boost::optional<float> depth_adjust;
	boost::optional<glm::vec4> color;
	boost::optional<float> specular_power;
};

//-----------------------------------------------------------------------------

class RenderComponent : public Component, public Renderable
{
public:
	RenderComponent( Scene& o_scene, ModelComponent* i_modelCmp, TransformComponent* i_transformCmp );
	virtual ~RenderComponent();

	void update( float i_dt ) override;
	void render( const gc::OpenGLRendererAPI& i_renderer ) const override;

	void initComponent( const Json::Value& i_value ) override;

	void accept( ComponentConstVisitor& i_visitor ) const override;
	void accept( ComponentVisitor& i_visitor ) override;

	static ComponentType getComponentType() { return ComponentType::render; }

	std::unique_ptr<Component> clone() override;

	const gc::Shader* getShader() const;
	std::string_view getShaderName() const;
	
	void setShader( const gc::Shader* i_shader );

	void setUniformValue( std::string_view i_name, float i_val );
	void setUniformValue( std::string_view i_name, gc::s32 i_val );
	void setUniformValue( std::string_view i_name, const glm::vec3& i_val );
	void setUniformValue( std::string_view i_name, const glm::vec4& i_val );

	void setTexture2D( std::string_view i_name, const gc::Texture2D& i_tex2D );
	void setCubeTexture( std::string_view i_name, const gc::CubeTexture& i_cubeTex );

	RenderParams& getParams();
	const RenderParams& getParams() const;

private:
	void uniformShaderParams() const;
	void uniformFog() const;
	void uniformLight() const;
	void uniformWater() const;
	void uniformAmbient() const;

private:
	Scene& m_scene;

	RenderParams m_params;

	const gc::Shader* m_shader;
	std::string m_shaderName;

	ModelComponent* m_modelComponent;
	TransformComponent* m_transformComponent;
	const LightComponent* m_lightComponent;

	bool m_hasFog;
	bool m_hasLight;
	bool m_hasWater;
};

//------------------------------------------------------------------------------

inline RenderParams& RenderComponent::getParams()
{
	return m_params;
}

inline const RenderParams& RenderComponent::getParams() const
{
	return m_params;
}

inline const gc::Shader* RenderComponent::getShader() const
{
	return m_shader;
}

inline std::string_view RenderComponent::getShaderName() const
{
	return m_shaderName;
}

//------------------------------------------------------------------------------