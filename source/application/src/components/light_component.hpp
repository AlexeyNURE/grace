#pragma once

#include <glm/vec4.hpp>

#include "application/src/components/component.hpp"

//-----------------------------------------------------------------------------

namespace gc
{
class Light;
}
class TransformComponent;
class Scene;

//-----------------------------------------------------------------------------

class LightComponent : public Component
{
public:
	LightComponent( Scene& i_scene, const TransformComponent& i_transformCmp );
	virtual ~LightComponent();

	void update( float i_dt ) override;

	void accept( ComponentConstVisitor& i_visitor ) const override;
	void accept( ComponentVisitor& i_visitor ) override;

	static ComponentType getComponentType() { return ComponentType::light; }

	void initDefault();
	void initComponent( const Json::Value& i_value );

	std::unique_ptr<Component> clone() override;

	const gc::Light* getLight() const;

	const glm::vec4& getColor() const;
	void setColor( const glm::vec4& );

private:
	gc::Light* m_light;

	Scene& m_scene;

	const TransformComponent& m_transformCmp;
};

//------------------------------------------------------------------------------

inline const gc::Light* LightComponent::getLight() const
{
	return m_light;
}

//------------------------------------------------------------------------------