#include "application/src/components/component.hpp"

#include "application/src/components/component_visitor.hpp"

//------------------------------------------------------------------------------

Component::Component()
{
	// update & render by default
	setComponentTag( ComponentTags::render );
	setComponentTag( ComponentTags::update );
}

void Component::setComponentTag( ComponentTags tag )
{
	m_tags.set( tag );
}

void Component::clearComponentTag( ComponentTags tag )
{
	m_tags.reset( tag );
}

bool Component::hasComponentTag( ComponentTags tag ) const
{
	return m_tags.test( tag );
}

void Component::setOwner( Entity* i_entity )
{
	m_entity = i_entity;
}

ComponentType Component::getComponentType()
{
	return ComponentType::INVALID;
}

void Component::initComponent( const Json::Value& i_value )
{
	// update & render by default
	setComponentTag( ComponentTags::render );
	setComponentTag( ComponentTags::update );
}

//------------------------------------------------------------------------------