#pragma once

#include <boost/noncopyable.hpp>
#include <memory>
#include <vector>
#include <unordered_map>

#include "application/src/entities/entity.hpp"

#include "application/src/components/model_component.hpp"
#include "application/src/components/transform_component.hpp"
#include "application/src/components/light_component.hpp"
#include "application/src/components/motion_component.hpp"
#include "application/src/components/render_component.hpp"

#include "application/src/components/component_array.hpp"

//-----------------------------------------------------------------------------

class Entity;

//-----------------------------------------------------------------------------

class ComponentManager : boost::noncopyable
{
public:
	using ComponentID = gc::u32;

public:
	ComponentManager() = default;
	~ComponentManager() = default;

	void init();

	void update( float i_dt );

	template<typename T>
	void addComponent( std::unique_ptr<T> i_component, Entity* i_entity );
	template<typename T>
	void removeComponent( T* i_component, Entity* i_entity );

	void removeEntityComponents( Entity* i_entity );

	template<typename T>
	boost::optional<T&> getComponent( EntityId i_id );
	template<typename T>
	boost::optional<const T&> getComponent( EntityId i_id ) const;

	template<typename T>
	ComponentArray<T>& getComponentArray();
	template<typename T>
	const ComponentArray<T>& getComponentArray() const;

	template<typename T>
	bool hasComponent() const;

private:
	template<typename T>
	void registerComponent();

	std::vector<std::unique_ptr<IComponentArray>> m_componentArrays;
	std::unordered_map<ComponentType, std::size_t /*idx*/> m_componentTypeToIdx;

	std::unordered_map<EntityId, std::vector<Component*>> m_componentsMap;
};

//------------------------------------------------------------------------------

template<typename T>
boost::optional<T&> ComponentManager::getComponent( EntityId i_id )
{
	auto& components = m_componentsMap[i_id];
	for( auto& cmp : components )
	{
		if( T* result = dynamic_cast<T*>( cmp ) )
		{
			return *result;
		}
	}

	return boost::none;
}

template<typename T>
boost::optional<const T&> ComponentManager::getComponent( EntityId i_id ) const
{
	const auto& components = m_componentsMap[i_id];
	for( const auto& cmp : components )
	{
		if( const T* result = dynamic_cast<const T*>( cmp ) )
		{
			return *result;
		}
	}

	return boost::none;
}

template<typename T>
inline ComponentArray<T>& ComponentManager::getComponentArray()
{
	auto it = m_componentTypeToIdx.find( T::getComponentType() );
	if( it == m_componentTypeToIdx.end() )
		ASSERT( false, "No such component in components array" );

	std::size_t idx = m_componentTypeToIdx[T::getComponentType()];
	auto cmp = dynamic_cast<ComponentArray<T>*>( m_componentArrays[idx].get() );
	return *cmp;
}

template<typename T>
inline const ComponentArray<T>& ComponentManager::getComponentArray() const
{
	auto it = m_componentTypeToIdx.find( T::getComponentType() );
	if( it == m_componentTypeToIdx.end() )
		ASSERT( false, "No such component in components array" );

	std::size_t idx = m_componentTypeToIdx[T::getComponentType()];
	auto cmp = dynamic_cast<ComponentArray<T>*>( m_componentArrays[idx].get() );
	return *cmp;
}

template<typename T>
inline bool ComponentManager::hasComponent() const
{
	auto it = m_componentTypeToIdx.find( T::getComponentType() );
	if( it == m_componentTypeToIdx.end() )
		return false;

	std::size_t idx = m_componentTypeToIdx[T::getComponentType()];
	return !m_componentArrays[idx]->isEmpty();
}

template<typename T>
inline void ComponentManager::registerComponent()
{
	// just create empty componentArray
	std::unique_ptr<ComponentArray<T>> array = std::make_unique<ComponentArray<T>>();
	m_componentArrays.emplace_back( std::move( array ) );

	std::size_t lastElement = m_componentArrays.size() - 1;
	m_componentTypeToIdx[T::getComponentType()] = lastElement;
}

template<typename T>
inline void ComponentManager::addComponent( std::unique_ptr<T> i_component, Entity* i_entity )
{
	m_componentsMap[i_entity->getId()].emplace_back( i_component.get() );
	i_entity->addComponent( i_component.get() );

	auto& cmpArray = getComponentArray<T>();
	cmpArray.pushBack( std::move( i_component ) );
}

template<typename T>
void ComponentManager::removeComponent( T* i_component, Entity* i_entity )
{
	m_componentsMap.erase( i_entity->getId() );
	i_entity->removeComponent( i_component );

	auto& cmpArray = getComponentArray<T>();
	cmpArray.erase( i_component );
}

//------------------------------------------------------------------------------
