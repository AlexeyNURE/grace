#include "application/src/components/component_manager.hpp"

//------------------------------------------------------------------------------

void ComponentManager::init()
{
	registerComponent<TransformComponent>();
	registerComponent<MotionComponent>();
	registerComponent<LightComponent>();
	registerComponent<ModelComponent>();
	registerComponent<RenderComponent>();
}

void ComponentManager::removeEntityComponents( Entity* i_entity )
{
	if( auto cmp = i_entity->getComponent<TransformComponent>() )
		removeComponent<TransformComponent>( &cmp.get(), i_entity );
	if( auto cmp = i_entity->getComponent<MotionComponent>() )
		removeComponent<MotionComponent>( &cmp.get(), i_entity );
	if( auto cmp = i_entity->getComponent<LightComponent>() )
		removeComponent<LightComponent>( &cmp.get(), i_entity );
	if( auto cmp = i_entity->getComponent<ModelComponent>() )
		removeComponent<ModelComponent>( &cmp.get(), i_entity );
	if( auto cmp = i_entity->getComponent<RenderComponent>() )
		removeComponent<RenderComponent>( &cmp.get(), i_entity );
}

void ComponentManager::update( float i_dt )
{
	for( auto& cmpArray : m_componentArrays )
	{
		cmpArray->update( i_dt );
	}
}

//------------------------------------------------------------------------------
