#pragma once

#include <memory>
#include <bitset>

#include <grace/render/renderer_API.hpp>
#include "application/src/components/component_visitor.hpp"
#include "application/src/components/component_type.hpp"
#include "application/src/scene/renderable.hpp"

//-----------------------------------------------------------------------------

namespace Json
{
	class Value;
}
class Entity;

//-----------------------------------------------------------------------------

enum ComponentTags
{
	update = 0, // 0x01
	render = 1, // 0x02
	COUNT = 2
};

//-----------------------------------------------------------------------------

class Component
{
public:
	using ComponentID = gc::u32;
	Component();
	virtual ~Component() = default;

	virtual void update( float i_dt ) {};

	virtual void accept( ComponentConstVisitor& i_visitor ) const = 0;
	virtual void accept( ComponentVisitor& i_visitor ) = 0;

	virtual std::unique_ptr<Component> clone() = 0;

	static ComponentType getComponentType();

	virtual void initComponent( const Json::Value& i_value );

	void setComponentTag( ComponentTags i_tag );
	void clearComponentTag( ComponentTags i_tag );
	bool hasComponentTag( ComponentTags i_tag ) const;

	void setEnabled( bool i_yes );
	bool isEnabled() const;

	void setOwner( Entity* i_entity );

	static ComponentID getComponentTypeID() { return 1; }

	Entity* getOwner();
	const Entity* getOwner() const;

protected:
	std::bitset<ComponentTags::COUNT> m_tags;
	bool m_isEnabled{ true };

	Entity* m_entity;
};

//------------------------------------------------------------------------------

DEFINE_CAST( Component );

//------------------------------------------------------------------------------

inline Entity* Component::getOwner()
{
	return m_entity;
}

inline const Entity* Component::getOwner() const
{
	return m_entity;
}

inline void Component::setEnabled( bool i_yes )
{
	m_isEnabled = i_yes;
}

inline bool Component::isEnabled() const
{
	return m_isEnabled;
}

//------------------------------------------------------------------------------
