#include "application/src/components/light_component.hpp"

#include <json/json.h>

#include <grace/render/light/light.hpp>

#include "application/src/project/project_loader.hpp"

#include "application/src/scene/scene.hpp"

#include "application/src/components/transform_component.hpp"
#include "application/src/components/motion_component.hpp"

//-----------------------------------------------------------------------------

LightComponent::LightComponent( Scene& i_scene, const TransformComponent& i_transformCmp ) :
	m_light( nullptr ),
	m_transformCmp( i_transformCmp ),
	m_scene( i_scene )
{
}

LightComponent::~LightComponent()
{
	m_scene.removeLight( *m_light );
}

void LightComponent::update( float i_dt )
{
	const auto& position = m_transformCmp.getPosition();
	if( m_light->getPositionDir() != m_transformCmp.getPosition() )
	{
		m_light->setPositionDir( position );
	}
}

void LightComponent::accept( ComponentConstVisitor& i_visitor ) const
{
	i_visitor.visit( *this );
}

void LightComponent::accept( ComponentVisitor& i_visitor )
{
	i_visitor.visit( *this );
}

void LightComponent::initDefault()
{
	const glm::vec3& pos = m_transformCmp.getPosition();

	std::unique_ptr<gc::Light> light = std::make_unique<gc::Light>( gc::LightType::point, pos, glm::vec4( 1.f, 1.f, 1.f, 1.f ) );
	m_light = light.get();

	m_scene.addLight( std::move( light ) );
}

void LightComponent::initComponent( const Json::Value& i_value )
{
	Component::initComponent( i_value );

	glm::vec4 color = ProjectLoader::JsonToVector4( i_value["color"] );
	const glm::vec3& pos = m_transformCmp.getPosition();

	std::unique_ptr<gc::Light> light = std::make_unique<gc::Light>( gc::LightType::point, pos, color );
	m_light = light.get();

	m_scene.addLight( std::move( light ) );
}

std::unique_ptr<Component> LightComponent::clone()
{
	TODO( "temp" )
	return nullptr;
}

const glm::vec4& LightComponent::getColor() const
{
	return m_light->getColor();
}

void LightComponent::setColor( const glm::vec4& i_color )
{
	m_light->setColor( i_color );
}

//-----------------------------------------------------------------------------