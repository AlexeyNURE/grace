#include "application/src/components/model_component.hpp"

#include <json/json.h>

#include <grace/render/model/model.hpp>
#include <grace/render/shader/shader.hpp>
#include <grace/render/material/material.hpp>
#include <grace/core/utils/utils.hpp>

#include "application/src/gui/applicationController.hpp"

#include "application/src/entities/entity.hpp"
#include "application/src/resource/resource_manager.hpp"
#include "application/src/resource/model_loader.hpp"
#include "application/src/scene/scene.hpp"

#include "application/src/components/transform_component.hpp"

//-----------------------------------------------------------------------------

ModelComponent::ModelComponent( Scene& o_scene ) :
	m_scene( o_scene ),
	m_modelId( -1 ),
	m_model( nullptr ),
	m_renderAabb( false ),
	m_shader( nullptr )
{
}

void ModelComponent::update( float i_dt )
{
	// do nothing
}

void ModelComponent::render( const gc::OpenGLRendererAPI& i_renderer ) const
{
	if( !m_model )
	{
		return;
	}

	i_renderer.render( m_model.get() );

	m_model->unbind();

	if( m_renderAabb )
	{
		m_scene.getRenderer3D().drawEntityAabb( i_renderer, *getOwner() );
	}
}

void ModelComponent::accept( ComponentConstVisitor& i_visitor ) const
{
	i_visitor.visit( *this );
}

void ModelComponent::accept( ComponentVisitor& i_visitor )
{
	i_visitor.visit( *this );
}

void ModelComponent::initComponent( const Json::Value& i_value )
{
	Component::initComponent( i_value );
	// we render model manually from RenderComponent
	clearComponentTag( ComponentTags::render );

	ResourceManager& resManager = ResourceManager::getInstance();
	
	m_modelFileName = i_value["model"].asCString();
	m_model = ModelLoader::loadModel( m_modelFileName );
}

void ModelComponent::setModel( std::string_view i_modelName )
{
	AppController::makeContextCurrent();
	m_modelFileName = i_modelName;
	
	m_model.reset();

	m_model = ModelLoader::loadModel( m_modelFileName );
	
	m_model->forEachMaterial( [this]( gc::Material& material )
	{
		setShader( m_shader );
	} );

	sigModelChanged();
}

void ModelComponent::setModelName( std::string_view i_modelName )
{
	m_modelFileName = i_modelName;
}

void ModelComponent::setShader( const gc::Shader* i_shader )
{
	m_shader = i_shader;
	m_model->forEachMaterial( [this]( gc::Material& material )
	{
		material.setShader( m_shader );
	} );
}

void ModelComponent::setRebderAabb( bool i_yes )
{
	m_renderAabb = i_yes;
}

boost::optional<gc::utils::aabb3f> ModelComponent::getAabbWorld() const
{
	if( !m_model )
	{
		return boost::none;
	}
	auto& modelMatrix = getOwner()->getComponent<TransformComponent>()->getModelMatrix();

	const auto& aabb = m_model->getBoundingBox();

	glm::vec3 corners[8];
	aabb.getCorenrs( corners );
	glm::vec4 pos0 = modelMatrix * glm::vec4( corners[0], 1.f );
	glm::vec4 pos7 = modelMatrix * glm::vec4( corners[7], 1.f );

	gc::utils::aabb3f res( pos0, pos7 );

	return res;
}

std::unique_ptr<Component> ModelComponent::clone()
{
	TODO( "temp" )
	return nullptr;
}

//-----------------------------------------------------------------------------
