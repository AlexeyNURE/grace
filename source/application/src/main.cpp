#include <grace/grace.hpp>
#include <memory>
#include <qapplication.h>

#include "application/src/gui/mainWindow.hpp"

//-----------------------------------------------------------------------------

int main(int argc, char* argv[])
{
	QApplication QtApp( argc, argv );

	gc::Log::Init();

	LOG_CORE_INFO( "Creating application: width - {}, height - {}", gc::Globals::s_screenWidth, gc::Globals::s_screenHeight );

	MainWindow mainWindow;
	mainWindow.init();
	mainWindow.show();

	return QtApp.exec();
}

//-----------------------------------------------------------------------------