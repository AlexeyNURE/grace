#pragma once

#include <string>
#include <istream>
#include <boost/noncopyable.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/quaternion.hpp>

//-----------------------------------------------------------------------------

namespace Json
{
class Value;
}
class Scene;
class Project;
class ComponentManager;

//-----------------------------------------------------------------------------

class ProjectLoader : public boost::noncopyable
{
public:
	static std::unique_ptr<Project> load( std::istream& o_stream );

	static glm::vec3 JsonToVector3( const Json::Value& i_value );
	static glm::vec4 JsonToVector4( const Json::Value& i_value );
	static glm::quat JsonToQuat( const Json::Value& i_value );

private:
	static void initEntities( const Json::Value& i_value, Project& o_project );
	static void initCamera( const Json::Value& i_value, Scene& o_scene );
	static void initFog( const Json::Value& i_value, Scene& o_scene );
	static void initLight( const Json::Value& i_value, Scene& o_scene );
	static void initAmbientLight( const Json::Value& i_value, Scene& o_scene );
};

//-----------------------------------------------------------------------------