#include <json/json.h>
#include <fstream>

#include <grace/core/utils/utils.hpp>
#include <grace/render/fog/fog.hpp>

#include "application/src/project/project_loader.hpp"
#include "application/src/project/project.hpp"

#include "application/src/scene/scene.hpp"

#include "application/src/entities/object_entity.hpp"

#include "application/src/components/component_manager.hpp"
#include "application/src/components/model_component.hpp"
#include "application/src/components/transform_component.hpp"
#include "application/src/components/motion_component.hpp"
#include "application/src/components/light_component.hpp"

//-----------------------------------------------------------------------------

glm::vec3 ProjectLoader::JsonToVector3( const Json::Value& i_value )
{
	return glm::vec3(
		i_value[0].asFloat(),
		i_value[1].asFloat(),
		i_value[2].asFloat()
	);
}

glm::vec4 ProjectLoader::JsonToVector4( const Json::Value& i_value )
{
	return glm::vec4(
		i_value[0].asFloat(),
		i_value[1].asFloat(),
		i_value[2].asFloat(),
		i_value[3].asFloat()
	);
}

glm::quat ProjectLoader::JsonToQuat( const Json::Value& i_value )
{
	return glm::quat(
		i_value[3].asFloat(), // w
		i_value[0].asFloat(),
		i_value[1].asFloat(),
		i_value[2].asFloat()
	);
}

std::unique_ptr<Project> ProjectLoader::load( std::istream& i_stream )
{
	Json::Value root;
	i_stream >> root;

	std::unique_ptr<Project> project = std::make_unique<Project>();
	Scene& scene = *project->getScene();

	Json::Value entities = root["entities"];
	if( !entities.empty() )
		initEntities( entities, *project );

	Json::Value ambLight = root["ambient_light"];
	if( !ambLight.empty() )
		initAmbientLight( ambLight, scene );

	Json::Value light = root["lights"];
	if( !light.empty() )
		initLight( light, scene );

	Json::Value camera = root["camera"];
	if( !camera.empty() )
		initCamera( camera, scene );

	Json::Value fog = root["fog"];
	if( !fog.empty() )
		initFog( fog, scene );

	return project;
}

void ProjectLoader::initEntities( const Json::Value& i_value, Project& o_project )
{
	ComponentManager& componentMgr = o_project.getComponentManager();
	Scene& scene = *o_project.getScene();

	size_t objectsCount = i_value.size();
	for( size_t i = 0; i < objectsCount; i++ )
	{
		Json::Value currentEntity = i_value[static_cast<int>( i )];

		const char* name = currentEntity["name"].asCString();
		gc::u32 id = currentEntity["ID"].asUInt();
		Json::Value modelComponentValue = currentEntity["modelComponent"];
		Json::Value transformComponentValue = currentEntity["transformComponent"];
		Json::Value motionComponentValue = currentEntity["motionComponent"];
		Json::Value lightComponentValue = currentEntity["lightComponent"];
		Json::Value renderComponentValue = currentEntity["renderComponent"];

		std::unique_ptr<ObjectEntity> obj = std::make_unique<ObjectEntity>( EntityId( id ), name );

		if( modelComponentValue )
		{
			std::unique_ptr<ModelComponent> modelCmp = std::make_unique<ModelComponent>( scene );
			modelCmp->initComponent( modelComponentValue );
			componentMgr.addComponent( std::move( modelCmp ), obj.get() );
		}

		if( transformComponentValue )
		{
			std::unique_ptr<TransformComponent> transformCmp = std::make_unique<TransformComponent>( scene.getCamera() );
			transformCmp->initComponent( transformComponentValue );
			componentMgr.addComponent( std::move( transformCmp ), obj.get() );
		}

		if( motionComponentValue )
		{
			auto transformCmp = componentMgr.getComponent<TransformComponent>( obj->getId() );
			std::unique_ptr<MotionComponent> motionCmp = std::make_unique<MotionComponent>( transformCmp.get() );
			motionCmp->initComponent( motionComponentValue );
			componentMgr.addComponent( std::move( motionCmp ), obj.get() );
		}

		if( lightComponentValue )
		{
			auto transformCmp = componentMgr.getComponent<TransformComponent>( obj->getId() );
			std::unique_ptr<LightComponent> lightCmp = std::make_unique<LightComponent>( scene, transformCmp.get() );
			lightCmp->initComponent( lightComponentValue );
			componentMgr.addComponent( std::move( lightCmp ), obj.get() );
		}

		if( renderComponentValue )
		{
			auto transformCmpOpt = componentMgr.getComponent<TransformComponent>( obj->getId() );
			auto modelCmpOpt = componentMgr.getComponent<ModelComponent>( obj->getId() );
			std::unique_ptr<RenderComponent> renderCmp = std::make_unique<RenderComponent>( scene, &modelCmpOpt.get(), &transformCmpOpt.get() );

			RenderParams& objParams = renderCmp->getParams();

			Json::Value value;
			Json::Value params = renderComponentValue["parameters"];

			if( value = params["tiling_factor"] )
			{
				objParams.tilingFactor = value.asFloat();
			}
			if( value = params["dMax"] )
			{
				objParams.dMax = value.asFloat();
			}
			if( value = params["depth_displ"] )
			{
				objParams.depth_displ = value.asFloat();
			}
			if( value = params["depth_adjust"] )
			{
				objParams.depth_adjust = value.asFloat();
			}
			if( value = params["color"] )
			{
				objParams.color = JsonToVector4( value );
			}
			if( value = params["specular_power"] )
			{
				objParams.specular_power = value.asFloat();
			}

			renderCmp->initComponent( renderComponentValue );
			scene.registerRenderable( *renderCmp.get() );
			componentMgr.addComponent( std::move( renderCmp ), obj.get() );
		}

		o_project.addEntity( std::move( obj ) );
	}
}

void ProjectLoader::initAmbientLight( const Json::Value& i_value, Scene& o_scene )
{
	gc::AmbientLight ambientLight = gc::AmbientLight();

	Json::Value color = i_value["color"];
	float weight = i_value["weight"].asFloat();

	ambientLight.color = JsonToVector4( color );
	ambientLight.weight = weight;

	o_scene.setAmbientLight( ambientLight );
}

void ProjectLoader::initLight( const Json::Value& i_value, Scene& o_scene )
{
	gc::u32 lightsCount = i_value.size();

	for( gc::u32 i = 0; i < lightsCount; i++ )
	{
		Json::Value currentLight = i_value[i];

		gc::LightType lightType = gc::fromString<gc::LightType>( currentLight["type"].asString() );
		glm::vec4 color = JsonToVector4( currentLight["color"] );
		glm::vec3 posDir = JsonToVector3( currentLight["posDir"] );

		std::unique_ptr<gc::Light> light = std::make_unique<gc::Light>( lightType, posDir, color );
		
		o_scene.addLight( std::move( light ) );
	}
}

void ProjectLoader::initCamera( const Json::Value& i_value, Scene& o_scene )
{
	float camNear = i_value["near"].asFloat();
	float camFar = i_value["far"].asFloat();
	float camFov = i_value["fov"].asFloat();
	float camSpeed = i_value["speed"].asFloat();
	float rot_speed = i_value["rot_speed"].asFloat();
	Json::Value position = i_value["position"];
	Json::Value rotation = i_value["rotation"];

	Camera camera;
	camera.init( camNear, camFar, camFov, camSpeed, rot_speed );
	camera.setPosition( JsonToVector3( position ) );
	camera.setRotation( JsonToQuat( rotation ) );

	o_scene.setCamera( camera );
}

void ProjectLoader::initFog( const Json::Value& i_value, Scene& o_scene )
{
	gc::Fog fog;
	Json::Value color = i_value["color"];

	fog.color = JsonToVector4( color );
	fog.start = i_value["start"].asFloat();
	fog.range = i_value["range"].asFloat();

	o_scene.setFog( fog );
}

//-----------------------------------------------------------------------------