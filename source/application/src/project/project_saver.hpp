#pragma once

#include <string>
#include <boost/noncopyable.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/quaternion.hpp>

#include "application/src/components/component_visitor.hpp"

//-----------------------------------------------------------------------------

namespace Json
{
class Value;
}
class Scene;
class Project;
class ComponentManager;
class Entity;

//-----------------------------------------------------------------------------

class ProjectSaver : public boost::noncopyable
{
	//-----------------------------------------------------------------------------

	struct ComponentSaverVisitor : public ComponentConstVisitor
	{
		ComponentSaverVisitor( Json::Value& i_jsonValue );

		void visit( const Component& ) override;
		void visit( const TransformComponent& ) override;
		void visit( const MotionComponent& ) override;
		void visit( const LightComponent& ) override;
		void visit( const ModelComponent& ) override;
		void visit( const RenderComponent& ) override;

		Json::Value& entityValue;
	};

	//-----------------------------------------------------------------------------

public:
	static void save( std::ostream& o_stream, const Project& i_project );

	static Json::Value vec3ToJson( const glm::vec3& _val ) noexcept;
	static Json::Value vec4ToJson( const glm::vec4& _val ) noexcept;
	static Json::Value quatToJson( const glm::quat& _val ) noexcept;

	//-----------------------------------------------------------------------------

private:
	static Json::Value saveEntities( const Project& i_project );
	static Json::Value saveEntity( const Entity& i_entity );
	static Json::Value saveCamera( const Scene& i_scene );
	static Json::Value saveFog( const Scene& i_scene );
	static Json::Value saveLight( const Scene& i_scene );
	static Json::Value saveAmbientLight( const Scene& i_scene );
};

//-----------------------------------------------------------------------------