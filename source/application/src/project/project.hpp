#pragma once

#include <vector>
#include <memory>

#include <grace/core/utils/math/ray3.hpp>

#include "application/src/entities/entity_id.hpp"
#include "application/src/components/component_manager.hpp"

//-----------------------------------------------------------------------------

class Entity;
class Scene;

//-----------------------------------------------------------------------------

class Project : public boost::noncopyable
{
public:
	Project();

	void update( float i_dt );
	void renderScene( const gc::OpenGLRendererAPI& i_rendererAPI );

	void addEntity( std::unique_ptr<Entity> i_entity );
	void removeSelectedEntity();

	Entity* getEntityById( EntityId i_id );
	const Entity* getEntityById( EntityId i_id ) const;

	Entity* getEntityByName( std::string_view i_name );
	const Entity* getEntityByName( std::string_view i_name ) const;

	void setSelected( std::string_view i_name );

	Entity* getSelected();
	const Entity* getSelected() const;

	Scene* getScene();
	const Scene* getScene() const;

	ComponentManager& getComponentManager();
	const ComponentManager& getComponentManager() const;

	void onRayPicked( const gc::utils::ray3f& i_ray );

	void forEachEntity( std::function<void( const Entity& )> i_callback ) const;
	void forEachEntity( std::function<void( Entity& )> i_callback );

public:
	boost::signals2::signal<void( Entity* )> sigEntitySelected;

private:
	std::unique_ptr<Scene> m_scene;

	std::vector<std::unique_ptr<Entity>> m_entities;

	ComponentManager m_componentManager;

	Entity* m_selectedEntity{ nullptr };
};

//------------------------------------------------------------------------------

inline Scene* Project::getScene()
{
	return m_scene.get();
}

inline const Scene* Project::getScene() const
{
	return m_scene.get();
}

inline ComponentManager& Project::getComponentManager()
{
	return m_componentManager;
}

inline const ComponentManager& Project::getComponentManager() const
{
	return m_componentManager;
}

inline Entity* Project::getSelected()
{
	return m_selectedEntity;
}

inline const Entity* Project::getSelected() const
{
	return m_selectedEntity;
}

//------------------------------------------------------------------------------