#include <json/json.h>
#include <fstream>

#include <grace/core/utils/utils.hpp>
#include <grace/render/texture/texture2D.hpp>
#include <grace/render/fog/fog.hpp>
#include <grace/render/model/model.hpp>
#include <grace/render/texture/cube_texture.hpp>

#include "application/src/project/project_saver.hpp"
#include "application/src/project/project.hpp"

#include "application/src/scene/scene.hpp"

#include "application/src/entities/object_entity.hpp"

#include "application/src/components/component_manager.hpp"
#include "application/src/components/model_component.hpp"
#include "application/src/components/transform_component.hpp"
#include "application/src/components/motion_component.hpp"
#include "application/src/components/light_component.hpp"

//-----------------------------------------------------------------------------

Json::Value ProjectSaver::vec3ToJson( const glm::vec3& _val ) noexcept
{
	Json::Value result;

	result.append( _val.x );
	result.append( _val.y );
	result.append( _val.z );

	return result;
}

Json::Value ProjectSaver::vec4ToJson( const glm::vec4& _val ) noexcept
{
	Json::Value result;

	result.append( _val.x );
	result.append( _val.y );
	result.append( _val.z );
	result.append( _val.w );

	return result;
}

Json::Value ProjectSaver::quatToJson( const glm::quat& _val ) noexcept
{
	Json::Value result;

	result.append( _val.x );
	result.append( _val.y );
	result.append( _val.z );
	result.append( _val.w );

	return result;
}

//-----------------------------------------------------------------------------

struct MaterialPropertySaver
{
	MaterialPropertySaver( const gc::MaterialData& i_materialData, Json::Value& i_renderCmpValue ) :
		materialData( i_materialData ),
		renderCmpValue( i_renderCmpValue )
	{
	}

	void operator() ( const gc::Texture2D* i_texture ) const
	{
		renderCmpValue["2Dtex"].append( materialData.m_name.c_str() );
	}

	void operator() ( const gc::CubeTexture* i_texture ) const
	{
		renderCmpValue["cubeTex"].append( materialData.m_name.c_str() );
	}

	template<typename T>
	void operator() ( const T& i_value ) const
	{
		renderCmpValue["val"].append( materialData.m_name.c_str() );
	}

	const gc::MaterialData& materialData;
	Json::Value& renderCmpValue;
};

//-----------------------------------------------------------------------------

void ProjectSaver::save( std::ostream& o_stream, const Project& i_project )
{
	Json::Value root;

	const Scene& scene = *i_project.getScene();
	root["entities"] = saveEntities( i_project );
	root["ambient_light"] = saveAmbientLight( scene );
	root["lights"] = saveLight( scene );
	root["camera"] = saveCamera( scene );
	root["fog"] = saveFog( scene );

	o_stream << root;
}

Json::Value ProjectSaver::saveEntities( const Project& i_project )
{
	const ComponentManager& componentManager = i_project.getComponentManager();
	const Scene& scene = *i_project.getScene();

	Json::Value entities;
	i_project.forEachEntity( [&]( const Entity& entity )
	{
		entities.append( saveEntity( entity ) );
	} );

	return entities;
}

Json::Value ProjectSaver::saveEntity( const Entity& i_entity )
{
	Json::Value result;

	result["name"] = i_entity.getName().data();
	result["ID"] = i_entity.getId().value();

	ComponentSaverVisitor visitor( result );

	i_entity.forEachComponent( [&]( const Component* cmp )
	{
		cmp->accept( visitor );
	} );

	return result;
}

Json::Value ProjectSaver::saveAmbientLight( const Scene& i_scene )
{
	Json::Value result;

	const gc::AmbientLight& ambientLight = i_scene.getAmbientLight();
	result["color"] = vec4ToJson( ambientLight.color );
	result["weight"] = ambientLight.weight;

	return result;
}

Json::Value ProjectSaver::saveLight( const Scene& i_scene )
{
	Json::Value result;

	i_scene.forEachLight( [&]( const gc::Light& i_light )
	{
		TODO( "Remove this" );
		if( i_light.getLightType() == gc::LightType::point )
		{
			return;
		}
		Json::Value lightValue;
		const std::string_view typeStr = gc::toString( i_light.getLightType() );
		lightValue["type"] = Json::Value( typeStr.data(), typeStr.data() + typeStr.size() );
		lightValue["posDir"] = vec3ToJson( i_light.getPositionDir() );
		lightValue["color"] = vec4ToJson( i_light.getColor() );

		result.append( lightValue );
	} );

	return result;
}

Json::Value ProjectSaver::saveCamera( const Scene& i_scene )
{
	Json::Value result;

	const Camera& camera = i_scene.getCamera();

	result["near"] = camera.getNear();
	result["far"] = camera.getFar();
	result["fov"] = camera.getFov();
	result["speed"] = camera.getMoveSpeed();
	result["rot_speed"] = camera.getRotationSpeed();
	result["position"] = vec3ToJson( camera.getPosition() );
	result["rotation"] = quatToJson( camera.getRotation() );

	return result;
}

Json::Value ProjectSaver::saveFog( const Scene& i_scene )
{
	Json::Value result;

	const gc::Fog fog = i_scene.getFog();
	result["color"] = vec4ToJson( fog.color );
	result["start"] = fog.start;
	result["range"] = fog.range;

	return result;
}

ProjectSaver::ComponentSaverVisitor::ComponentSaverVisitor( Json::Value& i_jsonValue ) :
	entityValue( i_jsonValue )
{
}

void ProjectSaver::ComponentSaverVisitor::visit( const Component& i_cmp )
{
	ASSERT( false, "ComponentSaverVisitor::visit for this component is not implemented" );
}

void ProjectSaver::ComponentSaverVisitor::visit( const TransformComponent& i_transformCmp )
{
	Json::Value transformComponent;

	transformComponent["position"] = ProjectSaver::vec3ToJson( i_transformCmp.getPosition() );
	transformComponent["rotation"] = ProjectSaver::quatToJson( i_transformCmp.getRotation() );
	transformComponent["scale"] = ProjectSaver::vec3ToJson( i_transformCmp.getScale() );

	entityValue["transformComponent"] = transformComponent;
}

void ProjectSaver::ComponentSaverVisitor::visit( const MotionComponent& i_motionCmp )
{
	Json::Value motionComponent;

	motionComponent["speed"] = i_motionCmp.getSpeed();
	motionComponent["radius"] = i_motionCmp.getRadius();
	motionComponent["angle"] = i_motionCmp.getAngle();

	entityValue["motionComponent"] = motionComponent;
}

void ProjectSaver::ComponentSaverVisitor::visit( const LightComponent& i_lightCmp )
{
	Json::Value lightComponent;
	const auto* light = i_lightCmp.getLight();
	lightComponent["color"] = vec4ToJson( light->getColor() );

	entityValue["lightComponent"] = lightComponent;
}

void ProjectSaver::ComponentSaverVisitor::visit( const ModelComponent& i_modelComponent )
{
	Json::Value modelComponent;
	modelComponent["model"] = i_modelComponent.getModelName().data();

	entityValue["modelComponent"] = modelComponent;
}

void ProjectSaver::ComponentSaverVisitor::visit( const RenderComponent& i_renderComponent )
{
	Json::Value renderComponent;

	TODO( "Terrible workaround..." );
	auto modelCmp = i_renderComponent.getOwner()->getComponent<ModelComponent>();
	if( modelCmp )
	{
		if( modelCmp->getModelName().find( ".nfg" ) != std::string::npos )
		{
			modelCmp->getModel()->forEachMaterial( [&]( const gc::Material& material )
			{
				material.forEachMaterialData( [&]( const gc::MaterialData& i_data )
				{
					MaterialPropertySaver saver( i_data, renderComponent );
					std::visit( saver, i_data.m_value );
				} );
			} );
		}
	}

	renderComponent["shader"] = i_renderComponent.getShaderName().data();

	Json::Value parameters;
	const auto& params = i_renderComponent.getParams();
	if( params.color )
	{
		parameters["color"] = vec4ToJson( params.color.get() );
	}
	if( params.specular_power )
	{
		parameters["specular_power"] = params.specular_power.get();
	}
	if( params.dMax )
	{
		parameters["dMax"] = params.dMax.get();
	}
	if( params.depth_displ )
	{
		parameters["depth_displ"] = params.depth_displ.get();
	}
	if( params.depth_adjust )
	{
		parameters["depth_adjust"] = params.depth_adjust.get();
	}
	if( params.tilingFactor )
	{
		parameters["tiling_factor"] = params.tilingFactor.get();
	}

	renderComponent["parameters"] = parameters;

	entityValue["renderComponent"] = renderComponent;
}

//-----------------------------------------------------------------------------