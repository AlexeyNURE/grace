#include "project.hpp"

#include <grace/core/utils/math/intersect.hpp>

#include "application/src/entities/entity.hpp"

#include "application/src/project/project_loader.hpp"

//-----------------------------------------------------------------------------

Project::Project()
{
	m_componentManager.init();
	m_scene = std::make_unique<Scene>();

	m_scene->sigRayPicked.connect( boost::bind( &Project::onRayPicked, this, _1 ) );
}

void Project::update( float i_dt )
{
	m_componentManager.update( i_dt );
	m_scene->update( i_dt );
}

void Project::renderScene( const gc::OpenGLRendererAPI& i_rendererAPI )
{
	m_scene->render( i_rendererAPI );
}

void Project::addEntity( std::unique_ptr<Entity> i_entity )
{
	m_entities.emplace_back( std::move( i_entity ) );
}

void Project::removeSelectedEntity()
{
	auto it = m_entities.begin();
	while( it != m_entities.end() )
	{
		if( it->get() == m_selectedEntity )
		{
			m_componentManager.removeEntityComponents( m_selectedEntity );

			it = m_entities.erase( it );
			m_selectedEntity = nullptr;
			sigEntitySelected( nullptr );
		}
		else
			++it;
	}
}

void Project::onRayPicked( const gc::utils::ray3f& i_ray )
{
	if( m_selectedEntity && m_selectedEntity->hasComponent<ModelComponent>() )
	{
		m_selectedEntity->getComponent<ModelComponent>()->setRebderAabb( false );
	}

	m_selectedEntity = nullptr;

	ComponentArray<ModelComponent>& modelCmpArray = m_componentManager.getComponentArray<ModelComponent>();
	size_t size = modelCmpArray.getSize();
	float minDistance = std::numeric_limits<float>::max();
	ModelComponent* selectedModel = nullptr;
	for( int i = 0; i < size; i++ )
	{
		auto* modelCmp = static_cast<ModelComponent*>( modelCmpArray.at( i ) );
		auto aabb = modelCmp->getAabbWorld();
		if( !aabb )
		{
			continue;
		}

		float nearDistance, farDistance;
		bool intersects = gc::utils::intersectsWithRay( i_ray, nearDistance, farDistance, *aabb );
		if( intersects )
		{
			if( farDistance < minDistance )
			{
				minDistance = farDistance;
				selectedModel = modelCmp;
				continue;
			}
		}
	}

	if( selectedModel )
	{
		selectedModel->setRebderAabb( true );
		m_selectedEntity = selectedModel->getOwner();
	}

	sigEntitySelected( m_selectedEntity );
}

void Project::forEachEntity( std::function<void( const Entity& )> i_callback ) const
{
	for( const auto& e : m_entities )
	{
		i_callback( *e );
	}
}

void Project::forEachEntity( std::function<void( Entity& )> i_callback )
{
	for( const auto& e : m_entities )
	{
		i_callback( *e );
	}
}

Entity* Project::getEntityById( EntityId i_id )
{
	for( auto& e : m_entities )
	{
		if( e->getId() == i_id )
		{
			return e.get();
		}
	}

	return nullptr;
}

const Entity* Project::getEntityById( EntityId i_id ) const
{
	for( auto& e : m_entities )
	{
		if( e->getId() == i_id )
		{
			return e.get();
		}
	}

	return nullptr;
}

Entity* Project::getEntityByName( std::string_view i_name )
{
	for( auto& e : m_entities )
	{
		if( e->getName() == i_name )
		{
			return e.get();
		}
	}

	return nullptr;
}

const Entity* Project::getEntityByName( std::string_view i_name ) const
{
	for( auto& e : m_entities )
	{
		if( e->getName() == i_name )
		{
			return e.get();
		}
	}

	return nullptr;
}

void Project::setSelected( std::string_view i_name )
{
	if( m_selectedEntity && m_selectedEntity->hasComponent<ModelComponent>() )
	{
		m_selectedEntity->getComponent<ModelComponent>()->setRebderAabb( false );
	}

	for( const auto& e : m_entities )
	{
		if( e->getName() == i_name )
		{
			m_selectedEntity = e.get();
			sigEntitySelected( m_selectedEntity );

			if( m_selectedEntity->hasComponent<ModelComponent>() )
			{
				m_selectedEntity->getComponent<ModelComponent>()->setRebderAabb( true );
			}
		}
	}
}

//-----------------------------------------------------------------------------