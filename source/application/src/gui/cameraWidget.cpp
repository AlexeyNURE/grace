#include "application/src/gui/cameraWidget.hpp"

#include <QTreeWidget>
#include <QSpinBox>
#include <QCombobox>
#include <QLineEdit>

#include "ui_CameraWidget.h"

#include "application/src/resource/resource_manager.hpp"
#include "application/src/components/model_component.hpp"

//-----------------------------------------------------------------------------

CameraWidget::CameraWidget( QWidget* i_parent ) :
	QWidget( i_parent ),
	m_ui( std::make_unique<Ui::CameraWidget>() ),
	m_camera( nullptr )
{
	m_ui->setupUi( this );
}

void CameraWidget::init( Camera& i_camera )
{
	m_camera = &i_camera;

	m_ui->treeWidget->setColumnCount( 2 );

	// Add root node
	QTreeWidgetItem* treeRootGeneral = addTreeRoot( "General", "Expand to see more" );

	QWidget* farWidget = createSpinBoxWithSlider( m_camera->getFar(), 1, 500, &CameraWidget::sbFarChanged );
	addTreeWidget( treeRootGeneral, "Far", farWidget );

	QWidget* fovWidget = createSpinBoxWithSlider( m_camera->getFov(), 0, 3, &CameraWidget::sbFovChanged );
	addTreeWidget( treeRootGeneral, "FOV", fovWidget );

	QWidget* nearWidget = createSpinBoxWithSlider( m_camera->getNear(), 0.01, 30, &CameraWidget::sbNearChanged );
	addTreeWidget( treeRootGeneral, "Near", nearWidget );

	QWidget* moveSpeedWidget = createSpinBoxWithSlider( m_camera->getMoveSpeed(), 1, 50, &CameraWidget::sbMoveSpeedChanged );
	addTreeWidget( treeRootGeneral, "Move speed", moveSpeedWidget );

	QWidget* rotSpeedWidget = createSpinBoxWithSlider( m_camera->getRotationSpeed(), 1, 20, &CameraWidget::sbRotationSpeedChanged );
	addTreeWidget( treeRootGeneral, "Rotation speed", rotSpeedWidget );
}

void CameraWidget::sbFovChanged( double i_fov )
{
	m_camera->setFov( i_fov );
}

void CameraWidget::sbNearChanged( double i_near )
{
	m_camera->setNear( i_near );
}

void CameraWidget::sbMoveSpeedChanged( double i_moveSpeed )
{
	m_camera->setMoveSpeed( i_moveSpeed );
}

void CameraWidget::sbRotationSpeedChanged( double i_rotSpeed )
{
	m_camera->setRotationSpeed( i_rotSpeed );
}

void CameraWidget::sbFarChanged( double i_far )
{
	m_camera->setFar( i_far );
}

void CameraWidget::addTreeWidget( QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child )
{
	QTreeWidgetItem* treeItemChild = new QTreeWidgetItem();
	treeItemChild->setText( 0, i_name.data() );
	treeItemChild->setExpanded( true );

	o_parent->addChild( treeItemChild );
	m_ui->treeWidget->setItemWidget( treeItemChild, 1, i_child );
}

QTreeWidgetItem* CameraWidget::addTreeRoot( std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( m_ui->treeWidget );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

void CameraWidget::addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem();

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	o_parent->addChild( treeItem );
}

//-----------------------------------------------------------------------------