#pragma once

#include <grace/grace.hpp>

#include <boost/optional/optional.hpp>

#include <QDockWidget.h>

//------------------------------------------------------------------------

namespace Ui
{
class TransformWindow;
}
class TransformComponent;
class Entity;
class Project;

//------------------------------------------------------------------------

class TransformWindow : public QWidget
{
	Q_OBJECT
public:

	explicit TransformWindow( QWidget* parent = Q_NULLPTR );
	~TransformWindow() = default;

	void init( Project& o_project );

	void onEntitySelected( Entity* i_entity );

private slots:
	void sbPosValueXChanged( double i_x );
	void sbPosValueYChanged( double i_y );
	void sbPosValueZChanged( double i_z );

	void sbRotValueXChanged( double i_x );
	void sbRotValueYChanged( double i_y );
	void sbRotValueZChanged( double i_z );

	void sbScaleValueXChanged( double i_x );
	void sbScaleValueYChanged( double i_y );
	void sbScaleValueZChanged( double i_z );

private:
	void buildTransformWindow();

	std::unique_ptr<Ui::TransformWindow> m_ui;

	Entity* m_currentEntity;
	boost::optional<TransformComponent&> m_transformComponent;

};

//------------------------------------------------------------------------