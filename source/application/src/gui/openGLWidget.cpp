#include <QKeyEvent>
#include <grace/core/utils/math/intersect.hpp>
#include <grace/core/utils/globals.hpp>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/norm.hpp >

#include "application/src/entities/object_entity.hpp"

#include <glad/glad.h>

#include "application/src/gui/openGLWidget.hpp"
#include "application/src/gui/applicationController.hpp"

#include "application/src/resource/effect_manager.hpp"
#include "application/src/scene/scene.hpp"
#include "application/src/input/input_manager.hpp"

//------------------------------------------------------------------------

OpenGLWidget::OpenGLWidget( QWidget* parent ) :
	QOpenGLWidget( parent ),
	m_scene( nullptr ),
	screenResChanged( true )
{
}

OpenGLWidget::~OpenGLWidget() = default;

void OpenGLWidget::init( Scene& i_scene )
{
	m_scene = &i_scene;
}

void OpenGLWidget::initializeGL()
{
	if( gladLoadGL() != 1 )
		ASSERT( false, "Glad is not loaded" );

	LOG_INFO( "Initializing OpenGL version: {}", glGetString( GL_VERSION ) );

	m_rendererAPI.init();
	m_rendererAPI.clearColor();
	//m_rendererAPI.setPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	AppController::setGlWidget( this );
	
	resizeGL( width(), height() );
}

void OpenGLWidget::resizeGL( int i_width, int i_height )
{
	gc::Globals::s_screenWidth = i_width;
	gc::Globals::s_screenHeight = i_height;
	m_rendererAPI.setViewPort( 0, 0, i_width, i_height );

	if( m_scene )
	{
		m_scene->getCamera().calculateProjectionMatrix();
	}

	screenResChanged = true;

	LOG_INFO( "OpenGL Widget resolution changed: width: {}, height: {}", gc::Globals::s_screenWidth, gc::Globals::s_screenHeight );
}

void OpenGLWidget::paintGL()
{
	if( !m_scene )
		return;

	if( screenResChanged )
	{
		EffectManager::getInstance().regenerateFBO();
		screenResChanged = false;
	}

	EffectManager::getInstance().bindFbo();

	m_rendererAPI.clear();

	m_scene->render( m_rendererAPI );
	
	EffectManager::getInstance().render( m_rendererAPI );
}

void OpenGLWidget::mouseReleaseEvent( QMouseEvent* i_event )
{
	m_scene->processMouseReleased( glm::vec2( i_event->x(), i_event->y() ) );
}

//------------------------------------------------------------------------