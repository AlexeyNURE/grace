#include <QKeyEvent>
#include <QLayout>
#include <QDoubleSpinBox>
#include <QMainWindow.h>

#include <ui_TransformWindow.h>

#include "application/src/project/project.hpp"
#include "application/src/components/transform_component.hpp"
#include "application/src/gui/transformWindow.hpp"

//------------------------------------------------------------------------

TransformWindow::TransformWindow( QWidget* parent ) :
	QWidget( parent ),
	m_ui( std::make_unique<Ui::TransformWindow>() ),
	m_currentEntity( nullptr )
{
	m_ui->setupUi( this );

	connect( m_ui->posDoubleSpinBoxX, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbPosValueXChanged );
	connect( m_ui->posDoubleSpinBoxY, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbPosValueYChanged );
	connect( m_ui->posDoubleSpinBoxZ, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbPosValueZChanged );

	connect( m_ui->rotDoubleSpinBoxX, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbRotValueXChanged );
	connect( m_ui->rotDoubleSpinBoxY, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbRotValueYChanged );
	connect( m_ui->rotDoubleSpinBoxZ, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbRotValueZChanged );

	connect( m_ui->scaleDoubleSpinBoxX, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbScaleValueXChanged );
	connect( m_ui->scaleDoubleSpinBoxY, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbScaleValueYChanged );
	connect( m_ui->scaleDoubleSpinBoxZ, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, &TransformWindow::sbScaleValueZChanged );

	m_ui->scrollArea->setEnabled( false );
}

void TransformWindow::init( Project& o_project )
{
	m_ui->scrollArea->setWidgetResizable( true );
	m_ui->scrollArea->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded );
	m_ui->scrollArea->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );
	o_project.sigEntitySelected.connect( boost::bind( &TransformWindow::onEntitySelected, this, _1 ) );
}

void TransformWindow::onEntitySelected( Entity* i_entity )
{
	m_currentEntity = i_entity;
	if( !m_currentEntity )
	{
		m_transformComponent = boost::none;
		m_ui->scrollArea->setEnabled( false );

		return;
	}

	m_transformComponent = m_currentEntity->getComponent<TransformComponent>();

	buildTransformWindow();
}

void TransformWindow::sbPosValueXChanged( double i_x )
{
	glm::vec3 newPos( static_cast<float>( i_x ), m_transformComponent->getPosition().y, m_transformComponent->getPosition().z );
	m_transformComponent->setPosition( newPos );
}

void TransformWindow::sbPosValueYChanged( double i_y )
{
	glm::vec3 newPos( m_transformComponent->getPosition().x, static_cast<float>( i_y ), m_transformComponent->getPosition().z );
	m_transformComponent->setPosition( newPos );
}

void TransformWindow::sbPosValueZChanged( double i_z )
{
	glm::vec3 newPos( m_transformComponent->getPosition().x, m_transformComponent->getPosition().y, static_cast<float>( i_z ) );
	m_transformComponent->setPosition( newPos );
}

void TransformWindow::sbRotValueXChanged( double i_x )
{
	glm::quat newRot( 1.f, static_cast<float>( i_x ), m_transformComponent->getRotation().y, m_transformComponent->getRotation().z );
	m_transformComponent->setRotation( newRot );
}

void TransformWindow::sbRotValueYChanged( double i_y )
{
	glm::quat newRot( 1.f, m_transformComponent->getRotation().x, static_cast<float>( i_y ), m_transformComponent->getRotation().z );
	m_transformComponent->setRotation( newRot );
}

void TransformWindow::sbRotValueZChanged( double i_z )
{
	glm::quat newRot( 1.f, m_transformComponent->getRotation().x, m_transformComponent->getRotation().y, static_cast<float>( i_z ) );
	m_transformComponent->setRotation( newRot );
}

void TransformWindow::sbScaleValueXChanged( double i_x )
{
	glm::vec3 newScale( static_cast<float>( i_x ), m_transformComponent->getScale().y, m_transformComponent->getScale().z );
	m_transformComponent->setScale( newScale );
}

void TransformWindow::sbScaleValueYChanged( double i_y )
{
	glm::vec3 newScale( m_transformComponent->getScale().x, static_cast<float>( i_y ), m_transformComponent->getScale().z );
	m_transformComponent->setScale( newScale );
}

void TransformWindow::sbScaleValueZChanged( double i_z )
{
	glm::vec3 newScale( m_transformComponent->getScale().x, m_transformComponent->getScale().y, static_cast<float>( i_z ) );
	m_transformComponent->setScale( newScale );
}

void TransformWindow::buildTransformWindow()
{
	if( !m_transformComponent )
	{
		return;
	}

	m_ui->scrollArea->setEnabled( true );

	ASSERT( m_transformComponent );
	const auto& position = m_transformComponent->getPosition();
	const auto& rotation = m_transformComponent->getRotation();
	const auto& scale = m_transformComponent->getScale();

	m_ui->posDoubleSpinBoxX->setValue( position.x );
	m_ui->posDoubleSpinBoxY->setValue( position.y );
	m_ui->posDoubleSpinBoxZ->setValue( position.z );

	m_ui->rotDoubleSpinBoxX->setValue( rotation.x );
	m_ui->rotDoubleSpinBoxY->setValue( rotation.y );
	m_ui->rotDoubleSpinBoxZ->setValue( rotation.z );

	m_ui->scaleDoubleSpinBoxX->setValue( scale.x );
	m_ui->scaleDoubleSpinBoxY->setValue( scale.y );
	m_ui->scaleDoubleSpinBoxZ->setValue( scale.z );
}

//------------------------------------------------------------------------