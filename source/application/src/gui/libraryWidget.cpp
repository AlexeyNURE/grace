#include "libraryWidget.hpp"

#include "ui_libraryWidget.h"

#include "application/src/resource/resource_manager.hpp"

//-----------------------------------------------------------------------------

LibraryWidget::LibraryWidget( QWidget* parent ) :
	QWidget( parent ),
	m_ui( std::make_unique<Ui::LibraryWidget>() )
{
	m_ui->setupUi( this );
}

void LibraryWidget::init()
{
	m_ui->treeWidget->setColumnCount( 2 );

	// Set the headers
	m_ui->treeWidget->setHeaderLabels( QStringList() << "Resources" << "Names" );

	// Add root node
	addTreeRootModels( "Models", "List of models" );
	addTreeRootTextures( "Textures", "List of textures" );
	addTreeRootShaders( "Shaders", "List of shaders" );
}

LibraryWidget::~LibraryWidget() = default;

void LibraryWidget::addTreeRootModels( std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( m_ui->treeWidget );
	const auto& models = ResourceManager::getInstance().getModels();

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );
	for( const auto& [name, model] : models )
	{
		addTreeChild( treeItem, i_name, name );
	}
}

void LibraryWidget::addTreeRootTextures( std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( m_ui->treeWidget );
	const auto& textures = ResourceManager::getInstance().getTextures2D();

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );
	for( const auto& [name, texture] : textures )
	{
		addTreeChild( treeItem, i_name, name );
	}
}

void LibraryWidget::addTreeRootShaders( std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( m_ui->treeWidget );
	const auto& shaders = ResourceManager::getInstance().getShaders();

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );
	for( const auto& [name, model] : shaders )
	{
		addTreeChild( treeItem, i_name, name );
	}
}

void LibraryWidget::addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem();

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	o_parent->addChild( treeItem );
}

//-----------------------------------------------------------------------------