#include "application/src/gui/environment_widget.hpp"

#include <QPixmap>
#include <QToolButton>
#include <QTreeWidget>
#include <QColorDialog>
#include <QSpinBox>
#include <QCombobox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QFileDialog>
#include <qitemdelegate.h>

#include "ui_EnvironmentWidget.h"

#include "application/src/resource/resource_manager.hpp"
#include "application/src/resource/textureFactory.hpp"
#include "application/src/components/model_component.hpp"
#include <grace/render/model/mesh.hpp>

#include <glad/glad.h>

//-----------------------------------------------------------------------------

EnvironmentWidget::EnvironmentWidget( QWidget* i_parent ) :
	QWidget( i_parent ),
	m_scene( nullptr ),
	m_ui( std::make_unique<Ui::EnvironmentWidget>() ),
	m_ambientLight( nullptr ),
	m_fog( nullptr )
{
	m_ui->setupUi( this );
}

void EnvironmentWidget::init( Scene& i_scene )
{
	m_scene = &i_scene;
	m_ambientLight = &m_scene->getAmbientLight();
	m_fog = &m_scene->getFog();

	// Ambient Light
	QTreeWidgetItem* treeRootGeneral = addTreeRoot( m_ui->treeWidget, "Ambient Light", "Expand to see more" );

	QWidget* ambientWeightWidget = createSpinBoxWithSlider( m_ambientLight->weight, 0, 1, &EnvironmentWidget::sbAmbientWeightChanged );
	addTreeWidget( m_ui->treeWidget, treeRootGeneral, "Ambient light weight", ambientWeightWidget );

	QWidget* colorAmbWidget = createColorWidget( m_ambientLight->color );
	addTreeWidget( m_ui->treeWidget, treeRootGeneral, "Ambient color", colorAmbWidget );

	// Fog
	QTreeWidgetItem* treeRootFog = addTreeRoot( m_ui->treeWidget, "Fog", "Expand to see more" );

	QWidget* fogRangeWidget = createSpinBoxWithSlider( m_fog->range, 0, 100, &EnvironmentWidget::sbAmbientWeightChanged );
	addTreeWidget( m_ui->treeWidget, treeRootFog, "Fog range", fogRangeWidget );

	QWidget* fogStartWidget = createSpinBoxWithSlider( m_fog->start, 0, 100, &EnvironmentWidget::sbAmbientWeightChanged );
	addTreeWidget( m_ui->treeWidget, treeRootFog, "Fog start", fogStartWidget );

	QWidget* colorFogWidget = createColorWidget( m_fog->color );
	addTreeWidget( m_ui->treeWidget, treeRootFog, "Fog color", colorFogWidget );
}

void EnvironmentWidget::sbAmbientWeightChanged( double i_weight )
{
	m_ambientLight->weight = i_weight;
}

void EnvironmentWidget::addTreeWidget( QTreeWidget* o_treeWidget, QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child )
{
	QTreeWidgetItem* treeItemChild = new QTreeWidgetItem();
	treeItemChild->setText( 0, i_name.data() );

	o_parent->addChild( treeItemChild );
	o_treeWidget->setItemWidget( treeItemChild, 1, i_child );
}

QTreeWidgetItem* EnvironmentWidget::addTreeRoot( QTreeWidget* o_treeWidget, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_treeWidget );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

QTreeWidgetItem* EnvironmentWidget::addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_parent );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

QWidget* EnvironmentWidget::createColorWidget( glm::vec4& o_color )
{
	QColor currentColor(
		o_color.r * 255,
		o_color.g * 255,
		o_color.b * 255,
		o_color.a * 255 );

	QToolButton* toolButton = new QToolButton();

	toolButton->setStyleSheet( QString( "QToolButton{ background: %1; }" ).arg( currentColor.name() ) );

	connect( toolButton, static_cast<void( QToolButton::* )( )>( &QToolButton::pressed ), [toolButton, &o_color]()
	{
		QColorDialog* colorWidget = new QColorDialog();
		colorWidget->setModal( true );

		QColor currentColor(
			o_color.r * 255,
			o_color.g * 255,
			o_color.b * 255,
			o_color.a * 255 );

		colorWidget->setCurrentColor( currentColor );

		connect( colorWidget, static_cast<void( QColorDialog::* )( const QColor& )>( &QColorDialog::colorSelected ),
			[toolButton, &o_color]( const QColor& color )
		{
			toolButton->setStyleSheet( QString( "QToolButton{ background: %1; }" ).arg( color.name() ) );
			o_color = glm::vec4( color.redF(), color.greenF(), color.blueF(), color.alphaF() );
		} );

		colorWidget->show();
	} );

	return toolButton;
}

//-----------------------------------------------------------------------------