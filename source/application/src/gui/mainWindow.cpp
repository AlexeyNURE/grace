#include <fstream>
#include <QKeyEvent>
#include <QLayout>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <qpushbutton.h>
#include <QAction>
#include <qmessagebox.h>
#include <grace/grace.hpp>

#include "application/src/scene/scene.hpp"
#include "application/src/input/input_manager.hpp"

#include "application/src/gui/mainWindow.hpp"

#include "application/src/project/project_loader.hpp"
#include "application/src/project/project_saver.hpp"

#include "application/src/gui/transformWindow.hpp"
#include "application/src/gui/libraryWidget.hpp"
#include "application/src/gui/propertyWindow.hpp"
#include "application/src/gui/openGLWidget.hpp"
#include "application/src/gui/cameraWidget.hpp"
#include "application/src/gui/meshWindow.hpp"
#include "application/src/gui/entities_widget.hpp"
#include "application/src/gui/environment_widget.hpp"
#include "application/src/gui/material_widget.hpp"

#include "application/src/scene/scene.hpp"
#include "application/src/resource/effect_manager.hpp"
#include "application/src/resource/resource_manager.hpp"

#include <ui_MainWidget.h>

//------------------------------------------------------------------------

MainWindow::MainWindow( QMainWindow* parent ) :
	QMainWindow( parent ),
	m_updateTimer( this ),
	m_ui( std::make_unique<Ui::MainWindow>() ),
	m_entitiesWidget( nullptr )
{
	m_ui->setupUi( this );

	showMaximized();
}

MainWindow::~MainWindow()
{
}

void MainWindow::init()
{
	ResourceManager::getInstance().init();

	std::ifstream file( ms_SMFileName.data() );
	m_project = ProjectLoader::load( file );

	EffectManager::getInstance().load( m_project->getScene()->getCamera() );

	connect( &m_updateTimer, SIGNAL( timeout() ), this, SLOT( update() ) );

	connect( m_ui->actionSave, static_cast<void( QAction::* )( bool )>( &QAction::triggered ), [this]( bool )
	{
		std::ofstream file( ms_SMFileName.data() );
		ProjectSaver::save( file, *m_project );
	} );

	m_updateTimer.start( 1 );

	initGUI();
}

void MainWindow::initGUI()
{
	m_ui->openGLWidget->init( *m_project->getScene() );

	{
		TransformWindow* w = new TransformWindow( m_ui->transformWindow );
		w->init( *m_project );
		m_ui->transformWindow->setWidget( w );
	}

	{
		LibraryWidget* w = new LibraryWidget( m_ui->librariesWindow );
		w->init();
		m_ui->librariesWindow->setWidget( w );
	}

	{
		m_propertyWindow = new PropertyWindow( m_ui->propertyWindow );
		m_propertyWindow->init( *m_project.get() );
		m_ui->propertyWindow->setWidget( m_propertyWindow );
	}

	{
		CameraWidget* w = new CameraWidget( m_ui->cameraWidget );
		w->init( m_project->getScene()->getCamera() );
		m_ui->cameraWidget->setWidget( w );
	}

	{
		MaterialWidget* materialWidget = new MaterialWidget( m_ui->materialWidget );
		materialWidget->init();
		m_ui->materialWidget->setWidget( materialWidget );

		MeshWindow* w = new MeshWindow( m_ui->meshWindow );
		w->init( *m_project, *materialWidget );
		m_ui->meshWindow->setWidget( w );
	}

	{
		m_entitiesWidget = new EntitiesWidget( m_ui->entitiesWidget );
		m_entitiesWidget->init( *m_project );
		m_ui->entitiesWidget->setWidget( m_entitiesWidget );
	}

	{
		EnvironmentWidget* w = new EnvironmentWidget( m_ui->environmentWidget );
		w->init( *m_project->getScene() );
		m_ui->environmentWidget->setWidget( w );
	}
}

void MainWindow::update()
{
	using namespace std::chrono;

	static auto s_lastTime = high_resolution_clock::now();
	const auto currentTime = high_resolution_clock::now();
	auto deltaTime = duration_cast<duration<float>>( currentTime - s_lastTime );

	m_project->update( deltaTime.count() );
	EffectManager::getInstance().update( deltaTime.count() );

	m_ui->openGLWidget->repaint();

	gc::Globals::s_totalTime += deltaTime.count();
	s_lastTime = currentTime;
}

void MainWindow::keyPressEvent( QKeyEvent* _event )
{
	if( _event->isAutoRepeat() )
	{
		return;
	}

	LOG_INFO( "Key pressed: {}", static_cast<char>( _event->nativeVirtualKey() ) );
	InputManager::getInstance().processKey( _event->nativeVirtualKey(), true );
}

void MainWindow::keyReleaseEvent( QKeyEvent* _event )
{
	if( _event->isAutoRepeat() )
	{
		return;
	}

	InputManager::getInstance().processKey( _event->nativeVirtualKey(), false );
}

//------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////