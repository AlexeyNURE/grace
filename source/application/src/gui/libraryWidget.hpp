#pragma once

#include <memory>
#include <QWidget>

//-----------------------------------------------------------------------------

namespace Ui
{
class LibraryWidget;
}
class QTreeWidgetItem;

//-----------------------------------------------------------------------------

class LibraryWidget : public QWidget
{
	Q_OBJECT

public:
	explicit LibraryWidget( QWidget* parent = 0 );
	~LibraryWidget();

	void init();

private:
	void addTreeRootModels( std::string_view i_name, std::string_view i_description );
	void addTreeRootTextures( std::string_view i_name, std::string_view i_description );
	void addTreeRootShaders( std::string_view i_name, std::string_view i_description );
	void addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description );

	std::unique_ptr<Ui::LibraryWidget> m_ui;
};

//-----------------------------------------------------------------------------