#pragma once

#include <string>
#include <QOpenGLWidget>
#include <QTimer>

#include <grace/core/utils/math/ray3.hpp>
#include <grace/render/renderer_API.hpp>

//------------------------------------------------------------------------

class Scene;

//------------------------------------------------------------------------

class OpenGLWidget : public QOpenGLWidget
{
	Q_OBJECT

public:

	OpenGLWidget( QWidget* parent = Q_NULLPTR );
	~OpenGLWidget();

	void init( Scene& i_scene );

	void initializeGL() override;
	void resizeGL( int i_width, int i_height ) override;
	void paintGL() override;

	void mouseReleaseEvent( QMouseEvent* i_event ) override;

private:
	Scene* m_scene;

	bool screenResChanged;

	gc::OpenGLRendererAPI m_rendererAPI;
};

//------------------------------------------------------------------------