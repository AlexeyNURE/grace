#pragma once

#include <memory>
#include <QWidget.h>

//-----------------------------------------------------------------------------

namespace Ui
{
class CameraWidget;
}

class QTreeWidgetItem;
class Camera;

//-----------------------------------------------------------------------------

class CameraWidget : public QWidget
{
	Q_OBJECT

public:
	explicit CameraWidget( QWidget* parent = Q_NULLPTR );
	~CameraWidget() = default;

	void init( Camera& i_camera );

private slots:
	void sbFarChanged( double i_far );
	void sbFovChanged( double i_fov );
	void sbNearChanged( double i_near );
	void sbMoveSpeedChanged( double i_far );
	void sbRotationSpeedChanged( double i_far );

private:
	QTreeWidgetItem* addTreeRoot( std::string_view i_name, std::string_view i_description );
	void addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description );
	void addTreeWidget( QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child );
	
	template<typename Func>
	QWidget* createSpinBoxWithSlider( double i_value, int i_minValue, int i_maxValue, Func i_slot );

private:
	std::unique_ptr<Ui::CameraWidget> m_ui;

	Camera* m_camera;
};

template<typename Func>
inline QWidget* CameraWidget::createSpinBoxWithSlider( double i_value, int i_minValue, int i_maxValue, Func i_slot )
{
	QWidget* w = new QWidget();
	QHBoxLayout* layout = new QHBoxLayout();
	QDoubleSpinBox* spinBox = new QDoubleSpinBox();
	QSlider* slider = new QSlider( Qt::Orientation::Horizontal );

	spinBox->setRange( i_minValue, i_maxValue );
	slider->setRange( 0, 100 ); // percent

	connect( spinBox, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [=]( double value )
	{
		int realValue = ( value / i_maxValue ) * 100;
		slider->setValue( realValue );
	} );

	connect( slider, static_cast<void( QSlider::* )( int )>( &QSlider::valueChanged ), [=]( double value )
	{
		double realValue = ( value / 100 ) * i_maxValue;
		spinBox->setValue( realValue );
	} );

	spinBox->setValue( i_value );
	slider->setValue( ( i_value / i_maxValue ) * 100 );

	connect( spinBox, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), this, i_slot );

	layout->addWidget( spinBox );
	layout->addWidget( slider );
	w->setLayout( layout );

	return w;
}

//-----------------------------------------------------------------------------