#include "application/src/gui/meshWindow.hpp"

#include <QPixmap>
#include <QToolButton>
#include <QTreeWidget>
#include <QColorDialog>
#include <QSpinBox>
#include <QCombobox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <qitemdelegate.h>

#include <ui_MeshWindow.h>

#include "application/src/gui/material_widget.hpp"

#include "application/src/resource/resource_manager.hpp"
#include "application/src/resource/textureFactory.hpp"
#include "application/src/components/model_component.hpp"
#include <grace/render/model/mesh.hpp>

#include <glad/glad.h>

//-----------------------------------------------------------------------------

MeshWindow::MeshWindow( QWidget* i_parent ) :
	QWidget( i_parent ),
	m_ui( std::make_unique<Ui::MeshWindow>() )
{
	m_ui->setupUi( this );
}

void MeshWindow::init( Project& i_project, MaterialWidget& i_materialWidget )
{
	i_project.sigEntitySelected.connect( boost::bind( &MeshWindow::onEntitySelected, this, _1 ) );
	m_materialWidget = &i_materialWidget;
}

void MeshWindow::buildUi()
{
	m_ui->treeWidget->clear();

	// Add root node
	auto modelCmp = m_entity->getComponent<ModelComponent>();
	gc::Model* model = modelCmp->getModel();
	if( !model )
	{
		return;
	}

	connect( m_ui->treeWidget, SIGNAL( itemDoubleClicked( QTreeWidgetItem*, int ) ), this, SLOT( onItemDoubleClicked( QTreeWidgetItem*, int ) ) );

	int idx = 0;
	model->forEachMesh( [this, &idx]( gc::Mesh& mesh )
	{
		std::string meshName = mesh.getName().data();
		if( meshName.empty() )
		{
			meshName = fmt::format( "Mesh_{}", idx );
			idx++;
		}
		QTreeWidgetItem* treeRoot = MeshWindow::addTreeRoot( m_ui->treeWidget, meshName, "" );
	} );
}

void MeshWindow::onEntitySelected( Entity* i_entity )
{
	if( m_entity == i_entity )
	{
		return;
	}
	else
	{
		m_entity = i_entity;
		m_ui->treeWidget->clear();
	}

	if( !m_entity )
		return;

	auto modelCmp = m_entity->getComponent<ModelComponent>();
	if( !modelCmp )
	{
		m_ui->treeWidget->clear();

		return;
	}

	modelCmp->sigModelChanged.connect( boost::bind( &MeshWindow::buildUi, this ) );

	buildUi();
}

void MeshWindow::addTreeWidget( QTreeWidget* o_treeWidget, QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child )
{
	QTreeWidgetItem* treeItemChild = new QTreeWidgetItem();
	treeItemChild->setText( 0, i_name.data() );

	o_parent->addChild( treeItemChild );
	o_treeWidget->setItemWidget( treeItemChild, 1, i_child );
}

QTreeWidgetItem* MeshWindow::addTreeRoot( QTreeWidget* o_treeWidget, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_treeWidget );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

QTreeWidgetItem* MeshWindow::addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_parent );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

void MeshWindow::onItemDoubleClicked( QTreeWidgetItem* item, int column )
{
	auto modelCmp = m_entity->getComponent<ModelComponent>();
	gc::Model* model = modelCmp->getModel();
	if( !model )
	{
		return;
	}

	gc::Mesh* mesh = model->getMeshByName( item->text( column ).toStdString() );
	if( !mesh )
	{
		mesh = model->getMeshByIdx( 0 );
	}

	m_materialWidget->buildUi( mesh );
}

//-----------------------------------------------------------------------------