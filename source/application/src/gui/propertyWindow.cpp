#include "application/src/gui/propertyWindow.hpp"

#include <QPixmap>
#include <QToolButton>
#include <QTreeWidget>
#include <QColorDialog>
#include <QSpinBox>
#include <QCombobox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <qitemdelegate.h>

#include "ui_PropertyWindow.h"

#include "application/src/resource/resource_manager.hpp"
#include "application/src/components/model_component.hpp"

//-----------------------------------------------------------------------------

PropertyWindow::PropertyWindow( QWidget* i_parent ) :
	QWidget( i_parent ),
	m_ui( std::make_unique<Ui::PropertyWindow>() ),
	m_project( nullptr )
{
	m_ui->setupUi( this );
}

void PropertyWindow::init( Project& i_project )
{
	m_project = &i_project;

	m_ui->tabWidget->clear();

	connect( m_ui->tabWidget, static_cast<void( QTabWidget::* )( int )>( &QTabWidget::tabCloseRequested ), [this]( int index )
	{
		m_ui->tabWidget->removeTab( index );
		for( auto& [name, idx] : m_tabs )
		{
			TODO( "Refactor" );
			//if( idx == index )
			//{
				m_tabs.erase( name );
				return;
			//}
		}
	} );

	m_project->sigEntitySelected.connect( boost::bind( &PropertyWindow::onEntitySelected, this, _1 ) );
}

void PropertyWindow::onEntitySelected( Entity* i_entity )
{
	m_entity = i_entity;

	if( !m_entity )
	{
		m_ui->tabWidget->clear();
		m_tabs.clear();
		return;
	}

	std::string_view entityName = m_entity->getName().data();

	const auto& it = m_tabs.find( entityName.data() );
	if( it != m_tabs.end() )
	{
		m_ui->tabWidget->setCurrentIndex( it->second );
		return;
	}

	QTreeWidget* treeWidget = new QTreeWidget();

	treeWidget->clear();

	treeWidget->setColumnCount( 2 );
	treeWidget->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded );
	treeWidget->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );

	// Set the headers
	treeWidget->setHeaderLabels( QStringList() << "Property" << "Value" );

	// Add root node
	QTreeWidgetItem* treeRootGeneral = addTreeRoot( treeWidget, "General", "" );

	QLineEdit* lineEditName = new QLineEdit();
	lineEditName->setText( m_entity->getName().data() );
	addTreeWidget( treeWidget, treeRootGeneral, "Name", lineEditName );

	QSpinBox* spinBoxId = new QSpinBox();
	spinBoxId->setValue( m_entity->getId().value() );
	spinBoxId->setReadOnly( true );
	spinBoxId->setEnabled( false );
	addTreeWidget( treeWidget, treeRootGeneral, "Id", spinBoxId );

	UIComponentVisitor visitor( treeWidget, this );

	m_entity->forEachComponent( [&visitor]( Component* i_cmp )
	{
		i_cmp->accept( visitor );
	} );

	int index = m_ui->tabWidget->addTab( treeWidget, entityName.data() );
	m_tabs[entityName.data()] = index;
	m_ui->tabWidget->setCurrentIndex( index );
}

void PropertyWindow::addTreeWidget( QTreeWidget* o_treeWidget, QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child )
{
	QTreeWidgetItem* treeItemChild = new QTreeWidgetItem( o_parent );
	treeItemChild->setText( 0, i_name.data() );

	o_treeWidget->setItemWidget( treeItemChild, 1, i_child );
}

QTreeWidgetItem* PropertyWindow::addTreeRoot( QTreeWidget* o_treeWidget, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_treeWidget );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

QTreeWidgetItem* PropertyWindow::addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_parent );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

//-----------------------------------------------------------------------------

UIComponentVisitor::UIComponentVisitor( QTreeWidget* o_treeWidget, QWidget* o_propertyWidget ) :
	treeWidget( o_treeWidget ),
	propertyWidget( o_propertyWidget )
{
}

void UIComponentVisitor::visit( MotionComponent& i_motionCmp )
{
	QTreeWidgetItem* treeRoot = PropertyWindow::addTreeRoot( treeWidget, "Motion Component", "" );

	QWidget* speedWidget = PropertyWindow::createSpinBoxWithSlider( i_motionCmp.getSpeed(), 0, 20,
	[&i_motionCmp]( double value )
	{
		i_motionCmp.setSpeed( value );
	} );

	QWidget* radiusWidget = PropertyWindow::createSpinBoxWithSlider( i_motionCmp.getRadius(), 0, 10,
	[&i_motionCmp]( double value )
	{
		i_motionCmp.setRadius( value );
	} );

	PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Speed", speedWidget );
	PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Radius", radiusWidget );
}

void UIComponentVisitor::visit( LightComponent& i_lightCmp )
{
	QTreeWidgetItem* treeRoot = PropertyWindow::addTreeRoot( treeWidget, "Light Component", "" );

	const glm::vec4& color = i_lightCmp.getLight()->getColor();
	QColor currentColor(
		color.r * 255,
		color.g * 255,
		color.b * 255,
		color.a * 255 );
	
	QToolButton* toolButton = new QToolButton();

	toolButton->setStyleSheet( QString( "QToolButton{ background: %1; }" ).arg( currentColor.name() ) );

	treeWidget->connect( toolButton, static_cast<void( QToolButton::* )()>( &QToolButton::pressed ), [toolButton, &i_lightCmp, this]()
	{
		QColorDialog* colorWidget = new QColorDialog();
		colorWidget->setModal( true );

		const glm::vec4& color = i_lightCmp.getLight()->getColor();
		QColor currentColor(
			color.r * 255,
			color.g * 255,
			color.b * 255,
			color.a * 255 );

		colorWidget->setCurrentColor( currentColor );

		treeWidget->connect( colorWidget, static_cast<void( QColorDialog::* )( const QColor& )>( &QColorDialog::colorSelected ),
		[toolButton, &i_lightCmp]( const QColor& color )
		{
			toolButton->setStyleSheet( QString( "QToolButton{ background: %1; }" ).arg( color.name() ) );
			i_lightCmp.setColor( glm::vec4( color.redF(), color.greenF(), color.blueF(), color.alphaF() ) );
		} );

		colorWidget->show();
	} );

	PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Color", toolButton );
}

void UIComponentVisitor::visit( ModelComponent& i_modelCmp )
{
	QTreeWidgetItem* treeRootModel = PropertyWindow::addTreeRoot( treeWidget, "Model Component", "" );

	QWidget* widget = new QWidget;
	QHBoxLayout* layout = new QHBoxLayout;

	QLineEdit* lineEdit = new QLineEdit();
	lineEdit->setText( i_modelCmp.getModelName().data() );

	QToolButton* toolButton = new QToolButton();
	toolButton->setText( "..." );

	widget->connect( toolButton, &QToolButton::clicked, [lineEdit, toolButton, widget, &i_modelCmp, this]( bool )
	{
		toolButton->setFocus();
		const QString filter = "Model (*.obj *.nfg *.dae *.blend)";
		const QString fileName = QFileDialog::getOpenFileName( widget, "Open a model file", QDir::currentPath(), filter );
		if( !fileName.isEmpty() )
		{
			i_modelCmp.setModel( fileName.toStdString() );
			lineEdit->setText( fileName );
		}
	} );

	layout->addWidget( lineEdit );
	layout->addWidget( toolButton );
	widget->setLayout( layout );

	PropertyWindow::addTreeWidget( treeWidget, treeRootModel, "Model", widget );
}

void UIComponentVisitor::visit( RenderComponent& i_renderCmp )
{
	QTreeWidgetItem* treeRoot = PropertyWindow::addTreeRoot( treeWidget, "Render Component", "" );


	const auto& textures2D = ResourceManager::getInstance().getTextures2D();
	QComboBox* comboBox2DTextures = new QComboBox();
	for( const auto& [name, model] : textures2D )
	{
		comboBox2DTextures->addItem( name.data() );
	}

	const auto& shaders = ResourceManager::getInstance().getShaders();
	QComboBox* comboBoxshaders = new QComboBox();
	for( const auto& [name, shader] : shaders )
	{
		comboBoxshaders->addItem( name.data() );
	}

	const gc::Shader* shader = i_renderCmp.getShader();
	if( shader )
	{
		comboBoxshaders->setCurrentText( gc::toString( shader->getShaderKind() ).data() );
	}

	treeWidget->connect( comboBoxshaders, static_cast<void( QComboBox::* )( const QString & )>( &QComboBox::currentTextChanged ), [&i_renderCmp]( const QString& text )
	{
		i_renderCmp.setShader( ResourceManager::getInstance().getShaderByName( text.toStdString() ) );
	} );

	QWidget* specularWidget = nullptr;
	QWidget* depthAdjustWidget = nullptr;
	QWidget* depthDisplWidget = nullptr;
	QWidget* tilingFactorWidget = nullptr;
	QWidget* dMaxWidget = nullptr;
	auto& params = i_renderCmp.getParams();
	if( params.specular_power )
	{
		specularWidget = PropertyWindow::createSpinBoxWithSlider( params.specular_power.get(), 0, 100,
		[&i_renderCmp, &params]( double value )
		{
			params.specular_power = value;
		} );
	}

	if( params.depth_adjust )
	{
		depthAdjustWidget = PropertyWindow::createSpinBoxWithSlider( params.depth_adjust.get(), 0, 1,
			[&i_renderCmp, &params]( double value )
		{
			params.depth_adjust = value;
		} );
	}

	if( params.depth_displ )
	{
		depthDisplWidget = PropertyWindow::createSpinBoxWithSlider( params.depth_displ.get(), 0, 1,
			[&i_renderCmp, &params]( double value )
		{
			params.depth_displ = value;
		} );
	}

	if( params.tilingFactor )
	{
		tilingFactorWidget = PropertyWindow::createSpinBoxWithSlider( params.tilingFactor.get(), 0, 60,
			[&i_renderCmp, &params]( double value )
		{
			params.tilingFactor = value;
		} );
	}

	if( params.dMax )
	{
		dMaxWidget = PropertyWindow::createSpinBoxWithSlider( params.dMax.get(), 0, 1,
			[&i_renderCmp, &params]( double value )
		{
			params.dMax = value;
		} );
	}

	//comboBox2DTextures->setCurrentText( i_renderCmp.getModelName().data() );

	//treeWidget->connect( comboBoxModel, static_cast<void( QComboBox::* )( const QString & text )>( &QComboBox::currentTextChanged ), [&i_renderCmp]( const QString& text )
	//{
	//	std::string modelName = text.toStdString();
	//	gc::Model* model = ResourceManager::getInstance().getModelByName( modelName );
	//	i_renderCmp.setTexture2D( model );
	//	i_renderCmp.setModelName( modelName );
	//} );

	PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Shader", comboBoxshaders );
	if( specularWidget )
	{
		PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Specular power", specularWidget );
	}
	if( depthAdjustWidget )
	{
		PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Depth Adjustment", depthAdjustWidget );
	}
	if( depthDisplWidget )
	{
		PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Depth displacement", depthDisplWidget );
	}
	if( tilingFactorWidget )
	{
		PropertyWindow::addTreeWidget( treeWidget, treeRoot, "Tiling Factor", tilingFactorWidget );
	}
	if( dMaxWidget )
	{
		PropertyWindow::addTreeWidget( treeWidget, treeRoot, "dMax", dMaxWidget );
	}
}

//-----------------------------------------------------------------------------