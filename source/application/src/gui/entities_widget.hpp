#pragma once

#include <unordered_map>

#include <QMainWindow.h>
#include "application/src/project/project.hpp"
#include "application/src/components/component_visitor.hpp"

//-----------------------------------------------------------------------------

namespace Ui
{
class EntitiesWidget;
}

class QTreeWidget;
class QTreeWidgetItem;
class Entity;

//-----------------------------------------------------------------------------

class EntitiesWidget : public QWidget
{
	Q_OBJECT

public:
	explicit EntitiesWidget( QWidget* parent = Q_NULLPTR );
	~EntitiesWidget() = default;

	void init( Project& i_scene );
	void buildUi();

public:
	static QTreeWidgetItem* addTreeRoot( QTreeWidget* o_treeWidget, std::string_view i_name, std::string_view i_description );
	static void addTreeWidget( QTreeWidget* o_treeWidget, QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child );
	static QTreeWidgetItem* addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description );

	template<typename Func>
	static QWidget* createSpinBoxWithSlider( double i_value, int i_minValue, int i_maxValue, Func i_slot );

private slots:
	void onMouseDoubleClicked( QTreeWidgetItem*, int );
	void onRemoveEntityBtnClicked();
	void onAddEntityBtnClicked();
	void prepareMenu( const QPoint& );

	void addModelCmp();
	void addTransformCmp();
	void addRenderCmp();
	void addMotionCmp();
	void addLightCmp();

private:
	template<typename T>
	void removeCmp();

private:
	std::unique_ptr<Ui::EntitiesWidget> m_ui;

	Project* m_project;
	Entity* m_contextEntity;
};

//-----------------------------------------------------------------------------

template<typename Func>
inline QWidget* EntitiesWidget::createSpinBoxWithSlider( double i_value, int i_minValue, int i_maxValue, Func i_slot )
{
	QWidget* w = new QWidget();
	QHBoxLayout* layout = new QHBoxLayout();
	QDoubleSpinBox* spinBox = new QDoubleSpinBox();
	QSlider* slider = new QSlider( Qt::Orientation::Horizontal );

	spinBox->setRange( i_minValue, i_maxValue );
	slider->setRange( 0, 100 ); // percent

	connect( spinBox, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [=]( double value )
	{
		int realValue = ( value / i_maxValue ) * 100;
		slider->setValue( realValue );
	} );

	connect( slider, static_cast<void( QSlider::* )( int )>( &QSlider::valueChanged ), [=]( double value )
	{
		double realValue = ( value / 100 ) * i_maxValue;
		spinBox->setValue( realValue );
	} );

	spinBox->setValue( i_value );
	slider->setValue( ( i_value / i_maxValue ) * 100 );

	connect( spinBox, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), i_slot );

	layout->addWidget( spinBox );
	layout->addWidget( slider );
	w->setLayout( layout );

	return w;
}

//-----------------------------------------------------------------------------