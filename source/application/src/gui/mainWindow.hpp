#pragma once

#include <grace/grace.hpp>

#include <qmainwindow.h>
#include <QTimer>

//-----------------------------------------------------------------------------

namespace Ui {
class MainWindow;
}
class Project;
class EntitiesWidget;
class PropertyWindow;

//-----------------------------------------------------------------------------

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	
	explicit MainWindow( QMainWindow* parent = Q_NULLPTR );
	~MainWindow();

	void init();

	void keyPressEvent( QKeyEvent* _event ) override;
	void keyReleaseEvent( QKeyEvent* _event ) override;

public:
	static constexpr std::string_view ms_SMFileName{ "..\\..\\data\\SM.json" };

private:
	void initGUI();

private slots:
	void update();

private:
	std::unique_ptr<Ui::MainWindow> m_ui;
	EntitiesWidget* m_entitiesWidget;
	PropertyWindow* m_propertyWindow;

	std::unique_ptr<Project> m_project;

	QTimer m_updateTimer;
};

//-----------------------------------------------------------------------------