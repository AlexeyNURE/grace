#pragma once

#include <unordered_map>

#include <QMainWindow.h>
#include "application/src/project/project.hpp"
#include "application/src/components/component_visitor.hpp"

//-----------------------------------------------------------------------------

namespace Ui
{
class PropertyWindow;
}

class QTreeWidget;
class QTreeWidgetItem;
class Entity;

//-----------------------------------------------------------------------------

struct UIComponentVisitor : public ComponentDefaultVisitor
{
	UIComponentVisitor( QTreeWidget* o_treeWidget, QWidget* o_propertyWidget );

	void visit( MotionComponent& ) override;
	void visit( LightComponent& ) override;
	void visit( ModelComponent& ) override;
	void visit( RenderComponent& ) override;

	QTreeWidget* treeWidget;
	QWidget* propertyWidget;
};

//-----------------------------------------------------------------------------

class PropertyWindow : public QWidget
{
	Q_OBJECT

	using TabNameToIdx = std::unordered_map<std::string, int>;
public:
	explicit PropertyWindow( QWidget* parent = Q_NULLPTR );
	~PropertyWindow() = default;

	void init( Project& i_scene );

	void onEntitySelected( Entity* i_entity );

public:
	static QTreeWidgetItem* addTreeRoot( QTreeWidget* o_treeWidget, std::string_view i_name, std::string_view i_description );
	static void addTreeWidget( QTreeWidget* o_treeWidget, QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child );
	static QTreeWidgetItem* addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description );

	template<typename Func>
	static QWidget* createSpinBoxWithSlider( double i_value, int i_minValue, int i_maxValue, Func i_slot );

private:
	Project* m_project;
	std::unique_ptr<Ui::PropertyWindow> m_ui;

	TabNameToIdx m_tabs;

	Entity* m_entity;
};

//-----------------------------------------------------------------------------

template<typename Func>
inline QWidget* PropertyWindow::createSpinBoxWithSlider( double i_value, int i_minValue, int i_maxValue, Func i_slot )
{
	QWidget* w = new QWidget();
	QHBoxLayout* layout = new QHBoxLayout();
	QDoubleSpinBox* spinBox = new QDoubleSpinBox();
	QSlider* slider = new QSlider( Qt::Orientation::Horizontal );

	spinBox->setRange( i_minValue, i_maxValue );
	slider->setRange( 0, 100 ); // percent

	connect( spinBox, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [=]( double value )
	{
		int realValue = ( value / i_maxValue ) * 100;
		slider->setValue( realValue );
	} );

	connect( slider, static_cast<void( QSlider::* )( int )>( &QSlider::valueChanged ), [=]( double value )
	{
		double realValue = ( value / 100 ) * i_maxValue;
		spinBox->setValue( realValue );
	} );

	spinBox->setValue( i_value );
	slider->setValue( ( i_value / i_maxValue ) * 100 );

	connect( spinBox, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), i_slot );

	layout->addWidget( spinBox );
	layout->addWidget( slider );
	w->setLayout( layout );

	return w;
}

//-----------------------------------------------------------------------------