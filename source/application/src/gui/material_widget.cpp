#include "application/src/gui/material_widget.hpp"

#include <QPixmap>
#include <QToolButton>
#include <QTreeWidget>
#include <QTableWidget>
#include <QColorDialog>
#include <QSpinBox>
#include <QCombobox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <qmessagebox.h>
#include <QMenu>

#include <ui_material_widget.h>

#include "application/src/resource/resource_manager.hpp"
#include "application/src/components/model_component.hpp"
#include <grace/render/model/mesh.hpp>
#include <grace/render/material/material.hpp>

#include <glad/glad.h>

//-----------------------------------------------------------------------------

struct MaterialDataUIVisitor
{
	MaterialDataUIVisitor( gc::MaterialData& i_data, QTreeWidget* i_treeWidget, QTreeWidgetItem* i_rootWidget ) :
		data( i_data ),
		treeWidget( i_treeWidget ),
		rootWidget( i_rootWidget )
	{
	}

	void operator() ( const gc::Texture2D* i_texture )
	{
		QWidget* w = new QWidget();
		QHBoxLayout* layout = new QHBoxLayout;
		QLineEdit* lineEdit = new QLineEdit();

		QToolButton* toolButton = new QToolButton();
		toolButton->setText( "..." );

		treeWidget->connect( toolButton, static_cast<void( QToolButton::* )( bool )>( &QToolButton::clicked ), [lineEdit, *this]( bool )
		{
			lineEdit->setFocus();
			const QString filter = "Texture (*.png *.jpg *.dae)";
			const QString fileName = QFileDialog::getOpenFileName( lineEdit, "Open a texture file", QDir::currentPath(), filter, nullptr/*, QFileDialog::DontUseNativeDialog*/ );
			if( !fileName.isEmpty() )
			{
				data.m_value = ResourceManager::getInstance().loadTexture2D( fileName.toStdString(), GL_CLAMP_TO_EDGE );
				lineEdit->setText( fileName );
			}
		} );

		layout->addWidget( lineEdit );
		layout->addWidget( toolButton );
		w->setLayout( layout );

		MaterialWidget::addTreeWidget( treeWidget, rootWidget, data.m_name, w );
	}

	void operator() ( const gc::CubeTexture* i_texture )
	{
		MaterialWidget::addTreeWidget( treeWidget, rootWidget, data.m_name, nullptr );
	}

	void operator() ( glm::vec2& i_value )
	{
		QWidget* w = new QWidget();
		QHBoxLayout* layout = new QHBoxLayout();
		QDoubleSpinBox* spinBoxX = new QDoubleSpinBox();
		QDoubleSpinBox* spinBoxY = new QDoubleSpinBox();

		spinBoxX->setRange( -1000.f, 1000.f );
		spinBoxY->setRange( -1000.f, 1000.f );

		spinBoxX->setValue( i_value.x );
		spinBoxY->setValue( i_value.y );

		treeWidget->connect( spinBoxX, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.x = value;
		} );

		treeWidget->connect( spinBoxY, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.y = value;
		} );

		layout->addWidget( spinBoxX );
		layout->addWidget( spinBoxY );
		w->setLayout( layout );

		MaterialWidget::addTreeWidget( treeWidget, rootWidget, data.m_name, w );
	}

	void operator() ( glm::vec3& i_value )
	{
		QWidget* w = new QWidget();
		QHBoxLayout* layout = new QHBoxLayout();
		QDoubleSpinBox* spinBoxX = new QDoubleSpinBox();
		QDoubleSpinBox* spinBoxY = new QDoubleSpinBox();
		QDoubleSpinBox* spinBoxZ = new QDoubleSpinBox();

		spinBoxX->setRange( -1000.f, 1000.f );
		spinBoxY->setRange( -1000.f, 1000.f );
		spinBoxZ->setRange( -1000.f, 1000.f );

		spinBoxX->setValue( i_value.x );
		spinBoxY->setValue( i_value.y );
		spinBoxZ->setValue( i_value.z );

		treeWidget->connect( spinBoxX, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.x = value;
		} );

		treeWidget->connect( spinBoxY, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.y = value;
		} );

		treeWidget->connect( spinBoxZ, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.z = value;
		} );

		layout->addWidget( spinBoxX );
		layout->addWidget( spinBoxY );
		layout->addWidget( spinBoxZ );
		w->setLayout( layout );

		MaterialWidget::addTreeWidget( treeWidget, rootWidget, data.m_name, w );
	}

	void operator() ( glm::vec4& i_value )
	{
		QWidget* w = new QWidget();
		QHBoxLayout* layout = new QHBoxLayout();
		QDoubleSpinBox* spinBoxX = new QDoubleSpinBox();
		QDoubleSpinBox* spinBoxY = new QDoubleSpinBox();
		QDoubleSpinBox* spinBoxZ = new QDoubleSpinBox();
		QDoubleSpinBox* spinBoxW = new QDoubleSpinBox();

		spinBoxX->setRange( -1000.f, 1000.f );
		spinBoxY->setRange( -1000.f, 1000.f );
		spinBoxZ->setRange( -1000.f, 1000.f );
		spinBoxW->setRange( -1000.f, 1000.f );

		spinBoxX->setValue( i_value.x );
		spinBoxY->setValue( i_value.y );
		spinBoxZ->setValue( i_value.z );
		spinBoxW->setValue( i_value.w );

		treeWidget->connect( spinBoxX, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.x = value;
		} );

		treeWidget->connect( spinBoxY, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.y = value;
		} );

		treeWidget->connect( spinBoxZ, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.z = value;
		} );

		treeWidget->connect( spinBoxW, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value.w = value;
		} );

		layout->addWidget( spinBoxX );
		layout->addWidget( spinBoxY );
		layout->addWidget( spinBoxZ );
		layout->addWidget( spinBoxW );
		w->setLayout( layout );

		MaterialWidget::addTreeWidget( treeWidget, rootWidget, data.m_name, w );
	}

	void operator() ( float& i_value )
	{
		QWidget* w = new QWidget();
		QHBoxLayout* layout = new QHBoxLayout();
		QDoubleSpinBox* spinBox = new QDoubleSpinBox();

		spinBox->setRange( -1000.f, 1000.f );

		spinBox->setValue( i_value );

		treeWidget->connect( spinBox, static_cast<void( QDoubleSpinBox::* )( double )>( &QDoubleSpinBox::valueChanged ), [&i_value]( double value )
		{
			i_value = value;
		} );

		layout->addWidget( spinBox );
		w->setLayout( layout );

		MaterialWidget::addTreeWidget( treeWidget, rootWidget, data.m_name, w );
	}

	void operator() ( gc::s32& i_value )
	{
		QWidget* w = new QWidget();
		QHBoxLayout* layout = new QHBoxLayout();
		QSpinBox* spinBox = new QSpinBox();

		spinBox->setRange( -1000, 1000 );

		spinBox->setValue( i_value );

		treeWidget->connect( spinBox, static_cast<void( QSpinBox::* )( int )>( &QSpinBox::valueChanged ), [&i_value]( int value )
		{
			i_value = value;
		} );

		layout->addWidget( spinBox );
		w->setLayout( layout );

		MaterialWidget::addTreeWidget( treeWidget, rootWidget, data.m_name, w );
	}

private:
	gc::MaterialData& data;
	QTreeWidget* treeWidget;
	QTreeWidgetItem* rootWidget;
};

//-----------------------------------------------------------------------------

MaterialWidget::MaterialWidget( QWidget* i_parent ) :
	QWidget( i_parent ),
	m_ui( std::make_unique<Ui::MaterialWidget>() )
{
	m_ui->setupUi( this );
}

void MaterialWidget::init()
{

}

void MaterialWidget::buildUi( gc::Mesh* i_mesh )
{
	m_ui->treeWidget->clear();

	QTreeWidgetItem* treeRoot = MaterialWidget::addTreeRoot( m_ui->treeWidget, i_mesh->getName(), "" );

	i_mesh->getMaterial().forEachMaterialData( [this, treeRoot]( gc::MaterialData& data )
	{
		MaterialDataUIVisitor visitor( data, m_ui->treeWidget, treeRoot );
		std::visit( visitor, data.m_value );
	} );
}

void MaterialWidget::addTreeWidget( QTreeWidget* o_treeWidget, QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child )
{
	QTreeWidgetItem* treeItemChild = new QTreeWidgetItem();
	treeItemChild->setText( 0, i_name.data() );

	o_parent->addChild( treeItemChild );
	o_treeWidget->setItemWidget( treeItemChild, 1, i_child );
}

QTreeWidgetItem* MaterialWidget::addTreeRoot( QTreeWidget* o_treeWidget, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_treeWidget );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

QTreeWidgetItem* MaterialWidget::addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_parent );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

//-----------------------------------------------------------------------------