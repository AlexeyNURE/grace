#include "application/src/gui/entities_widget.hpp"

#include <QPixmap>
#include <QToolButton>
#include <QTreeWidget>
#include <QColorDialog>
#include <QSpinBox>
#include <QCombobox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFileDialog>
#include <qmessagebox.h>
#include <QMenu>

#include <ui_EntitiesWidget.h>

#include "application/src/resource/resource_manager.hpp"
#include "application/src/components/model_component.hpp"
#include <grace/render/model/mesh.hpp>

//-----------------------------------------------------------------------------

EntitiesWidget::EntitiesWidget( QWidget* i_parent ) :
	QWidget( i_parent ),
	m_project( nullptr ),
	m_contextEntity( nullptr ),
	m_ui( std::make_unique<Ui::EntitiesWidget>() )
{
	m_ui->setupUi( this );
}

void EntitiesWidget::init( Project& i_project )
{
	m_project = &i_project;

	m_ui->treeWidget->setContextMenuPolicy( Qt::CustomContextMenu );
	connect( m_ui->treeWidget, SIGNAL( itemDoubleClicked( QTreeWidgetItem*, int ) ), this, SLOT( onMouseDoubleClicked( QTreeWidgetItem*, int ) ) );
	connect( m_ui->removeEntityButton, SIGNAL( clicked() ), this, SLOT( onRemoveEntityBtnClicked() ) );
	connect( m_ui->addEntityButton, SIGNAL( clicked() ), this, SLOT( onAddEntityBtnClicked() ) );
	connect( m_ui->treeWidget, SIGNAL( customContextMenuRequested( const QPoint& ) ), this, SLOT( prepareMenu( const QPoint& ) ) );

	buildUi();
}

void EntitiesWidget::buildUi()
{
	m_ui->treeWidget->clear();

	QTreeWidgetItem* treeRoot = EntitiesWidget::addTreeRoot( m_ui->treeWidget, "Entities", "" );

	m_project->forEachEntity( [this, treeRoot]( const Entity& i_entity )
	{
		QTreeWidgetItem* treeItemChild = new QTreeWidgetItem();
		treeItemChild->setText( 0, i_entity.getName().data() );
		treeRoot->addChild( treeItemChild );
	} );
}

void EntitiesWidget::onMouseDoubleClicked( QTreeWidgetItem* item, int column )
{
	m_project->setSelected( item->text( column ).toStdString() );
}

void EntitiesWidget::onRemoveEntityBtnClicked()
{
	if( !m_project->getSelected() )
	{
		QMessageBox::warning( this, "Warning", "Please select entity to remove and try again", QMessageBox::Ok );
		return;
	}

	m_project->removeSelectedEntity();
	buildUi();
}

void EntitiesWidget::onAddEntityBtnClicked()
{
	int id = 0;
	while( m_project->getEntityById( EntityId( id ) ) != nullptr )
	{
		id++;
	}

	m_project->addEntity( std::make_unique<Entity>( EntityId( id ), fmt::format( "NewEntity_{}", id ) ) );

	buildUi();
}

void EntitiesWidget::prepareMenu( const QPoint& pos )
{
	QTreeWidgetItem* item = m_ui->treeWidget->itemAt( pos );
	if( !item )
		return;

	Entity* e = m_project->getEntityByName( item->text( 0 ).toStdString() );
	if( !e )
		return;
	
	QMenu menu( tr( "Context menu" ), this );
	m_contextEntity = e;

	if( m_contextEntity->hasComponent<ModelComponent>() )
	{
		menu.addAction( "- ModelComponent",
		[this]()
		{
			removeCmp<ModelComponent>();
		} );
	}
	else
	{
		menu.addAction( "+ ModelComponent", this, SLOT( addModelCmp() ) );
	}

	if( e->hasComponent<TransformComponent>() )
	{
		menu.addAction( "- TransformComponent",
			[this]()
		{
			removeCmp<TransformComponent>();
		} );
	}
	else
		menu.addAction( "+ TransformComponent", this, SLOT( addTransformCmp() ) );

	if( e->hasComponent<RenderComponent>() )
	{
		menu.addAction( "- RenderComponent",
			[this]()
		{
			removeCmp<RenderComponent>();
		} );
	}
	else
		menu.addAction( "+ RenderComponent", this, SLOT( addRenderCmp() ) );

	if( e->hasComponent<MotionComponent>() )
	{
		menu.addAction( "- MotionComponent",
			[this]()
		{
			removeCmp<MotionComponent>();
		} );
	}
	else
		menu.addAction( "+ MotionComponent", this, SLOT( addMotionCmp() ) );

	if( e->hasComponent<LightComponent>() )
	{
		menu.addAction( "- LightComponent",
			[this]()
		{
			removeCmp<LightComponent>();
		} );
	}
	else
		menu.addAction( "+ LightComponent", this, SLOT( addLightCmp() ) );

	//add needed actions
	menu.exec( m_ui->treeWidget->viewport()->mapToGlobal( pos ) );
}

void EntitiesWidget::addModelCmp()
{
	auto& cmpManager = m_project->getComponentManager();

	std::unique_ptr<ModelComponent> modelCmp = std::make_unique<ModelComponent>( *m_project->getScene() );

	cmpManager.addComponent( std::move( modelCmp ), m_contextEntity );
}

void EntitiesWidget::addRenderCmp()
{
	if( !m_contextEntity->hasComponent<ModelComponent>() || !m_contextEntity->hasComponent<TransformComponent>() )
	{
		QMessageBox::warning( this, "Warning", "You should add ModelComponent and TransformComponent first", QMessageBox::Ok );
		return;
	}

	auto& cmpManager = m_project->getComponentManager();

	std::unique_ptr<RenderComponent> renderCmp = std::make_unique<RenderComponent>(
		*m_project->getScene(),
		&m_contextEntity->getComponent<ModelComponent>().get(),
		&m_contextEntity->getComponent<TransformComponent>().get() );

	m_project->getScene()->registerRenderable( *renderCmp.get() );

	cmpManager.addComponent( std::move( renderCmp ), m_contextEntity );
}

void EntitiesWidget::addTransformCmp()
{
	auto& cmpManager = m_project->getComponentManager();

	std::unique_ptr<TransformComponent> transformCmp = std::make_unique<TransformComponent>( m_project->getScene()->getCamera() );

	transformCmp->setPosition( glm::vec3( 0.f, 0.f, 0.f ) );
	transformCmp->setRotation( glm::vec3( 0.f, 0.f, 0.f ) );
	transformCmp->setScale( glm::vec3( 1.f, 1.f, 1.f ) );

	cmpManager.addComponent( std::move( transformCmp ), m_contextEntity );
}

void EntitiesWidget::addMotionCmp()
{
	if( !m_contextEntity->hasComponent<TransformComponent>() )
	{
		QMessageBox::warning( this, "Warning", "You should add TransformComponent first", QMessageBox::Ok );
		return;
	}

	auto& cmpManager = m_project->getComponentManager();

	std::unique_ptr<MotionComponent> motionCmp =
		std::make_unique<MotionComponent>( m_contextEntity->getComponent<TransformComponent>().get() );

	cmpManager.addComponent( std::move( motionCmp ), m_contextEntity );
}

void EntitiesWidget::addLightCmp()
{
	if( !m_contextEntity->hasComponent<TransformComponent>() )
	{
		QMessageBox::warning( this, "Warning", "You should add TransformComponent first", QMessageBox::Ok );
		return;
	}

	auto& cmpManager = m_project->getComponentManager();

	std::unique_ptr<LightComponent> lightCmp =
		std::make_unique<LightComponent>( *m_project->getScene(), m_contextEntity->getComponent<TransformComponent>().get() );

	lightCmp->initDefault();

	cmpManager.addComponent( std::move( lightCmp ), m_contextEntity );
}

template<typename T>
void EntitiesWidget::removeCmp()
{
	if( !m_contextEntity->hasComponent<T>() )
	{
		return;
	}

	auto& cmpManager = m_project->getComponentManager();
	cmpManager.removeComponent<T>( &m_contextEntity->getComponent<T>().get(), m_contextEntity );
}

void EntitiesWidget::addTreeWidget( QTreeWidget* o_treeWidget, QTreeWidgetItem* o_parent, std::string_view i_name, QWidget* i_child )
{
	QTreeWidgetItem* treeItemChild = new QTreeWidgetItem();
	treeItemChild->setText( 0, i_name.data() );

	o_parent->addChild( treeItemChild );
	o_treeWidget->setItemWidget( treeItemChild, 1, i_child );
}

QTreeWidgetItem* EntitiesWidget::addTreeRoot( QTreeWidget* o_treeWidget, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_treeWidget );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

QTreeWidgetItem* EntitiesWidget::addTreeChild( QTreeWidgetItem* o_parent, std::string_view i_name, std::string_view i_description )
{
	QTreeWidgetItem* treeItem = new QTreeWidgetItem( o_parent );

	treeItem->setText( 0, i_name.data() );
	treeItem->setText( 1, i_description.data() );

	return treeItem;
}

//-----------------------------------------------------------------------------