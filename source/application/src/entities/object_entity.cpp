#include "precompiled.hpp"

#include "application/src/entities/object_entity.hpp"

//-----------------------------------------------------------------------------

ObjectEntity::ObjectEntity( EntityId i_id, std::string_view i_name ) :
	Entity( i_id, i_name )
{
}

//-----------------------------------------------------------------------------