#include "application/src/entities/entity.hpp"
#include "application/src/components/component.hpp"

//-----------------------------------------------------------------------------

Entity::Entity( EntityId i_id, std::string_view i_name ) :
	m_id( i_id ),
	m_name( i_name )
{
}

void Entity::addComponent( Component* i_component )
{
	i_component->setOwner( this );
	m_components.emplace_back( std::move( i_component ) );
}

void Entity::removeComponent( Component* i_component )
{
	auto it = m_components.begin();
	while( it != m_components.end() )
	{
		if( *it == i_component )
		{
			it = m_components.erase( it );
		}
		else
			++it;
	}
}

void Entity::forEachComponent( const std::function<void( Component* )>& i_callback )
{
	for( auto& cmp : m_components )
	{
		i_callback( cmp );
	}
}

void Entity::forEachComponent( const std::function<void( const Component* )>& i_callback ) const
{
	for( const auto& cmp : m_components )
	{
		i_callback( cmp );
	}
}

//-----------------------------------------------------------------------------