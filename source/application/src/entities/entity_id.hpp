#pragma once

#include <grace/core/utils/basic_types.hpp>
#include <grace/core/utils/identifier.hpp>

//-----------------------------------------------------------------------------

DECLARE_IDENTIFIER( EntityId, gc::u32, -1 );

//------------------------------------------------------------------------------