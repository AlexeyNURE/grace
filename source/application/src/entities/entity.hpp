#pragma once

#include <string>
#include <memory>
#include <vector>
#include <functional>

#include <boost/noncopyable.hpp>
#include <boost/optional/optional.hpp>

#include <grace/core/utils/utils.hpp>

#include "application/src/entities/entity_id.hpp"

//-----------------------------------------------------------------------------

class Component;

//-----------------------------------------------------------------------------

class Entity : boost::noncopyable
{
public:
	Entity( EntityId i_id, std::string_view i_name );
	virtual ~Entity() = default;

	Entity( Entity&& ) = default;
	Entity& operator= ( Entity&& ) = default;

	std::string_view getName() const;
	EntityId getId() const;

	void addComponent( Component* i_component );
	void removeComponent( Component* i_component );

	template<typename T>
	boost::optional<T&> getComponent();
	template<typename T>
	boost::optional<const T&> getComponent() const;

	template<typename T>
	bool hasComponent() const;

	void forEachComponent( const std::function<void( Component* )>& i_callback );
	void forEachComponent( const std::function<void( const Component* )>& i_callback ) const;

protected:
	std::vector<Component*> m_components;

private:
	std::string m_name;
	EntityId m_id;
};

//------------------------------------------------------------------------------

inline std::string_view Entity::getName() const
{
	return m_name;
}

inline EntityId Entity::getId() const
{
	return m_id;
}

template<typename T>
inline boost::optional<T&> Entity::getComponent()
{
	for( auto& comp : m_components )
	{
		if( boost::optional<T&> result = ComponentCast<T>().cast( *comp ) )
		{
			return result;
		}
	}

	return boost::none;
}

template<typename T>
inline boost::optional<const T&> Entity::getComponent() const
{
	for( const auto& comp : m_components )
	{
		if( auto result = ComponentConstCast<const T>().cast( *comp ) )
		{
			return result;
		}
	}

	ASSERT( "Component not found in entity {}", getId().value() );

	return boost::none;
}

template<typename T>
inline bool Entity::hasComponent() const
{
	for( const auto& comp : m_components )
	{
		if( auto result = ComponentConstCast<const T>().cast( *comp ) )
		{
			return true;
		}
	}

	return false;
}

//------------------------------------------------------------------------------