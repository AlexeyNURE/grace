#pragma once

#include "application/src/entities/entity.hpp"

//-----------------------------------------------------------------------------

class ObjectEntity : public Entity
{
public:
	ObjectEntity( EntityId i_id, std::string_view i_name );
	~ObjectEntity() = default;
};

//------------------------------------------------------------------------------