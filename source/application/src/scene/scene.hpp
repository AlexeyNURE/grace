#pragma once

#include <boost/optional/optional.hpp>
#include <boost/signals2/signal.hpp>
#include <json/json.h>
#include <vector>

#include <grace/render/renderer_API.hpp>
#include <grace/render/light/light.hpp>
#include <grace/render/light/ambient_light.hpp>

#include <grace/core/utils/singleton.hpp>
#include <grace/core/utils/math/ray3.hpp>

#include <grace/render/fog/fog.hpp>
#include "application/src/camera/camera.hpp"

#include "application/src/scene/renderer3D.hpp"

//-----------------------------------------------------------------------------

class Renderable;

//-----------------------------------------------------------------------------

class Scene : public boost::noncopyable
{
public:
	static constexpr glm::vec3 getRightVector();
	static constexpr glm::vec3 getFrontVector();
	static constexpr glm::vec3 getUpVector();

	void registerRenderable( const Renderable& i_renderable );
	void unregisterRenderable( const Renderable& i_renderable );

	void update( float i_deltaTime );
	void render( const gc::OpenGLRendererAPI& i_rendererAPI ) const;

	Camera& getCamera();
	const Camera& getCamera() const;

	gc::Fog& getFog();
	const gc::Fog& getFog() const;
	bool hasFog() const;

	gc::AmbientLight& getAmbientLight();
	const gc::AmbientLight& getAmbientLight() const;

	size_t getLightCount() const;

	void forEachLight( const std::function<void( const gc::Light& )>& i_callback ) const;

	gc::u32 getLightDirCount() const;
	gc::u32 getLightPosCount() const;

	const std::vector<glm::vec3>& getLightDirs() const;
	const std::vector<glm::vec3>& getLightPoints() const;
	const std::vector<glm::vec4>& getLightDirColors() const;
	const std::vector<glm::vec4>& getLightPointColors() const;

	void setCamera( const Camera& i_camera );
	
	void setAmbientLight( const gc::AmbientLight& i_ambientLight );
	void addLight( std::unique_ptr<gc::Light> i_light );
	void removeLight( const gc::Light& i_light );

	void setFog( const gc::Fog& i_fog );
	void setFogEnabled( bool i_yes );
	bool isFogEnabled() const;

	Renderer3D& getRenderer3D();

public:
	boost::signals2::signal<void( gc::utils::ray3f )> sigRayPicked;
	void processMouseReleased( const glm::vec2& mouseCoords );

private:
	void updateLight( float i_deltaTime );
	void reloadLight();

private:
	Renderer3D m_renderer3D;

	std::vector<const Renderable*> m_renderables;

	gc::AmbientLight m_ambientLight;

	std::list<std::unique_ptr<gc::Light>> m_lights;

	std::vector<glm::vec3> m_lightDirections;
	std::vector<glm::vec3> m_lightPositions;
	std::vector<glm::vec4> m_dirColors;
	std::vector<glm::vec4> m_pointColors;

	Camera m_camera;

	boost::optional<gc::Fog> m_fog;
	bool m_fogEnabled{ true };

	static constexpr glm::vec3 s_rightVector{ 1.f, 0.f, 0.f };
	static constexpr glm::vec3 s_frontVector{ 0.f, 1.f, 0.f };
	static constexpr glm::vec3 s_upVector{ 0.f, 0.f, 1.f };
};

//-----------------------------------------------------------------------------

inline constexpr glm::vec3 Scene::getRightVector()
{
	return s_rightVector;
}

inline constexpr glm::vec3 Scene::getFrontVector()
{
	return s_frontVector;
}

inline constexpr glm::vec3 Scene::getUpVector()
{
	return s_upVector;
}

inline Camera& Scene::getCamera()
{
	return m_camera;
}

inline const Camera& Scene::getCamera() const
{
	return m_camera;
}

inline gc::Fog& Scene::getFog()
{
	return *m_fog;
}

inline const gc::Fog& Scene::getFog() const
{
	return *m_fog;
}

inline gc::AmbientLight& Scene::getAmbientLight()
{
	return m_ambientLight;
}

inline const gc::AmbientLight& Scene::getAmbientLight() const
{
	return m_ambientLight;
}

inline size_t Scene::getLightCount() const
{
	return m_lights.size();
}

inline gc::u32 Scene::getLightDirCount() const
{
	return static_cast<gc::u32>( m_lightDirections.size() );
}

inline gc::u32 Scene::getLightPosCount() const
{
	return static_cast<gc::u32>( m_lightPositions.size() );
}

inline const std::vector<glm::vec3>& Scene::getLightDirs() const
{
	return m_lightDirections;
}

inline const std::vector<glm::vec3>& Scene::getLightPoints() const
{
	return m_lightPositions;
}

inline const std::vector<glm::vec4>& Scene::getLightPointColors() const
{
	return m_pointColors;
}

inline const std::vector<glm::vec4>& Scene::getLightDirColors() const
{
	return m_dirColors;
}

inline Renderer3D& Scene::getRenderer3D()
{
	return m_renderer3D;
}

inline bool Scene::isFogEnabled() const
{
	return m_fogEnabled;
}

//-----------------------------------------------------------------------------