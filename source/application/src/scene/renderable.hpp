#pragma once

#include <grace/render/renderer_API.hpp>

//------------------------------------------------------------------------

class Renderable
{
public:
	virtual void render( const gc::OpenGLRendererAPI& i_rendererAPI ) const = 0;
};

//------------------------------------------------------------------------