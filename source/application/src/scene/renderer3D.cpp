#include "precompiled.hpp"

#include <glad/glad.h>

#include <grace/render/model/mesh.hpp>

#include "application/src/scene/renderer3D.hpp"
#include "application/src/scene/scene.hpp"
#include "application/src/resource/resource_manager.hpp"

#include "application/src/components/transform_component.hpp"
#include "application/src/components/model_component.hpp"

//-----------------------------------------------------------------------------

void Renderer3D::drawEntityAabb( const gc::OpenGLRendererAPI& i_rendererAPI, const Entity& i_entity ) const
{
	// MVP without calculating matrices
	auto& MVP = i_entity.getComponent<TransformComponent>()->getMVPMatrix();

	glm::vec3 corners[8];
	const auto& aabb = i_entity.getComponent<ModelComponent>()->getModel()->getBoundingBox();
	aabb.getCorenrs( corners );
	std::vector<gc::utils::Vertex> pVerticesData( 8, gc::utils::Vertex() );
	// front
	pVerticesData[0].pos = corners[0];
	pVerticesData[1].pos = corners[1];
	pVerticesData[2].pos = corners[5];
	pVerticesData[3].pos = corners[4];
	// back
	pVerticesData[4].pos = corners[2];
	pVerticesData[5].pos = corners[3];
	pVerticesData[6].pos = corners[7];
	pVerticesData[7].pos = corners[6];

	gc::u32 indices[] = {
		// front
		0, 1, 2,
		2, 3, 0,
		// right
		1, 5, 6,
		6, 2, 1,
		// back
		7, 6, 5,
		5, 4, 7,
		// left
		4, 0, 3,
		3, 7, 4,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// top
		3, 2, 6,
		6, 7, 3
	};

	gc::Mesh aabbModel = gc::Mesh( pVerticesData.data(), 8, indices, 36 );

	gc::Shader* aabbShader = ResourceManager::getInstance().getShaderByName( "simple" );

	aabbShader->bind();
	aabbShader->uniformValue( "u_matMVP", MVP );
	aabbShader->uniformValue( "u_color", glm::vec4( 1.f, 1.f, 1.f, 1.f ) );
	aabbModel.bind();
	
	glDrawElements( GL_LINES, aabbModel.getIndicesCount(), GL_UNSIGNED_INT, nullptr );
}

//-----------------------------------------------------------------------------