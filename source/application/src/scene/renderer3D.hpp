#pragma once

#include <grace/render/renderer_API.hpp>

#include "application/src/entities/entity.hpp"

//-----------------------------------------------------------------------------

class Renderer3D
{
public:
	Renderer3D() = default;
	~Renderer3D() = default;

	void drawEntityAabb( const gc::OpenGLRendererAPI& i_rendererAPI, const Entity& i_entity ) const;
};

//-----------------------------------------------------------------------------