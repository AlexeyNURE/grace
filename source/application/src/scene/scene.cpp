#include "precompiled.hpp"

#include <json\json.h>
#include <fstream>

#include <grace/core/utils/utils.hpp>
#include <grace/core/utils/globals.hpp>
#include <grace/core/utils/math/intersect.hpp>

#include "application/src/resource/resource_manager.hpp"
#include "application/src/scene/scene.hpp"
#include "application/src/components/model_component.hpp"
#include "application/src/components/transform_component.hpp"
#include "application/src/components/motion_component.hpp"

#include "application/src/scene/renderable.hpp"

//-----------------------------------------------------------------------------

void Scene::registerRenderable( const Renderable& i_renderable )
{
	m_renderables.push_back( &i_renderable );
}

void Scene::unregisterRenderable( const Renderable& i_renderable )
{
	auto it = m_renderables.begin();
	while( it != m_renderables.end() )
	{
		if( *it == &i_renderable )
		{
			it = m_renderables.erase( it );
		}
		else
			++it;
	}
}

void Scene::processMouseReleased( const glm::vec2& mouseCoords )
{
	float x = ( 2.0f * mouseCoords.x ) / gc::Globals::s_screenWidth - 1.0f;
	float y = 1.0f - ( 2.0f * mouseCoords.y ) / gc::Globals::s_screenHeight;
	float z = 1.0f;
	glm::vec3 ray_nds = glm::vec3( x, y, z );
	glm::vec4 ray_clip = glm::vec4( ray_nds.x, ray_nds.y, -1.0, 1.0 );
	auto & projection_matrix = m_camera.getProjectionMatrix();
	glm::vec4 ray_eye = glm::inverse( projection_matrix ) * ray_clip;
	ray_eye = glm::vec4( ray_eye.x, ray_eye.y, -1.0, 0.0 );
	auto& view_matrix = m_camera.getViewMatrix();
	glm::vec3 ray_wor = glm::vec3( glm::inverse( view_matrix ) * ray_eye );
	// don't forget to normalize the vector at some point
	ray_wor = glm::normalize( ray_wor );
	
	gc::utils::ray3f ray( m_camera.getPosition(), ray_wor );

	sigRayPicked( ray );
}

bool Scene::hasFog() const
{
	return m_fog.is_initialized();
}

void Scene::update( float i_deltaTime )
{
	m_camera.update( i_deltaTime );

	updateLight( i_deltaTime );
}

void Scene::render( const gc::OpenGLRendererAPI& i_rendererAPI ) const
{
	for( const auto& renderable : m_renderables )
	{
		renderable->render( i_rendererAPI );
	}
}

void Scene::updateLight( float i_deltaTime )
{
	int counter = 0;
	for( const auto& light : m_lights )
	{
		if( light->getLightType() == gc::LightType::point )
		{
			const auto& pos = light->getPositionDir();
			if( m_lightPositions[counter] != pos )
			{
				m_lightPositions[counter] = pos;
			}

			const auto& color = light->getColor();
			if( m_pointColors[counter] != color )
			{
				m_pointColors[counter] = color;
			}

			counter++;
		}
	}
}

void Scene::setCamera( const Camera& i_camera )
{
	m_camera = i_camera;
}

void Scene::setAmbientLight( const gc::AmbientLight& i_ambientLight )
{
	m_ambientLight = i_ambientLight;
}

void Scene::addLight( std::unique_ptr<gc::Light> i_light )
{
	if( i_light->getLightType() == gc::LightType::directional )
	{
		m_lightDirections.emplace_back( i_light->getPositionDir() );
		m_dirColors.emplace_back( i_light->getColor() );
	}
	else if( i_light->getLightType() == gc::LightType::point )
	{
		m_lightPositions.emplace_back( i_light->getPositionDir() );
		m_pointColors.emplace_back( i_light->getColor() );
	}

	m_lights.emplace_back( std::move( i_light ) );
}

void Scene::removeLight( const gc::Light& i_light )
{
	m_lights.remove_if( [&]( const std::unique_ptr<gc::Light>& light )
	{
		return light.get() == &i_light;
	} );

	reloadLight();
}

void Scene::setFog( const gc::Fog& i_fog )
{
	m_fog = i_fog;
}

void Scene::setFogEnabled( bool i_yes )
{
	m_fogEnabled = i_yes;
}

void Scene::reloadLight()
{
	m_lightDirections.clear();
	m_dirColors.clear();
	m_lightPositions.clear();
	m_pointColors.clear();

	for( const auto& light : m_lights )
	{
		if( light->getLightType() == gc::LightType::directional )
		{
			m_lightDirections.emplace_back( light->getPositionDir() );
			m_dirColors.emplace_back( light->getColor() );
		}
		else if( light->getLightType() == gc::LightType::point )
		{
			m_lightPositions.emplace_back( light->getPositionDir() );
			m_pointColors.emplace_back( light->getColor() );
		}
	}
}

void Scene::forEachLight( const std::function<void( const gc::Light& )>& i_callback ) const
{
	for( const auto& light : m_lights )
	{
		i_callback( *light );
	}
}

//-----------------------------------------------------------------------------