precision mediump float;

in vec2 v_uv;
in vec3 v_posW;

uniform sampler2D u_fire;
uniform sampler2D u_fire_mask;
uniform sampler2D u_displacement_map;

uniform float u_time;
uniform float u_dMax;

uniform vec3 u_camPos;
uniform float u_fogStart;
uniform float u_fogRange;
uniform vec4 u_fogColor;

void main()
{
	vec2 disp = texture2D(u_displacement_map, vec2(v_uv.x, v_uv.y + u_time)).xy;
	vec2 offset = (2.0 * disp - 1.0) * u_dMax;

	vec2 new_uv = v_uv + offset;
	vec4 AlphaValue = texture2D(u_fire_mask, v_uv);
	
	// fog
	float camDistance = distance(u_camPos, v_posW);
	float factor = clamp((camDistance - u_fogStart) / u_fogRange, 0.0, 1.0);

	vec4 result_color = texture2D(u_fire, new_uv) * (1.0, 1.0, 1.0, AlphaValue.r);
	gl_FragColor = mix(result_color, u_fogColor, factor);
}