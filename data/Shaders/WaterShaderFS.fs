precision mediump float;

in vec3 v_posW;
in vec2 v_uv;
in vec2 v_uvTiling;
in vec3 v_normW;
in vec3 v_bitangentW;
in vec3 v_tangentW;

uniform sampler2D u_rock;
uniform sampler2D u_displacement_map;
uniform sampler2D u_normalMapTex;

uniform samplerCube u_cubeTexture;

#define DIR_LIGHT_COUNT 1
#define POINT_LIGHT_COUNT 10

uniform vec4 u_ambientColor;
uniform float u_ambientWeight;

uniform vec4 u_dirColors[DIR_LIGHT_COUNT];
uniform vec4 u_pointColors[POINT_LIGHT_COUNT];

uniform vec3 u_lightDirections[DIR_LIGHT_COUNT];
uniform vec3 u_lightPositions[POINT_LIGHT_COUNT];

uniform vec3 u_camPos;

uniform float u_depthDispl;
uniform float u_depthAdjust;
uniform float u_dMax;
uniform float u_time;
uniform vec4 u_waterColor;

uniform float u_fogStart;
uniform float u_fogRange;
uniform vec4 u_fogColor;

void main()
{
	vec2 displMapUV = vec2(v_uv.x, v_uv.y + u_time);
	vec2 displTex = texture2D(u_displacement_map, displMapUV).xy;

	vec3 normal = normalize(v_normW);
	vec4 totalDiffuse = vec4(0.0, 0.0, 0.0, 0.0);

	for (int i = 0; i < DIR_LIGHT_COUNT; i++)
	{
		float diffuseWeight = max(dot(normal, -u_lightDirections[i]), 0.0 );
		totalDiffuse += u_dirColors[i] * diffuseWeight;
	}

	for (int i = 0; i < POINT_LIGHT_COUNT; i++)
	{
		vec3 light_pos = normalize( v_posW - u_lightPositions[i] );
		float diffuseWeight = max(dot(normal, - light_pos), 0.0);
		totalDiffuse += u_pointColors[i] * diffuseWeight;
	}

	// Bottom surface

	float dMaxRefraction = v_uv.x * u_depthDispl;
	float offsetRefractionX = dMaxRefraction * (2.0 * displTex.x - 1.0);
	float offsetRefractionY = dMaxRefraction * (2.0 * displTex.y - 1.0);

	vec2 uvBottom = v_uvTiling + vec2(offsetRefractionX, offsetRefractionY);
	vec4 bottomColor = texture2D(u_rock, uvBottom);

	float depthFactor = clamp(u_depthAdjust * v_uv.x, 0.0, 1.0);
	vec4 refractionColor = mix(bottomColor, u_waterColor, depthFactor);
	refractionColor = mix(totalDiffuse, u_ambientColor, u_ambientWeight) * refractionColor;

	// Upper surface

	float offsetReflectionX = u_dMax * (2.0 * displTex.x - 1.0);
	float offsetReflectionY = u_dMax * (2.0 * displTex.y - 1.0);
	vec2 uvNormal = v_uvTiling + vec2(offsetReflectionX, offsetReflectionY);

	vec3 normTex = texture2D(u_normalMapTex, uvNormal).xyz;
	mat3 TBN = mat3(normalize(v_tangentW), normalize(v_bitangentW), normal);
	vec3 waterNormW = normalize(TBN * (2.0 * normTex - 1.0));

	vec3 toEye = normalize(u_camPos - v_posW);
	vec3 reflectionDir = reflect(toEye, waterNormW);
	vec4 reflectionColor = textureCube(u_cubeTexture, normalize(reflectionDir));


	// fog
	float camDistance = distance(u_camPos, v_posW);
	float factor = clamp((camDistance - u_fogStart) / u_fogRange, 0.0, 1.0);
	
	// final color

	float fresnelPower = 0.9;
	float fresnelTerm = pow((1.0 - abs( dot( waterNormW, toEye ) )), fresnelPower);
	vec4 result_color = vec4(mix(refractionColor, reflectionColor, fresnelTerm).xyz, 1.0);
	gl_FragColor = mix(result_color, u_fogColor, factor);
	gl_FragColor.a = 1.0;
}