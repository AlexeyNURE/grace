precision mediump float;

in vec2 v_uv;

uniform sampler2D u_colorBuffer;
uniform sampler2D u_blurredCBuffer;

uniform float u_passMult;

void main(void)
{
	vec4 color = texture2D(u_colorBuffer, v_uv);
	vec4 blur = texture2D(u_blurredCBuffer, v_uv);
	
	gl_FragColor = color + u_passMult * blur;
}