#version 450

layout (location = 0) in vec3 a_posL;

out vec3 v_posL;
out vec3 v_posW;

uniform mat4 u_matMVP;
uniform mat4 u_matM;

void main()
{
	gl_Position = u_matMVP * vec4(a_posL, 1.0);
	v_posW = vec3(u_matM * vec4(a_posL, 1.0));
	v_posL = a_posL;
}