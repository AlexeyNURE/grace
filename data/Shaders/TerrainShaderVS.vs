#version 450

layout (location = 0) in vec3 a_posL;
layout (location = 1) in vec2 a_uv;

out vec2 v_uv;
out vec2 v_uvTiling;
out vec3 v_posW;

uniform mat4 u_matMVP;
uniform mat4 u_matM;
uniform float u_tilingFactor;

void main()
{
	gl_Position = u_matMVP * vec4(a_posL, 1.0);

	v_posW = vec3(u_matM * vec4(a_posL, 1.0));

	v_uv = a_uv;
	v_uvTiling = a_uv * u_tilingFactor;
}