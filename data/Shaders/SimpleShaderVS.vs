#version 450

layout (location = 0) in vec3 a_posL;

uniform mat4 u_matMVP;

void main()
{
	gl_Position = u_matMVP * vec4(a_posL, 1.0);
}