#version 450

layout (location = 0) in vec3 a_posL;
layout (location = 1) in vec2 a_uv;
layout (location = 2) in vec3 a_normL;
layout (location = 3) in vec3 a_bitangentL;
layout (location = 4) in vec3 a_tangentL;

out vec3 v_posW;
out vec2 v_uv;
out vec3 v_normW;
out vec3 v_bitangentW;
out vec3 v_tangentW;

uniform mat4 u_matM;
uniform mat4 u_matMVP;

void main()
{
	gl_Position = u_matMVP * vec4(a_posL, 1.0);
	
	v_posW = (u_matM * vec4(a_posL, 1.0)).xyz;
	v_uv = a_uv;

	v_normW = normalize( (u_matM * vec4(a_normL, 0.0)).xyz );
	v_tangentW = normalize( (u_matM * vec4(a_tangentL, 0.0)).xyz );
	v_bitangentW = normalize( (u_matM * vec4(a_bitangentL, 0.0)).xyz );
}