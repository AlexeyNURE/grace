precision mediump float;

in vec3 v_normW;
in vec3 v_posW;
in vec2 v_uv;

uniform sampler2D u_texture;
uniform sampler2D u_normalMapTex;
uniform vec3 u_camPos;

#define DIR_LIGHT_COUNT 1
#define POINT_LIGHT_COUNT 10

uniform vec4 u_ambientColor;
uniform float u_ambientWeight;

uniform vec4 u_dirColors[DIR_LIGHT_COUNT];
uniform vec4 u_pointColors[POINT_LIGHT_COUNT];

uniform vec3 u_lightDirections[DIR_LIGHT_COUNT];
uniform vec3 u_lightPositions[POINT_LIGHT_COUNT];

uniform vec4 u_matAmbient;
uniform vec4 u_matDiffuse;
uniform vec4 u_matSpecular;
uniform float u_matShiness;
uniform float u_matAlpha;

uniform float u_fogStart;
uniform float u_fogRange;
uniform vec4 u_fogColor;

void main()
{
	vec3 normal = texture2D(u_normalMapTex, v_uv).xyz;
	vec3 normW = normalize(2.0 * normal - 1.0);

	vec4 totalDiffuse = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 totalSpecular = vec4(0.0, 0.0, 0.0, 0.0);
	vec3 reflectDirection;
	vec3 toEye = normalize(u_camPos - v_posW);

	for (int i = 0; i < DIR_LIGHT_COUNT; i++)
	{
		float diffuseWeight = max(dot(normW, - u_lightDirections[i]), 0.0);
		totalDiffuse += u_dirColors[i] * diffuseWeight * u_matDiffuse;

		reflectDirection = normalize(reflect(u_lightDirections[i], normW));
		float specularWeight = pow(max(dot(reflectDirection, toEye), 0.0), u_matShiness);
		totalSpecular += u_dirColors[i] * specularWeight * u_matSpecular;
	}

	for (int i = 0; i < POINT_LIGHT_COUNT; i++)
	{
		vec3 light_pos = normalize( v_posW - u_lightPositions[i] );
		
		float diffuseWeight = max(dot(normW, - light_pos), 0.0);
		totalDiffuse += u_pointColors[i] * diffuseWeight * u_matDiffuse;
	
		reflectDirection = normalize(reflect(light_pos, normW));
		float specularWeight = pow(max(dot(reflectDirection, toEye), 0.0), u_matShiness);
		totalSpecular += u_pointColors[i] * specularWeight * u_matSpecular;
	}

	// fog
	float camDistance = distance(u_camPos, v_posW);
	float factor = clamp((camDistance - u_fogStart) / u_fogRange, 0.0, 1.0);

	vec4 texture = texture2D(u_texture, v_uv);

	vec4 result_color = mix(totalDiffuse, u_ambientColor * u_matAmbient, u_ambientWeight) * texture + totalSpecular;
	gl_FragColor = mix(result_color, u_fogColor, factor);gl_FragColor = mix(result_color, u_fogColor, factor);
	gl_FragColor.a = u_matAlpha;
}