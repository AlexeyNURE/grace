#version 450

layout (location = 0) in vec3 a_posL;
layout (location = 1) in vec2 a_uv;
layout (location = 2) in vec3 a_normL;

out vec3 v_posW;
out vec3 v_normW;
out vec2 v_uv;

uniform mat4 u_matMVP;
uniform mat4 u_matM;

void main()
{
	gl_Position = u_matMVP * vec4(a_posL, 1.0);

	v_uv = a_uv;
	v_normW = vec3(u_matM * vec4(a_normL, 0.0));
	v_posW = vec3(u_matM * vec4(a_posL, 1.0));
}