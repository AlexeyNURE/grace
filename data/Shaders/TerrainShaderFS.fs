precision mediump float;

in vec2 v_uv;
in vec2 v_uvTiling;
in vec3 v_posW;

uniform sampler2D u_blend_map;
uniform sampler2D u_grass;
uniform sampler2D u_dirt;
uniform sampler2D u_rock;

uniform vec4 u_ambientColor;
uniform float u_ambientWeight;

uniform float u_fogStart;
uniform float u_fogRange;
uniform vec4 u_fogColor;
uniform vec3 u_camPos;

void main()
{
	vec4 blend_map, grass, dirt, rock;

	blend_map = texture2D(u_blend_map, v_uv);
	grass = texture2D(u_grass, v_uvTiling);
	dirt = texture2D(u_dirt, v_uvTiling);
	rock = texture2D(u_rock, v_uvTiling);

	float camDistance = distance(u_camPos, v_posW);
	float factor = clamp((camDistance - u_fogStart) / u_fogRange, 0.0, 1.0);

	vec4 terrainColor = (rock * blend_map.x + grass * blend_map.y + dirt * blend_map.z) 
		/ (blend_map.x + blend_map.y + blend_map.z);

	vec4 result_color = mix(terrainColor, u_ambientColor, u_ambientWeight) * terrainColor;

	vec4 resultColor = mix(result_color, u_fogColor, factor);
	resultColor.a = 1.0;

	gl_FragColor = resultColor;
}