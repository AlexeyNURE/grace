precision mediump float;

in vec3 v_posL;
in vec3 v_posW;

uniform samplerCube u_s_texture;

uniform float u_fogStart;
uniform float u_fogRange;
uniform vec4 u_fogColor;
uniform vec3 u_camPos;

void main()
{
	float camDistance = distance(u_camPos, v_posW);
	float factor = clamp((camDistance - u_fogStart) / u_fogRange, 0.0, 1.0);
	
    vec4 result_color = textureCube(u_s_texture, v_posL);
	gl_FragColor = mix(result_color, u_fogColor, factor);
	gl_FragColor.a = 1.0;
}